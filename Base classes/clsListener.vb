Public Class clsListener
    Inherits Object

    Private ThreadClose As New Threading.Thread(AddressOf CloseApp)
    Private WithEvents _SBO_Application As SAPbouiCOM.Application
    Private _Company As SAPbobsCOM.Company
    Private _Utilities As clsUtilities
    Private _Collection As Hashtable
    Private _LookUpCollection As Hashtable
    Private _FormUID As String
    Private _Log As clsLog_Error
    Private oMenuObject As Object
    Private oItemObject As Object
    Private oSystemForms As Object
    Dim objFilters As SAPbouiCOM.EventFilters
    Dim objFilter As SAPbouiCOM.EventFilter

#Region "New"

    Public Sub New()
        MyBase.New()
        Try
            _Company = New SAPbobsCOM.Company
            _Utilities = New clsUtilities
            _Collection = New Hashtable(10, 0.5)
            _LookUpCollection = New Hashtable(10, 0.5)
            _Log = New clsLog_Error

            SetApplication()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Public Properties"

    Public ReadOnly Property SBO_Application() As SAPbouiCOM.Application
        Get
            Return _SBO_Application
        End Get
    End Property

    Public ReadOnly Property Company() As SAPbobsCOM.Company
        Get
            Return _Company
        End Get
    End Property

    Public ReadOnly Property Utilities() As clsUtilities
        Get
            Return _Utilities
        End Get
    End Property

    Public ReadOnly Property Collection() As Hashtable
        Get
            Return _Collection
        End Get
    End Property

    Public ReadOnly Property LookUpCollection() As Hashtable
        Get
            Return _LookUpCollection
        End Get
    End Property

    Public ReadOnly Property Log() As clsLog_Error
        Get
            Return _Log
        End Get
    End Property

#Region "Filter"

    Public Sub SetFilter(ByVal Filters As SAPbouiCOM.EventFilters)
        oApplication.SBO_Application.SetFilter(Filters)
    End Sub

    Public Sub SetFilter()
        Try

            objFilters = New SAPbouiCOM.EventFilters()

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_INVOICES)
            objFilter.AddEx(frm_UserAuthorization)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_ResetBatchQuanttiy)
            objFilter.AddEx(frm_DecGrpowizard)

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_MENU_CLICK)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_UserAuthorization)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_ResetBatchQuanttiy)
            objFilter.AddEx(frm_DecGrpowizard)

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_UserAuthorization)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_ResetBatchQuanttiy)
            objFilter.AddEx(frm_DecGrpowizard)

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_UserAuthorization)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_ResetBatchQuanttiy)
            objFilter.AddEx(frm_DecGrpowizard)

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_UserAuthorization)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_ResetBatchQuanttiy)
            objFilter.AddEx(frm_DecGrpowizard)

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_UserAuthorization)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_ResetBatchQuanttiy)
            objFilter.AddEx(frm_DecGrpowizard)

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_DecGrpowizard)

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_UserAuthorization)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_ResetBatchQuanttiy)
            objFilter.AddEx(frm_DecGrpowizard)

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_INVOICES)
            objFilter.AddEx(frm_UserAuthorization)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_ResetBatchQuanttiy)
            objFilter.AddEx(frm_DecGrpowizard)

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_CLICK)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_UserAuthorization)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_ResetBatchQuanttiy)
            objFilter.AddEx(frm_DecGrpowizard)

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_RESIZE)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_UserAuthorization)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_DecGrpowizard)

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_UserAuthorization)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_DecGrpowizard)

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_UserAuthorization)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_ResetBatchQuanttiy)
            objFilter.AddEx(frm_DecGrpowizard)

            objFilter = objFilters.Add(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
            objFilter.AddEx(frm_Packing)
            objFilter.AddEx(frm_CBatchCreation)
            objFilter.AddEx(frm_CustomBatchSelection)
            objFilter.AddEx(frm_UserAuthorization)
            objFilter.AddEx(frm_LandingReceipt)
            objFilter.AddEx(frm_PackingItems)
            objFilter.AddEx(frm_ResetBatchQuanttiy)
            objFilter.AddEx(frm_DecGrpowizard)

            SetFilter(objFilters)
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
#End Region

#End Region

    '#End Region

#Region "Data Event"
    Private Sub _SBO_Application_FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean) Handles _SBO_Application.FormDataEvent
        Try

            If _Collection.ContainsKey(_FormUID) Then
                Dim objform As SAPbouiCOM.Form
                objform = oApplication.SBO_Application.Forms.ActiveForm()
                'If 1 = 1 Then
                'Add New By Tejas
                If BusinessObjectInfo.BeforeAction = True Then
                    oMenuObject = _Collection.Item(_FormUID)
                    oMenuObject.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                End If
                'oMenuObject = _Collection.Item(_FormUID)
                'oMenuObject.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                'End If
            Else
                _Collection.Add(_FormUID, oItemObject)
            End If
            Select Case BusinessObjectInfo.FormTypeEx

                Case frm_Packing
                    oMenuObject = _Collection.Item(_FormUID)
                    oMenuObject.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case frm_UserAuthorization
                    oMenuObject = _Collection.Item(_FormUID)
                    oMenuObject.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case frm_LandingReceipt
                    oMenuObject = _Collection.Item(_FormUID)
                    oMenuObject.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case frm_PackingItems
                    oMenuObject = _Collection.Item(_FormUID)
                    oMenuObject.FormDataEvent(BusinessObjectInfo, BubbleEvent)
            End Select
        Catch ex As Exception
            Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
#End Region

#Region "Menu Event"

    Private Sub SBO_Application_MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) Handles _SBO_Application.MenuEvent
        Try
            If pVal.BeforeAction = False Then
                Select Case pVal.MenuUID
                    Case "mnu_MAX_101"
                        If pVal.BeforeAction = False Then
                            oApplication.Utilities.CreateTables()
                        End If

                    Case mnu_ResetBatchQuantity          '
                        oMenuObject = New clsResetBatchQuantity
                        oMenuObject.MenuEvent(pVal, BubbleEvent)

                    Case mnu_Packing         '
                        oMenuObject = New clsPacking
                        oMenuObject.MenuEvent(pVal, BubbleEvent)
                    Case mnu_Createcancel        '
                        oMenuObject = New clsPacking
                        oMenuObject.MenuEvent(pVal, BubbleEvent)

                    Case mnu_UserAuthorization          '
                        oMenuObject = New clsUserAuthorization
                        oMenuObject.MenuEvent(pVal, BubbleEvent)
                    Case mnu_LandingReceipt           '
                        oMenuObject = New clsLandingReceipt
                        oMenuObject.MenuEvent(pVal, BubbleEvent)

                    Case mnu_PackingItems            '
                        oMenuObject = New clsPackingItems
                        oMenuObject.MenuEvent(pVal, BubbleEvent)


                    Case mnu_ADD, mnu_FIND, mnu_FIRST, mnu_LAST, mnu_NEXT, mnu_PREVIOUS, mnu_ADD_ROW, mnu_DELETE_ROW
                        If _Collection.ContainsKey(_FormUID) Then
                            oMenuObject = _Collection.Item(_FormUID)
                            oMenuObject.MenuEvent(pVal, BubbleEvent)
                        End If
                End Select
            Else
                Select Case pVal.MenuUID
                    Case mnu_CLOSE
                        If _Collection.ContainsKey(_FormUID) Then
                            oMenuObject = _Collection.Item(_FormUID)
                            oMenuObject.MenuEvent(pVal, BubbleEvent)
                        End If
                    Case mnu_ADD, mnu_FIND, mnu_FIRST, mnu_LAST, mnu_NEXT, mnu_PREVIOUS, mnu_ADD_ROW, mnu_DELETE_ROW
                        If _Collection.ContainsKey(_FormUID) Then
                            oMenuObject = _Collection.Item(_FormUID)
                            oMenuObject.MenuEvent(pVal, BubbleEvent)
                        End If
                End Select
            End If
        Catch ex As Exception
            Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        Finally
            oMenuObject = Nothing
        End Try
    End Sub

#End Region

#Region "Item Event"

    Private Sub SBO_Application_ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) Handles _SBO_Application.ItemEvent
        Try
            _FormUID = FormUID
            If pVal.BeforeAction = False And pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_LOAD Then
                Select Case pVal.FormTypeEx

                    Case frm_ResetBatchQuanttiy
                        If Not _Collection.ContainsKey(FormUID) Then
                            oItemObject = New clsResetBatchQuantity
                            oItemObject.FrmUID = FormUID
                            _Collection.Add(FormUID, oItemObject)
                        End If

                    Case frm_Packing, frm_INVOICES.ToString
                        If Not _Collection.ContainsKey(FormUID) Then
                            oItemObject = New clsPacking
                            oItemObject.FrmUID = FormUID
                            _Collection.Add(FormUID, oItemObject)
                        End If
                    Case frm_CBatchCreation
                        If Not _Collection.ContainsKey(FormUID) Then
                            oItemObject = New clsBatchCreation1
                            oItemObject.FrmUID = FormUID
                            _Collection.Add(FormUID, oItemObject)
                        End If
                    Case frm_CustomBatchSelection
                        If Not _Collection.ContainsKey(FormUID) Then
                            oItemObject = New clsBatchSelection
                            oItemObject.FrmUID = FormUID
                            _Collection.Add(FormUID, oItemObject)
                        End If
                    Case frm_UserAuthorization
                        If Not _Collection.ContainsKey(FormUID) Then
                            oItemObject = New clsUserAuthorization
                            oItemObject.FrmUID = FormUID
                            _Collection.Add(FormUID, oItemObject)
                        End If
                    Case frm_LandingReceipt
                        If Not _Collection.ContainsKey(FormUID) Then
                            oItemObject = New clsLandingReceipt
                            oItemObject.FrmUID = FormUID
                            _Collection.Add(FormUID, oItemObject)
                        End If

                    Case frm_PackingItems
                        If Not _Collection.ContainsKey(FormUID) Then
                            oItemObject = New clsPackingItems
                            oItemObject.FrmUID = FormUID
                            _Collection.Add(FormUID, oItemObject)
                        End If

                        ''extra add New Wizard Screen
                    Case frm_DecGrpowizard
                        If Not _Collection.ContainsKey(FormUID) Then
                            oItemObject = New DecWizard
                            oItemObject.FrmUID = FormUID
                            _Collection.Add(FormUID, oItemObject)
                        End If
                End Select
            End If

            If _Collection.ContainsKey(FormUID) Then
                oItemObject = _Collection.Item(FormUID)
                If oItemObject.IsLookUpOpen And pVal.BeforeAction = True Then
                    _SBO_Application.Forms.Item(oItemObject.LookUpFormUID).Select()
                    BubbleEvent = False
                    Exit Sub
                End If
                _Collection.Item(FormUID).ItemEvent(FormUID, pVal, BubbleEvent)
            End If
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD And pVal.BeforeAction = False Then
                If _LookUpCollection.ContainsKey(FormUID) Then
                    oItemObject = _Collection.Item(_LookUpCollection.Item(FormUID))
                    If Not oItemObject Is Nothing Then
                        oItemObject.IsLookUpOpen = False
                    End If
                    _LookUpCollection.Remove(FormUID)
                End If

                If _Collection.ContainsKey(FormUID) Then
                    _Collection.Item(FormUID) = Nothing
                    _Collection.Remove(FormUID)
                End If

            End If

        Catch ex As Exception
            Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        Finally
            GC.WaitForPendingFinalizers()
            GC.Collect()
        End Try
    End Sub

#End Region

#Region "Right Click Event"

    Private Sub _SBO_Application_RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean) Handles _SBO_Application.RightClickEvent
        Try
            Dim oForm As SAPbouiCOM.Form
            oForm = oApplication.SBO_Application.Forms.Item(eventInfo.FormUID)
            If _Collection.ContainsKey(eventInfo.FormUID) Then
                If oForm.TypeEx = frm_BPMaster Then
                    oItemObject = _Collection.Item(eventInfo.FormUID)
                    _Collection.Item(eventInfo.FormUID).RightClickEvent(eventInfo, BubbleEvent)
                End If
                If oForm.TypeEx = frm_Packing Then
                    oItemObject = _Collection.Item(eventInfo.FormUID)
                    _Collection.Item(eventInfo.FormUID).RightClickEvent(eventInfo, BubbleEvent)
                End If
            End If

        Catch ex As Exception
            oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub

#End Region

#Region "Application Event"

    Private Sub SBO_Application_AppEvent(ByVal EventType As SAPbouiCOM.BoAppEventTypes) Handles _SBO_Application.AppEvent
        Try
            Select Case EventType
                Case SAPbouiCOM.BoAppEventTypes.aet_ShutDown, SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition, SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged
                    _Utilities.AddRemoveMenus("RemoveMenus.xml")
                    CloseApp()
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Termination Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

#End Region

#Region "Close Application"

    Private Sub CloseApp()
        Try
            If Not _SBO_Application Is Nothing Then
                System.Runtime.InteropServices.Marshal.ReleaseComObject(_SBO_Application)
            End If

            If Not _Company Is Nothing Then
                If _Company.Connected Then
                    _Company.Disconnect()
                End If
                System.Runtime.InteropServices.Marshal.ReleaseComObject(_Company)
            End If

            _Utilities = Nothing
            _Collection = Nothing
            _LookUpCollection = Nothing

            ThreadClose.Sleep(10)
            System.Windows.Forms.Application.Exit()
        Catch ex As Exception
            Throw ex
        Finally
            oApplication = Nothing
            GC.WaitForPendingFinalizers()
            GC.Collect()
        End Try
    End Sub

#End Region

#Region "Set Application"

    Private Sub SetApplication()
        Dim SboGuiApi As SAPbouiCOM.SboGuiApi
        Dim sConnectionString As String

        Try
            If Environment.GetCommandLineArgs.Length > 1 Then
                sConnectionString = Environment.GetCommandLineArgs.GetValue(1)
                SboGuiApi = New SAPbouiCOM.SboGuiApi
                SboGuiApi.Connect(sConnectionString)
                _SBO_Application = SboGuiApi.GetApplication()
            Else
                Throw New Exception("Connection string missing.")
            End If

        Catch ex As Exception
            Throw ex
        Finally
            SboGuiApi = Nothing
        End Try
    End Sub

#End Region

#Region "Finalize"

    Protected Overrides Sub Finalize()
        Try
            MyBase.Finalize()
            '            CloseApp()

            oMenuObject = Nothing
            oItemObject = Nothing
            oSystemForms = Nothing

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Addon Termination Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        Finally
            GC.WaitForPendingFinalizers()
            GC.Collect()
        End Try
    End Sub

#End Region



End Class

Public Class WindowWrapper
    Implements System.Windows.Forms.IWin32Window
    Private _hwnd As IntPtr

    Public Sub New(ByVal handle As IntPtr)
        _hwnd = handle
    End Sub

    Public ReadOnly Property Handle() As System.IntPtr Implements System.Windows.Forms.IWin32Window.Handle
        Get
            Return _hwnd
        End Get
    End Property

End Class


