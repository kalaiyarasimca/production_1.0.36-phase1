Public NotInheritable Class clsTable

#Region "Private Functions"
    '*************************************************************************************************************
    'Type               : Private Function
    'Name               : AddTables
    'Parameter          : 
    'Return Value       : none
    'Author             : Manu
    'Created Dt         : 
    'Last Modified By   : 
    'Modified Dt        : 
    'Purpose            : Generic Function for adding all Tables in DB. This function shall be called by 
    '                     public functions to create a table
    '**************************************************************************************************************
    Private Sub AddTables(ByVal strTab As String, ByVal strDesc As String, ByVal nType As SAPbobsCOM.BoUTBTableType)
        Dim oUserTablesMD As SAPbobsCOM.UserTablesMD
        Try

            oUserTablesMD = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables)
            'Adding Table
            If Not oUserTablesMD.GetByKey(strTab) Then
                oUserTablesMD.TableName = strTab
                oUserTablesMD.TableDescription = strDesc
                oUserTablesMD.TableType = nType
                If oUserTablesMD.Add <> 0 Then
                    Throw New Exception(oApplication.Company.GetLastErrorDescription)
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserTablesMD)
            oUserTablesMD = Nothing
            GC.WaitForPendingFinalizers()
            GC.Collect()
        End Try
    End Sub

    '*************************************************************************************************************
    'Type               : Private Function
    'Name               : AddFields
    'Parameter          : SstrTab As String,strCol As String,
    '                     strDesc As String,nType As Integer,i,nEditSize,nSubType As Integer
    'Return Value       : none
    'Author             : Manu
    'Created Dt         : 
    'Last Modified By   : 
    'Modified Dt        : 
    'Purpose            : Generic Function for adding all Fields in DB Tables. This function shall be called by 
    '                     public functions to create a Field
    '**************************************************************************************************************
    Private Sub AddFields(ByVal strTab As String, _
                            ByVal strCol As String, _
                                ByVal strDesc As String, _
                                    ByVal nType As SAPbobsCOM.BoFieldTypes, _
                                        Optional ByVal i As Integer = 0, _
                                            Optional ByVal nEditSize As Integer = 10, _
                                                Optional ByVal nSubType As SAPbobsCOM.BoFldSubTypes = 0, _
                                                    Optional ByVal Mandatory As SAPbobsCOM.BoYesNoEnum = SAPbobsCOM.BoYesNoEnum.tNO, Optional ByVal defval As String = "0")
        Dim oUserFieldMD As SAPbobsCOM.UserFieldsMD
        Try
            If Not (strTab = "OPDN" Or strTab = "OITM" Or strTab = "OACT" Or strTab = "OBTN" Or strTab = "OCRD" Or strTab = "OPRC" Or strTab = "OINV" Or strTab = "POR1" Or strTab = "INV1" Or strTab = "OPOR" Or strTab = "OWHS" Or strTab = "ORPC" Or strTab = "OIQR" Or strTab = "OCRN" Or strTab = "OJDT" Or strTab = "JDT1" Or strTab = "OINV" Or strTab = "INV1" Or strTab = "OBPL" Or strTab = "OWHS") Then
                strTab = "@" + strTab
            End If

            If Not IsColumnExists(strTab, strCol) Then
                oUserFieldMD = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields)

                oUserFieldMD.Description = strDesc
                oUserFieldMD.Name = strCol
                oUserFieldMD.Type = nType
                oUserFieldMD.SubType = nSubType
                oUserFieldMD.TableName = strTab
                oUserFieldMD.EditSize = nEditSize
                oUserFieldMD.Mandatory = Mandatory
                oUserFieldMD.DefaultValue = defval
                If oUserFieldMD.Add <> 0 Then
                    Throw New Exception(oApplication.Company.GetLastErrorDescription)
                End If

                System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserFieldMD)

            End If

        Catch ex As Exception
            Throw ex
        Finally
            oUserFieldMD = Nothing
            GC.WaitForPendingFinalizers()
            GC.Collect()
        End Try
    End Sub

    '*************************************************************************************************************
    'Type               : Private Function
    'Name               : AddFields
    'Parameter          : SstrTab As String,strCol As String,
    '                     strDesc As String,nType As Integer,i,nEditSize,nSubType As Integer
    'Return Value       : none
    'Author             : Manu
    'Created Dt         : 
    'Last Modified By   : 
    'Modified Dt        : 
    'Purpose            : Generic Function for adding all Fields in DB Tables. This function shall be called by 
    '                     public functions to create a Field
    '**************************************************************************************************************
    Public Sub addField(ByVal TableName As String, ByVal ColumnName As String, ByVal ColDescription As String, ByVal FieldType As SAPbobsCOM.BoFieldTypes, ByVal Size As Integer, ByVal SubType As SAPbobsCOM.BoFldSubTypes, ByVal ValidValues As String, ByVal ValidDescriptions As String, ByVal SetValidValue As String)
        Dim intLoop As Integer
        Dim strValue, strDesc As Array
        Dim objUserFieldMD As SAPbobsCOM.UserFieldsMD
        Try

            strValue = ValidValues.Split(Convert.ToChar(","))
            strDesc = ValidDescriptions.Split(Convert.ToChar(","))
            If (strValue.GetLength(0) <> strDesc.GetLength(0)) Then
                Throw New Exception("Invalid Valid Values")
            End If


            If (Not IsColumnExists(TableName, ColumnName)) Then
                objUserFieldMD = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields)
                objUserFieldMD.TableName = TableName
                objUserFieldMD.Name = ColumnName
                objUserFieldMD.Description = ColDescription
                objUserFieldMD.Type = FieldType
                If (FieldType <> SAPbobsCOM.BoFieldTypes.db_Numeric) Then
                    objUserFieldMD.Size = Size
                Else
                    objUserFieldMD.EditSize = Size
                End If
                objUserFieldMD.SubType = SubType
                objUserFieldMD.DefaultValue = SetValidValue
                For intLoop = 0 To strValue.GetLength(0) - 1
                    objUserFieldMD.ValidValues.Value = strValue(intLoop)
                    objUserFieldMD.ValidValues.Description = strDesc(intLoop)
                    objUserFieldMD.ValidValues.Add()
                Next
                If (objUserFieldMD.Add() <> 0) Then
                    MsgBox(oApplication.Company.GetLastErrorDescription)
                End If
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objUserFieldMD)
            Else
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        Finally
            objUserFieldMD = Nothing
            GC.WaitForPendingFinalizers()
            GC.Collect()

        End Try


    End Sub

    '*************************************************************************************************************
    'Type               : Private Function
    'Name               : IsColumnExists
    'Parameter          : ByVal Table As String, ByVal Column As String
    'Return Value       : Boolean
    'Author             : Manu
    'Created Dt         : 
    'Last Modified By   : 
    'Modified Dt        : 
    'Purpose            : Function to check if the Column already exists in Table
    '**************************************************************************************************************
    Private Function IsColumnExists(ByVal Table As String, ByVal Column As String) As Boolean
        Dim oRecordSet As SAPbobsCOM.Recordset

        Try
            strSQL = "SELECT COUNT(*) FROM CUFD WHERE ""TableID"" = '" & Table & "' AND ""AliasID"" = '" & Column & "'"
            oRecordSet = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRecordSet.DoQuery(strSQL)

            If oRecordSet.Fields.Item(0).Value = 0 Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Throw ex
        Finally
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet)
            oRecordSet = Nothing
            GC.Collect()
        End Try
    End Function

    Private Sub AddKey(ByVal strTab As String, ByVal strColumn As String, ByVal strKey As String, ByVal i As Integer)
        Dim oUserKeysMD As SAPbobsCOM.UserKeysMD

        Try
            '// The meta-data object must be initialized with a
            '// regular UserKeys object
            oUserKeysMD = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserKeys)

            If Not oUserKeysMD.GetByKey("@" & strTab, i) Then

                '// Set the table name and the key name
                oUserKeysMD.TableName = strTab
                oUserKeysMD.KeyName = strKey

                '// Set the column's alias
                oUserKeysMD.Elements.ColumnAlias = strColumn
                oUserKeysMD.Elements.Add()
                oUserKeysMD.Elements.ColumnAlias = "RentFac"

                '// Determine whether the key is unique or not
                oUserKeysMD.Unique = SAPbobsCOM.BoYesNoEnum.tYES

                '// Add the key
                If oUserKeysMD.Add <> 0 Then
                    Throw New Exception(oApplication.Company.GetLastErrorDescription)
                End If

            End If

        Catch ex As Exception
            Throw ex

        Finally
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserKeysMD)
            oUserKeysMD = Nothing
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try

    End Sub

    '********************************************************************
    'Type		            :   Function    
    'Name               	:	AddUDO
    'Parameter          	:   
    'Return Value       	:	Boolean
    'Author             	:	
    'Created Date       	:	
    'Last Modified By	    :	
    'Modified Date        	:	
    'Purpose             	:	To Add a UDO for Transaction Tables
    '********************************************************************
    Private Sub AddUDO(ByVal strUDO As String, ByVal strDesc As String, ByVal strTable As String, _
                               Optional ByVal sFind1 As String = "", Optional ByVal sFind2 As String = "", _
                                       Optional ByVal strChildTbl As String = "", Optional ByVal strChildTb2 As String = "", _
                                       Optional ByVal strChildTb3 As String = "", Optional ByVal strChildTb4 As String = "", Optional ByVal nObjectType As SAPbobsCOM.BoUDOObjType = SAPbobsCOM.BoUDOObjType.boud_Document, Optional ByVal blnCanArchive As Boolean = False, Optional ByVal strLogName As String = "")

        Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
        Try
            oUserObjectMD = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
            If oUserObjectMD.GetByKey(strUDO) = 0 Then
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES

                If sFind1 <> "" And sFind2 <> "" Then
                    oUserObjectMD.FindColumns.ColumnAlias = sFind1
                    oUserObjectMD.FindColumns.Add()
                    oUserObjectMD.FindColumns.SetCurrentLine(1)
                    oUserObjectMD.FindColumns.ColumnAlias = sFind2
                    oUserObjectMD.FindColumns.Add()
                End If

                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.LogTableName = ""
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.ExtensionName = ""

                Dim intTables As Integer = 0
                If strChildTbl <> "" Then
                    oUserObjectMD.ChildTables.TableName = strChildTbl
                End If
                If strChildTb2 <> "" Then
                    If strChildTbl <> "" Then
                        oUserObjectMD.ChildTables.Add()
                        intTables = intTables + 1
                    End If
                    oUserObjectMD.ChildTables.SetCurrentLine(intTables)
                    oUserObjectMD.ChildTables.TableName = strChildTb2
                End If
                If strChildTb3 <> "" Then
                    If strChildTb2 <> "" Then
                        oUserObjectMD.ChildTables.Add()
                        intTables = intTables + 1
                    End If
                    oUserObjectMD.ChildTables.SetCurrentLine(intTables)

                    oUserObjectMD.ChildTables.TableName = strChildTb3
                End If
                If strChildTb4 <> "" Then
                    If strChildTb3 <> "" Then
                        oUserObjectMD.ChildTables.Add()
                        intTables = intTables + 1
                    End If
                    oUserObjectMD.ChildTables.SetCurrentLine(intTables)
                    oUserObjectMD.ChildTables.TableName = strChildTb4
                End If
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.Code = strUDO
                oUserObjectMD.Name = strDesc
                oUserObjectMD.ObjectType = nObjectType
                oUserObjectMD.TableName = strTable

                If blnCanArchive Then
                    oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.LogTableName = strLogName
                    oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
                End If

                If oUserObjectMD.Add() <> 0 Then
                    Throw New Exception(oApplication.Company.GetLastErrorDescription)
                End If
            End If

        Catch ex As Exception
            Throw ex
        Finally
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserObjectMD)
            oUserObjectMD = Nothing
            GC.WaitForPendingFinalizers()
            GC.Collect()
        End Try

    End Sub

#End Region

#Region "Public Functions"
    '*************************************************************************************************************
    'Type               : Public Function
    'Name               : CreateTables
    'Parameter          : 
    'Return Value       : none
    'Author             : Manu
    'Created Dt         : 
    'Last Modified By   : 
    'Modified Dt        : 
    'Purpose            : Creating Tables by calling the AddTables & AddFields Functions
    '**************************************************************************************************************
    Public Sub CreateTables()
        Try

            oApplication.SBO_Application.StatusBar.SetText("Initializing Database...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            'oApplication.Company.StartTransaction()


            AddFields("OINV", "EA_LR", "LR No", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("OINV", "EA_SPOT", "Spot", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("OITM", "Flag", "Flag", SAPbobsCOM.BoFieldTypes.db_Alpha, , 1)

            AddFields("OINV", "LandRef", "Landing Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("OINV", "PackingRef", "Packing Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)

            addField("PDN1", "Flag", "Flag for Prodcution", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "N")
            'Modified by Arun
            AddFields("OPDN", "grpoqty", "GRPO Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("OPDN", "packqty", "Packing Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("OPDN", "differqty", "GRPO PACK Differ", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)

            ''Tejas
            AddFields("OPDN", "GRPOCFLFlag", "GRPOFlag", SAPbobsCOM.BoFieldTypes.db_Alpha, , 1)

            AddTables("S_OGINREF", "Packing Reference", SAPbobsCOM.BoUTBTableType.bott_NoObject)

            AddTables("S_OIGN", "Packing Header", SAPbobsCOM.BoUTBTableType.bott_Document)
            AddFields("S_OIGN", "DocDate", "Document Date", SAPbobsCOM.BoFieldTypes.db_Date)
            addField("@S_OIGN", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "D,C,L", "Draft,Confirm,Close", "D")
            AddFields("S_OIGN", "GoodReceipt", "Goods Receipt Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_OIGN", "GoodReceiptNo", "Goods Receipt No", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_OIGN", "GoodsIssue", "Goods Issue Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_OIGN", "GoodsIssueNo", "Goods Issue No", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_OIGN", "PackGoodsIssue", "Packaging Items Goods Issue Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_OIGN", "PackGoodsIssueNo", "Packaging Items Goods Issue No", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_OIGN", "PackGoodsReceipt", "Packaging Items Goods Receipt Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_OIGN", "PackGoodsReceiptNo", "Packaging Items Goods Receipt No", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_OIGN", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Alpha, , 254)
            AddFields("S_OIGN", "GRPONo", "GRPO Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            addField("@S_OIGN", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 30, SAPbobsCOM.BoFldSubTypes.st_Address, "Processing,Repacking,Reglazing", "Processing,Repacking,Reglazing", "Processing")
            AddFields("S_OIGN", "EA_BPLId", "Brand ID", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            addField("@S_OIGN", "sameItem", "Same Item check box", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "N")


            AddTables("S_IGN1", "Packing Issued Items", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            AddFields("S_IGN1", "GRPONo", "GRPO Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_IGN1", "GRPONum", "GRPO DocNum", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_IGN1", "CardName", "Supplier Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 200)
            AddFields("S_IGN1", "EA_LR", "LR No", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN1", "EA_SPOT", "Spot", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN1", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            AddFields("S_IGN1", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 200)
            AddFields("S_IGN1", "WhsCode", "Warehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            AddFields("S_IGN1", "UoM", "UoM", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_IGN1", "UoMName", "UoM Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_IGN1", "Quantity", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN1", "PackingRef", "Packing Issue Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)

            AddFields("S_IGN1", "BatchRef", "Batch Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            addField("@S_IGN1", "IsBatch", "Batch Item", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "N")
            AddFields("S_IGN1", "Grade", "Grade", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_IGN1", "Rate", "Unit Price", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Price)
            AddFields("S_IGN1", "Total", "Total", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Sum)
            AddFields("S_IGN1", "Freight", "Freight", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Sum)
            AddFields("S_IGN1", "Text", "Free Text", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            AddFields("S_IGN1", "PCartCost", "Packing Cost / Carton", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Price)
            AddFields("S_IGN1", "PSlabCost", "Packing Cost /Slab", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Price)

            AddFields("S_IGN1", "Pack1", "Pack1", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN1", "Pack2", "Pack2", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN1", "Glaze", "Glaze", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Percentage)
            AddFields("S_IGN1", "WCost", "Warehouse /Item Cost", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Sum)
            AddFields("S_IGN1", "PCost", "Packing Cost", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Sum)
            AddFields("S_IGN1", "NWeight", "Weight", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN1", "InStk", "In Stock standard", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Sum)
            AddFields("S_IGN1", "BatchRefrence", "BatchRefrence feild", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            AddFields("S_IGN1", "QtyCarton", "Quantity of Carton", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN1", "QtySlab", "Quantity of Slabs", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN1", "posting", "batch selection posting", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)

            oApplication.SBO_Application.StatusBar.SetText("Initializing Database...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            AddTables("S_IGN2", "Packing Receipt Items", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            AddFields("S_IGN2", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            AddFields("S_IGN2", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 200)
            AddFields("S_IGN2", "Pack1", "Pack1", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN2", "Pack2", "Pack2", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN2", "Glaze", "Glaze", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Percentage)
            AddFields("S_IGN2", "NoofSlabs", "No of Slabs", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN2", "MCarton", "M Carton", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN2", "LooseSlabs", "Loose Slabs", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN2", "GWeight", "Gross Weight", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN2", "NWeight", "NWeight", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN2", "ALS", "Actual Loose Slabs", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)

            AddFields("S_IGN2", "WhsCode", "Warehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            AddFields("S_IGN2", "UoM", "UoM", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_IGN2", "UoMName", "UoM Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)

            AddFields("S_IGN2", "CartonsKG", "Carton/Weight (IN KGS)", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN2", "SlabsKG", "Slabs /Weight (IN KGS)", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN2", "SlabsPerCar", "Slabs Per Carton", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN2", "Price", "Price", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Price)
            AddFields("S_IGN2", "Rate", "Rate Per Item Cost", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Price)
            AddFields("S_IGN2", "Yield", "Yield", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Percentage)
            AddFields("S_IGN2", "RYield", "Reprocessing Yield", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Percentage)
            AddFields("S_IGN2", "Quantity", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN2", "PackingRef", "Packing Issue Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            AddFields("S_IGN2", "PCartCost", "Packing Cost / Carton", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Price)
            AddFields("S_IGN2", "PSlabCost", "Packing Cost /Slab", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Price)
            AddFields("S_IGN2", "PTotal", "Packing Total", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Sum)
            AddFields("S_IGN2", "UnitPriceC", "Unit Price /Carton", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Price)
            AddFields("S_IGN2", "UnitPriceS", "Unit Price / Slab", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Price)
            AddFields("S_IGN2", "Total", "Total", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Sum)


            AddFields("S_IGN2", "BatchRef", "Batch Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            addField("@S_IGN2", "IsBatch", "Batch Item", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "N")
            AddFields("S_IGN2", "Text", "Free Text", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            addField("@S_IGN2", "Consume", "Consume one more Carton", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "N")

            AddFields("S_IGN2", "Flag", "Flag", SAPbobsCOM.BoFieldTypes.db_Alpha, , 1)
            AddFields("S_IGN2", "CFLFlag", "CFLFlag", SAPbobsCOM.BoFieldTypes.db_Alpha, , 1)

            AddUDO("US_OIGN", "Packing_UnPacking", "S_OIGN", "DocNum", , "S_IGN1", "S_IGN2", , , SAPbobsCOM.BoUDOObjType.boud_Document, True, "AUS_OIGN")





            AddTables("S_IGN3", "Packing Items", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            AddFields("S_IGN3", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            AddFields("S_IGN3", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 200)
            AddFields("S_IGN3", "WhsCode", "Warehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            AddFields("S_IGN3", "UoM", "UoM", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_IGN3", "UoMName", "UoM Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_IGN3", "Quantity", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN3", "QtySelected", "Selected Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN3", "QtyBalance", "Balance", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN3", "Price", "Price", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Price)
            AddFields("S_IGN3", "BatchRef", "Batch Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            addField("@S_IGN3", "Type", "DocType", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "R,I", "Receipt,Issue", "I")

            oApplication.SBO_Application.StatusBar.SetText("Initializing Database...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            AddFields("OITM", "EA_PACK", "Pack1", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("OITM", "EA_PACK1", "Pack2", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("OITM", "EA_GLAZE", "Glaze", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Percentage)
            AddFields("OITM", "EA_TYPE", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("OITM", "EA_GRADE", "Grade", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)




            'AddTables("S_IGN4", "Packing Batch Details", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            'AddFields("S_IGN4", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            'AddFields("S_IGN4", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 200)
            'AddFields("S_IGN4", "BatchRef", "Batch Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            'AddFields("S_IGN4", "BatchAbs", "BatchAbs", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            'AddFields("S_IGN4", "AbsEntry", "AbsEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            'AddFields("S_IGN4", "WhsCode", "Warehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            'AddFields("S_IGN4", "Quantity", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            'AddFields("S_IGN4", "BatchRef", "Batch Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)


            AddFields("OBTN", "Allocated", "Allocated", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("OBTN", "selectAllocated", "select Allocated", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("OBTN", "DRAFTALLOCATED", "DRAFT ALLOCATED", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("OBTN", "BOEQty", "BOE Batch Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("OBTN", "CustomQty", "Custom Transaction Qty", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)


            oApplication.SBO_Application.StatusBar.SetText("Initializing Database...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            AddTables("S_IGN4", "Packing Selected Batch", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            AddFields("S_IGN4", "BatchRef", "Batch Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN4", "AbsEntry", "Batch AbsEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_IGN4", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            AddFields("S_IGN4", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 200)
            AddFields("S_IGN4", "Qty", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN4", "UoM", "Unit of Measure", SAPbobsCOM.BoFieldTypes.db_Alpha, , 20)
            AddFields("S_IGN4", "WhsCode", "Warehouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 20)
            AddFields("S_IGN4", "BatchNum", "Batch Number", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN4", "SysNumber", "System Attribute", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN4", "PONo", "Production Order Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN4", "SelBatch", "Selected Batch Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN4", "BoENo", "Bill of Entry", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            addField("@S_IGN4", "CANCELED", "Canceled", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "N")
            AddFields("S_IGN4", "InvNo", "Custom Invoice No", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN4", "InvDate", "Custom Invoice Date", SAPbobsCOM.BoFieldTypes.db_Date)
            AddFields("S_IGN4", "BatchRefrence", "BatchRefrence feild", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            AddFields("S_IGN4", "posting", "batch selection posting", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            AddFields("S_IGN4", "DocEntry", "DocEntry reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN4", "Flag", "Flag reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 1)
            AddFields("S_IGN4", "ChangeFlag", "Change Flag reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 1)

            AddTables("S_IGN5", "Document Relationship Map", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            AddFields("S_IGN5", "RMDocentry", "Batch Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN5", "RMDocnum", "Batch AbsEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN5", "RMRemarks", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)

            AddTables("S_IGN6", "Packing Selected Batch draft", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            AddFields("S_IGN6", "dBatchRef", "Batch Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN6", "dAbsEntry", "Batch AbsEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("S_IGN6", "dItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            AddFields("S_IGN6", "dItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 200)
            AddFields("S_IGN6", "dQty", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("S_IGN6", "dUoM", "Unit of Measure", SAPbobsCOM.BoFieldTypes.db_Alpha, , 20)
            AddFields("S_IGN6", "dWhsCode", "Warehouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 20)
            AddFields("S_IGN6", "dBatchNum", "Batch Number", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN6", "dSysNumber", "System Attribute", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN6", "dBoENo", "Bill of Entry", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("S_IGN6", "dBatchRefrence", "BatchRefrence feild", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            AddFields("S_IGN6", "Flag", "Flag reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 1)
            AddFields("S_IGN6", "ChangeFlag", "Change Flag reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 1)

            oApplication.SBO_Application.StatusBar.SetText("Initializing Database...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            AddTables("EA_OUSR", "User Authorization", SAPbobsCOM.BoUTBTableType.bott_Document)
            AddFields("EA_OUSR", "USER_CODE", "User Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            AddFields("EA_OUSR", "U_NAME", "User Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            addField("@EA_OUSR", "Active", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "Y")
            AddFields("EA_OUSR", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            AddFields("EA_OUSR", "ItemCode1", "Item Code2", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("EA_OUSR", "ItemCode2", "Item Code2", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("EA_OUSR", "ItmsGrpCod", "Item Group Code Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("EA_OUSR", "CardCode1", "Business Partner 1", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("EA_OUSR", "CardCode2", "Business Partner 2", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("EA_OUSR", "ItmsGrpCod1", "BP Group Code Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)


            AddTables("EA_USR1", "User Item Authorization", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            AddFields("EA_USR1", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("EA_USR1", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            AddFields("EA_USR1", "ItmsGrpCod", "Item Group", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            addField("@EA_USR1", "Select", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "Y")


            AddTables("EA_USR2", "User BP Authorization", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            AddFields("EA_USR2", "CardCode", "BP  Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("EA_USR2", "CardName", "BP Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            AddFields("EA_USR2", "ItmsGrpCod", "BP Group", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            addField("@EA_USR2", "Select", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "Y")

            AddUDO("UEA_OUSR", "User_Authorization", "EA_OUSR", "DocNum", "U_USER_CODE", "EA_USR1", "EA_USR2", , , SAPbobsCOM.BoUDOObjType.boud_Document, True, "AEA_OUSR")

            oApplication.SBO_Application.StatusBar.SetText("Initializing Database...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            AddTables("EA_OPDN", "Landing Receipt", SAPbobsCOM.BoUTBTableType.bott_Document)
            AddFields("EA_OPDN", "EA_CardCode", "Vendor Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("EA_OPDN", "EA_CardName", "Vendor Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 200)
            AddFields("EA_OPDN", "EA_Spot", "Spot", SAPbobsCOM.BoFieldTypes.db_Alpha, , 20)
            '  addField("@EA_OPDN", "EA_Spot1", "Spot combobox", SAPbobsCOM.BoFieldTypes.db_Alpha, 25, SAPbobsCOM.BoFldSubTypes.st_Address, "KOCHI,KOLLAM,MUNAMBAM", "KOCHI,KOLLAM,MUNAMBAM", "KOCHI")
            addField("@EA_OPDN", "EA_Type", "Purchase Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 25, SAPbobsCOM.BoFldSubTypes.st_Address, "Direct,Regular,Rate", "Direct Purchase,Regular Purchase,Rate Purchase", "Direct")
            addField("@EA_OPDN", "EA_Status", "Document Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "D,C,L", "Draft,Confirm,Cancel", "D")
            AddFields("EA_OPDN", "EA_DocDate", "Document Date", SAPbobsCOM.BoFieldTypes.db_Date)
            AddFields("EA_OPDN", "EA_PostDate", "Posting Date", SAPbobsCOM.BoFieldTypes.db_Date)
            AddFields("EA_OPDN", "EA_GRPORef", "Draft GRPO Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("EA_OPDN", "EA_GRPOEntry", "Actual GRPO Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("EA_OPDN", "EA_APRef", "AP Invoice Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("EA_OPDN", "EA_LANDRef", "Landing Document Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("EA_OPDN", "EA_Owner", "Owner", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            AddFields("EA_OPDN", "EA_Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            AddFields("EA_OPDN", "EA_DocTotal", "Document Total", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Sum)
            AddFields("EA_OPDN", "EA_BPLId", "Brand ID", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("EA_OPDN", "EA_NumAtCard", "Supplier Ref No", SAPbobsCOM.BoFieldTypes.db_Alpha, , 100)
            AddFields("EA_OPDN", "EA_GRPOStatus", "GRPO Status Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 100)
            AddFields("EA_OPDN", "EA_GRPOStatusDoc", "GRPO Status DocEntry Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, , 100)

            AddTables("EA_PDN1", "Landing Receipt Lines", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            AddFields("EA_PDN1", "EA_ItemCode", "ItemCode", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)
            AddFields("EA_PDN1", "EA_ItemName", "ItemName", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            AddFields("EA_PDN1", "EA_GLAZE", "Glaze", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Percentage)
            AddFields("EA_PDN1", "EA_TYPE", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("EA_PDN1", "EA_GRADE", "Grade", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("EA_PDN1", "EA_WHS", "Warehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("EA_PDN1", "EA_UoM", "UoM", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("EA_PDN1", "EA_UoMName", "UoM Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            AddFields("EA_PDN1", "EA_Quantity", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("EA_PDN1", "EA_Rate", "Rate", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Sum)
            AddFields("EA_PDN1", "EA_LineTotal", "Line Total", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Sum)
            AddFields("EA_PDN1", "EA_CostCenter", "CostCenter", SAPbobsCOM.BoFieldTypes.db_Alpha, , 30)


            AddTables("EA_PDN2", "Landing Receipt Attachment", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            AddFields("EA_PDN2", "EA_FilePath", "File Path", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            AddFields("EA_PDN2", "EA_FileName", "File Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            AddUDO("UEA_OPDN", "Landing_Receipt", "EA_OPDN", "DocNum", "U_EA_CardCode", "EA_PDN1", "EA_PDN2", , , SAPbobsCOM.BoUDOObjType.boud_Document, True, "AEA_OPDN")

            AddFields("OCRD", "CFLFlag", "CFLFlag", SAPbobsCOM.BoFieldTypes.db_Alpha, , 1)
            AddFields("OITM", "CFLFlag", "CFLFlag", SAPbobsCOM.BoFieldTypes.db_Alpha, , 1)

            'AddTables("S_COSTCENTERMAP", "CostCenter Mapping", SAPbobsCOM.BoUTBTableType.bott_NoObjectAutoIncrement)

            AddFields("OBPL", "CostCenter", "CostCenter", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("OWHS", "CFLFlag", "CFLFlag", SAPbobsCOM.BoFieldTypes.db_Alpha, , 1)

            oApplication.SBO_Application.StatusBar.SetText("Initializing Database...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            AddTables("EA_OBOM", "Packing Items", SAPbobsCOM.BoUTBTableType.bott_Document)
            AddFields("EA_OBOM", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("EA_OBOM", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            AddFields("EA_OBOM", "Quantity", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("EA_OBOM", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            AddFields("EA_OBOM", "CFLFlag", "CFLFlag", SAPbobsCOM.BoFieldTypes.db_Alpha, , 1)

            AddFields("EA_OBOM", "CTotal", "Carton Total Price", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Sum)
            AddFields("EA_OBOM", "STotal", "Slab Total Price", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Sum)
            addField("@EA_OBOM", "Default", "Default", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "N")
            addField("@EA_OBOM", "Inactive", "Inactive", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "N")

            AddTables("EA_BOM1", "Packing Items Carton Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            AddFields("EA_BOM1", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("EA_BOM1", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            AddFields("EA_BOM1", "Quantity", "Base Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("EA_BOM1", "Price", "Price Per Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Price)
            addField("@EA_BOM1", "Exclude", "Exclude for Consumption", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "N")
            addField("@EA_BOM1", "ExcludeGR", "Include Extra for Goods Receipt", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "N")


            AddTables("EA_BOM2", "Packing Items Slabs Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            AddFields("EA_BOM2", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("EA_BOM2", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, , 250)
            AddFields("EA_BOM2", "Quantity", "Base Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Quantity)
            AddFields("EA_BOM2", "Price", "Price Per Quantity", SAPbobsCOM.BoFieldTypes.db_Float, , , SAPbobsCOM.BoFldSubTypes.st_Price)
            addField("@EA_BOM2", "Exclude", "Exclude for Consumption", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "N")
            addField("@EA_BOM2", "ExcludeGR", "Exclude for Goods Receipt", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "N")

            AddUDO("UEA_OBOM", "Packing_Items", "EA_OBOM", "DocNum", "U_ItemCode", "EA_BOM1", "EA_BOM2", , , SAPbobsCOM.BoUDOObjType.boud_Document, True, "AEA_OBOM")

            addField("OPLN", "CCRPrice", "CCR Price List", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_Address, "Y,N", "Yes,No", "N")
            ''created the udt with no object to denote the warehouse 
            AddTables("EA_OWAR", "Warehouse Refresh", SAPbobsCOM.BoUTBTableType.bott_NoObjectAutoIncrement)
            AddFields("EA_OWAR", "Flag", "Flag", SAPbobsCOM.BoFieldTypes.db_Numeric, , 1, , , 0)
            AddFields("EA_OWAR", "FCount", "Flag Count", SAPbobsCOM.BoFieldTypes.db_Numeric, , 10)
            AddFields("EA_OWAR", "PRDoc", "PR Docentry", SAPbobsCOM.BoFieldTypes.db_Alpha, , 50)
            AddFields("EA_OWAR", "CDate", "Create Date", SAPbobsCOM.BoFieldTypes.db_Date, , 8)
            AddFields("EA_OWAR", "CTime", "Create Time", SAPbobsCOM.BoFieldTypes.db_Alpha, , 10)
            CreateUDO()
            oApplication.Utilities.Message("Database Creation Completed...", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

        Catch ex As Exception
            'If oApplication.Company.InTransaction() Then
            '    oApplication.Company.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
            'End If
            Throw ex
        Finally
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try
    End Sub

    Public Sub CreateUDO()
        Try
           
            'Add UDO


        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    
#End Region

End Class
