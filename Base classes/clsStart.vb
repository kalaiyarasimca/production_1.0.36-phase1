Imports System.IO

Public Class clsStart

    Shared Sub Main()
        Dim oMenuItem As SAPbouiCOM.MenuItem
        Try
            Try
                oApplication = New clsListener
                oApplication.Utilities.Connect()
                oApplication.SetFilter()
                With oApplication.Company.GetCompanyService
                    CompanyDecimalSeprator = .GetAdminInfo.DecimalSeparator
                    CompanyThousandSeprator = .GetAdminInfo.ThousandsSeparator
                End With
                If oApplication.Company.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB Then
                    blnIsHANA = True
                Else
                    blnIsHANA = False
                End If
                strUserDefaultWhs = oApplication.Utilities.getUserDefaultWarehouse()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                Exit Sub
            End Try
            oApplication.Utilities.AddRemoveMenus("Menu.xml")
            oMenuItem = oApplication.SBO_Application.Menus.Item("mnu_MAX_102")
            oMenuItem.Image = Application.StartupPath & "\Logo.PNG"
            oApplication.Utilities.Message("Packing and UnPacking Addon Connected Successfully..", SAPbouiCOM.BoStatusBarMessageType.smt_Success)
            oApplication.Utilities.NotifyAlert()
            System.Windows.Forms.Application.Run()
        Catch ex As Exception
            oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            System.Windows.Forms.Application.Exit()
        End Try
    End Sub
End Class
