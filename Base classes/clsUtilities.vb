
Imports System.Collections.Specialized
'Imports CrystalDecisions.CrystalReports.Engine
'Imports CrystalDecisions.Shared
Imports System.Drawing
Imports System.IO
Imports System.Threading


Public Class clsUtilities


    Private strThousSep As String = ","
    Private strDecSep As String = "."
    Private intQtyDec As Integer = 3
    Private FormNum As Integer
    ' Dim cryRpt As New ReportDocument
    Private ds As New Barcode       '(dataset)
    Private oDRow As DataRow
    Public strFilterFile As String = String.Empty
    Private oRecordSet As SAPbobsCOM.Recordset


    Public Sub New()
        MyBase.New()
        FormNum = 1
    End Sub

    Public Sub UpdateBatchOBTN_CINV3NotExists(aDocEntry As String, aForm As SAPbouiCOM.Form)
        ' aForm.Freeze(True)
        Dim oCustomRS, oBatchRS, oBoERS, oPORS, oPOReturn, oBOERecordSet As SAPbobsCOM.Recordset
        oCustomRS = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oBatchRS = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oBoERS = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oBOERecordSet = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oPORS = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oPOReturn = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Dim strBatchRef, strBatchBOE, strItemCode, strUpdateQuery As String
        Dim strBoEDocEntry, strCustomInvoiceNo, strFGItemCode, strFGItemName, s1 As String

        If blnIsHANA = True Then
            oCustomRS.DoQuery("Select Count(*) ""BoEQty"",T0.""U_BatchRef"" from ""@S_IGN3"" T0   where T0.""U_Type""='I' and  T0.""U_BatchRef""<>'' and   ""U_BatchRef"" NOT IN (Select IFNULL(""U_BatchRef"",'X') from ""@S_IGN1"")  Group By T0.""U_BatchRef""")
        Else
            oCustomRS.DoQuery("Select Count(*) ""BoEQty"",T0.""U_BatchRef"" from ""@S_IGN3"" T0   where T0.""U_Type""='I' and  T0.""U_BatchRef""<>'' and  ""U_BatchRef"" NOT IN (Select ISNULL(""U_BatchRef"",'X') from ""@S_IGN1"")  Group By T0.""U_BatchRef""")
            Dim st As String = "Select Count(*) ""BoEQty"",T0.""U_BatchRef"" from ""@S_IGN3"" T0   where  T0.""U_Type""='I' and  T0.""U_BatchRef""<>'' and  ""U_BatchRef"" NOT IN (Select ISNULL(""U_BatchRef"",'X') from ""@S_IGN1"")  Group By T0.""U_BatchRef"""
        End If

        Dim dblSum As Double
        Dim oStatic As SAPbouiCOM.StaticText
        For intRow As Integer = 0 To oCustomRS.RecordCount - 1
            oStatic = aForm.Items.Item("6").Specific
            strBatchRef = oCustomRS.Fields.Item("U_BatchRef").Value
            oStatic.Caption = "Processing Batch Ref: " & strBatchRef
            oPORS.DoQuery("Update ""@S_IGN4"" set ""U_CANCELED""='Y' where   ""U_BatchRef""='" & strBatchRef & "'")
            oBOERecordSet.DoQuery("Select Count(*) ""BoEQty"",T0.""U_ItemCode"",T0.""U_AbsEntry""  from ""@S_IGN4"" T0  where   ""U_BatchRef""='" & strBatchRef & "'   Group By T0.""U_ItemCode"",T0.""U_AbsEntry""")
            For intLoop1 As Integer = 0 To oBOERecordSet.RecordCount - 1
                Dim strSysNumber1 As String = oBOERecordSet.Fields.Item("U_AbsEntry").Value
                oStatic.Caption = "Processing Batch Ref: " & strBatchRef & " and BatchNo: " & strSysNumber1
                If strSysNumber1 <> "" Then
                    UpdateBatchCustomQty(strSysNumber1)
                End If

                oPORS.DoQuery("Delete from  ""@S_IGN4"" Where    ""U_CANCELED""='Y' and ""U_BatchRef""='" & strBatchRef & "'")
                oBOERecordSet.MoveNext()
            Next
            oCustomRS.MoveNext()
        Next

    End Sub
    Public Sub UpdateBatchCustomQty(aSysNumber As String)
        Dim oBatchRS As SAPbobsCOM.Recordset
        oBatchRS = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Dim dblCustomQty As Double
        oBatchRS.DoQuery("Select Sum(""U_Qty"") from ""@S_IGN4"" where ""U_CANCELED""='N' and  ""U_AbsEntry""='" & aSysNumber & "' and ""Code""=""Name""")
        dblCustomQty = oBatchRS.Fields.Item(0).Value
        oBatchRS.DoQuery("Update OBTN set ""U_Allocated""='" & dblCustomQty & "' where ""AbsEntry""=" & CInt(aSysNumber))
        oBatchRS.DoQuery("Update OBTN set ""U_selectAllocated""='" & dblCustomQty & "' where ""AbsEntry""=" & CInt(aSysNumber))
    End Sub

    Public Function getUserDefaultWarehouse() As String
        Dim oRec As SAPbobsCOM.Recordset
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRec.DoQuery("SELECT T1.""Warehouse"" FROM OUSR T0  INNER JOIN OUDG T1 ON T0.""DfltsGroup"" = T1.""Code"" where ""USERID""=" & oApplication.Company.UserSignature)
        If oRec.RecordCount > 0 Then
            Return (oRec.Fields.Item(0).Value)
        End If
        Return ""
    End Function
    ''Modify By S rajeswari
    Public Function getTopwarhouse(ByVal stritem As String) As String
        Dim oRec As SAPbobsCOM.Recordset
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRec.DoQuery("Select T1.WhsCode from OITW T1   where  T1.ItemCode='" & stritem & "' and T1.OnHand<>0")
        If oRec.RecordCount > 0 Then
            Return (oRec.Fields.Item(0).Value)
        End If
        Return ""
    End Function

    Public Function getUserDefaultBranch() As String
        Dim oRec As SAPbobsCOM.Recordset
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRec.DoQuery("SELECT T1.""BPLId"" FROM OUSR T0  INNER JOIN OUDG T1 ON T0.""DfltsGroup"" = T1.""Code"" where ""USERID""=" & oApplication.Company.UserSignature)
        If oRec.RecordCount > 0 Then
            Return (oRec.Fields.Item(0).Value)
        End If
        Return ""
    End Function
    Public Sub fillopen()
        Dim mythr As New System.Threading.Thread(AddressOf ShowFileDialog)
        mythr.SetApartmentState(Threading.ApartmentState.STA)
        mythr.Start()
        mythr.Join()

    End Sub
    Public Function GetAttachmentFolder() As String
        Dim oRec As SAPbobsCOM.Recordset
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Dim SqlQuery, Targetpath As String
        If blnIsHANA = True Then
            SqlQuery = "select IFNULL(""AttachPath"",'') AS ""AttachPath"" from OADP"
        Else
            SqlQuery = "select ISNULL(""AttachPath"",'') AS ""AttachPath"" from OADP"
        End If
        oRec.DoQuery(SqlQuery)
        Targetpath = oRec.Fields.Item(0).Value
        If Targetpath = "" Then
            oApplication.Utilities.Message("Attachment file is not defined", SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return ""

        End If
        Return Targetpath
    End Function
    Public Sub MoveFilestoAttachFolder(aDocEntry As String, atableName As String)
        Dim oRec As SAPbobsCOM.Recordset
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Dim SqlQuery, Targetpath, AttachFolder As String
        If blnIsHANA = True Then
            SqlQuery = "select IFNULL(""AttachPath"",'') AS ""AttachPath"" from OADP"
        Else
            SqlQuery = "select ISNULL(""AttachPath"",'') AS ""AttachPath"" from OADP"
        End If
        oRec.DoQuery(SqlQuery)
        Targetpath = oRec.Fields.Item(0).Value
        If Targetpath = "" Then
            oApplication.Utilities.Message("Attachment file is not defined", SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Exit Sub
        End If
        oRec.DoQuery("Select * from ""@" & atableName & """ where ""DocEntry""=" & aDocEntry)
        AttachFolder = Targetpath
        For intRow As Integer = 0 To oRec.RecordCount - 1
            Dim lblfile As String = oRec.Fields.Item("U_EA_FileName").Value
            Dim lblFilePath As String = oRec.Fields.Item("U_EA_FilePath").Value
            If lblFilePath <> "" Then
                Dim fileName As String = lblfile
                If Targetpath <> "" Then
                    Targetpath = AttachFolder & lblfile
                    Try
                        If File.Exists(lblFilePath) Then
                            If File.Exists(Targetpath) Then
                                File.Delete(Targetpath)
                            End If
                            '   File.Move(lblFilePath, Targetpath)
                            File.Copy(lblFilePath, Targetpath)
                        End If
                    Catch ex As Exception
                    End Try
                End If
            End If
            oRec.MoveNext()
        Next
    End Sub
    
    Public Sub FolerOpen()
        Dim mythr As New System.Threading.Thread(AddressOf ShowFolderDialog)
        mythr.SetApartmentState(Threading.ApartmentState.STA)
        mythr.Start()
        mythr.Join()
    End Sub
    Private Sub ShowFolderDialog()
        Try
            Dim oDialogBox As New FolderBrowserDialog
            Dim oProcesses() As Process
            Dim oWinForm As New System.Windows.Forms.Form()
            oWinForm.TopMost = True
            oWinForm.Show()
            Try
                oProcesses = Process.GetProcessesByName("SAP Business One")
                If oProcesses.Length <> 0 Then
                    For i As Integer = 0 To oProcesses.Length - 1
                        Dim MyWindow As New WindowWrapper(oProcesses(i).MainWindowHandle)
                        SetForegroundWindow(FindWindow(Nothing, gWindowText))
                        If oDialogBox.ShowDialog(MyWindow) = DialogResult.OK Then
                            strSelectedFolderPath = oDialogBox.SelectedPath
                            strSelectedFilepath = oDialogBox.SelectedPath
                            Exit For
                        Else
                            strSelectedFolderPath = ""
                            strSelectedFilepath = ""
                            Exit For
                        End If
                    Next
                End If
            Catch ex As Exception
                oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Finally
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub LoadUDFCombo(aForm As SAPbouiCOM.Form, aUID As String, aField As String, aTable As String)
        Try
            aForm.Freeze(True)
            Dim oCOmbo As SAPbouiCOM.ComboBox
            oCOmbo = aForm.Items.Item(aUID).Specific
            For intRow As Integer = oCOmbo.ValidValues.Count - 1 To 0 Step -1
                Try
                    oCOmbo.ValidValues.Remove(intRow, SAPbouiCOM.BoSearchKey.psk_Index)
                Catch ex As Exception
                End Try
            Next
            Dim oRec As SAPbobsCOM.Recordset
            oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim strSQL As String
            oCOmbo.ValidValues.Add("", "")
            strSQL = "select T1.""FldValue"",T1.""Descr"" from CUFD T0 inner Join UFD1 T1 on T1.""FieldID""=T0.""FieldID""  and T1.""TableID""=T0.""TableID""   where T0.""TableID""='" & aTable & "' and ""AliasID""='" & aField & "'"
            oRec.DoQuery(strSQL)
            For intloop As Integer = 0 To oRec.RecordCount - 1
                oCOmbo.ValidValues.Add(oRec.Fields.Item(0).Value, oRec.Fields.Item(1).Value)
                oRec.MoveNext()
            Next
            oCOmbo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
            oCOmbo.Select(0, SAPbouiCOM.BoSearchKey.psk_Index)
            aForm.Items.Item(aUID).DisplayDesc = True
            aForm.Freeze(False)
        Catch ex As Exception
            oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            aForm.Freeze(False)
        End Try
    End Sub
    Public Sub UpdateActualHours(aDocEntry As Integer)
        Dim oRec, oRec1, oRec2 As SAPbobsCOM.Recordset
        Dim dblHours, dblQty As Double
        Dim intLineID As Integer
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRec1 = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRec2 = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRec.DoQuery("Select * from ""@S_CADE1"" where ""U_Status""='O' and  ""DocEntry""=" & aDocEntry)
        For intRow As Integer = 0 To oRec.RecordCount - 1
            intLineID = oRec.Fields.Item("LineId").Value
            dblHours = oRec.Fields.Item("U_Qty").Value
            dblQty = ConvertHoursIntoDecimal(dblHours)
            oRec1.DoQuery("Update ""@S_CADE1"" set ""U_SQty""='" & dblQty & "' where ""DocEntry""=" & aDocEntry & " and ""LineId""=" & intLineID)
            oRec.MoveNext()
        Next
    End Sub
    Public Function ConvertHoursIntoDecimal(aTime As String) As Decimal
        Dim D As String
        Dim TB As String()
        Dim result As Double
        'For example, it can be at a string
        If aTime = "0" Then
            Return 0
        End If
        D = aTime
        TB = Split(D, ":")
        TB = Split(D, ".")
        If TB.Length > 1 Then
            result = TB(0) + ((TB(1) * 100) / 60) / 100
        Else
            result = TB(0)
        End If
        Return result
    End Function
    Public Function Return_CardCode(ByVal UDFvalue As String)
        Try
            Dim StrSql As String
            Dim ObjOCRD As SAPbobsCOM.Recordset
            ObjOCRD = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            StrSql = "Select * from OCRD where ""U_AltCode""='" & UDFvalue & "'"
            ObjOCRD.DoQuery(StrSql)
            If ObjOCRD.EoF = False Then
                Return ObjOCRD.Fields.Item("CardCode").Value
            Else
                Return ""
            End If
        Catch ex As Exception
            oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Function
    Public Function GetDateSeparator() As String
        Dim oRec As SAPbobsCOM.Recordset
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRec.DoQuery("Select * from ""@SJE_SETUP""")
        If oRec.RecordCount > 0 Then
            Return oRec.Fields.Item("U_DateSep").Value
        Else
            Return "-"
        End If
    End Function
    Public Function getDateFormat() As String
        Dim oRec As SAPbobsCOM.Recordset
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRec.DoQuery("Select * from ""@S_OVADA""")
        If oRec.RecordCount > 0 Then
            Return oRec.Fields.Item("U_DF1").Value
        Else
            Return "-"
        End If
    End Function
    Public Function getFileDelimiter() As String
        Dim oRec As SAPbobsCOM.Recordset
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRec.DoQuery("Select * from ""@SJE_SETUP""")
        If oRec.RecordCount > 0 Then
            Return oRec.Fields.Item("U_Delimiter").Value
        Else
            Return ";"
        End If
    End Function

    Public Function UpdatePriceFromWizard(aFromdate As Date, aToDate As Date, aFromCardCode As String, aToCardCode As String) As Boolean
        Try



            Dim oRec, oRec1 As SAPbobsCOM.Recordset
            Dim strItemcode, strCardCode, strQty As String
            Dim dtDate As Date
            Dim dblPrice, dblTotal, dblQty As Double
            Dim strBPCondition As String
            If aFromCardCode <> "" Then
                strBPCondition = " ""U_CardCode"">='" & aFromCardCode & "'"
            Else
                strBPCondition = " 1=1"
            End If
            If aToCardCode <> "" Then
                strBPCondition = strBPCondition & " and ""U_CardCode""<='" & aToCardCode & "'"
            Else
                strBPCondition = strBPCondition & " and 1=1"
            End If
            strBPCondition = "(" & strBPCondition & ")"

            oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRec1 = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRec.DoQuery("Select * from ""@S_CADE1"" where " & strBPCondition & " and ""U_FileDate"" between '" & aFromdate.ToString("yyyy-MM-dd") & "' and '" & aToDate.ToString("yyyy-MM-dd") & "'")
            Message("Processing....", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            For intRow As Integer = 0 To oRec.RecordCount - 1
                strItemcode = oRec.Fields.Item("U_ItemCode").Value
                strCardCode = oRec.Fields.Item("U_CardCode").Value
                strQty = oRec.Fields.Item("U_Qty").Value
                dtDate = oRec.Fields.Item("U_FileDate").Value
                dblQty = oApplication.Utilities.getDocumentQuantity(strQty)
                If dblQty > 0 Then
                    dblPrice = TestMethod_GetItemPrice(strCardCode, strItemcode, dblQty, dtDate)
                Else
                    dblPrice = 0
                End If
                dblTotal = dblPrice * dblQty
                If blnIsHANA = True Then
                    oRec1.DoQuery("select * from SPP1 where ""CardCode""='" & strCardCode & "' and ""ItemCode""='" & strItemcode & "' and '" & dtDate.ToString("yyyy-MM-dd") & "' between ""FromDate"" and ifnull(""ToDate"",'" & dtDate.ToString("yyyy-MM-dd") & "')")
                Else
                    oRec1.DoQuery("select * from SPP1 where ""CardCode""='" & strCardCode & "' and ""ItemCode""='" & strItemcode & "' and '" & dtDate.ToString("yyyy-MM-dd") & "' between ""FromDate"" and isnull(""ToDate"",'" & dtDate.ToString("yyyy-MM-dd") & "')")
                End If
                Dim dtpriceDate As Date
                If oRec1.RecordCount > 0 Then
                    dtpriceDate = oRec1.Fields.Item("FromDate").Value
                Else
                    '  dtpriceDate = 
                    dtpriceDate = New DateTime(2010, 1, 1)
                End If
                oRec1.DoQuery("Update ""@S_CADE1"" set ""U_PriceStartDate""='" & dtpriceDate.ToString("yyyy-MM-dd") & "', ""U_Total""='" & dblTotal & "',""U_Price""='" & dblPrice & "' where ""DocEntry""=" & oRec.Fields.Item("DocEntry").Value & " and ""LineId""=" & oRec.Fields.Item("LineId").Value)
                oRec.MoveNext()
            Next
            Message("Operation completed succesfully....", SAPbouiCOM.BoStatusBarMessageType.smt_Success)
        Catch ex As Exception
            Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Function


    Public Function TestMethod_GetItemPrice(ByVal cardCode As String, ByVal itemCode As String, ByVal amount As Single, ByVal refDate As Date) As Double
        Dim vObj As SAPbobsCOM.SBObob
        Dim rs As SAPbobsCOM.Recordset
        Dim strResult As String
        Dim errResult As String
        vObj = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge)
        rs = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        rs = vObj.GetItemPrice(cardCode, itemCode, amount, refDate)
        If rs.RecordCount <= 0 Then
            Return 0
        Else
            Return rs.Fields.Item(0).Value

        End If

    End Function

    Public Function getNumberingSeries(aDate As Date, aType As String, aBranch As String) As Integer
        'If CheckMulitiBranch() = False Then
        '    Return -1
        'End If
        Dim oSeries1 As SAPbobsCOM.Recordset
        oSeries1 = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        'oSeries1.DoQuery("select ""BPLId"",""ObjectCode"",""Series""  from NNM1 where ""BPLId""=" & aBranch & " and ""ObjectCode""='24'")
        'If oSeries1.RecordCount > 0 Then
        '    Return oSeries1.Fields.Item("Series").Value
        'Else
        '    Return -1
        'End If

        Dim strQuery As String
        If CheckMulitiBranch() = False Then
            strQuery = "SELECT T0.""BPLId"",T0.""ObjectCode"",T0.""Series""   FROM NNM1 T0  INNER JOIN OPID T1 ON T0.""Indicator"" = T1.""Indicator"" INNER JOIN OFPR T2 ON T1.""Indicator"" = T2.""Indicator"" WHERE  T0.""ObjectCode"" ='" & aType & "' and '" & aDate.ToString("yyyy-MM-dd") & "' between T2.""F_RefDate"" and  T2.""T_RefDate"" and ""Locked"" = 'N'"
        Else
            strQuery = "SELECT T0.""BPLId"",T0.""ObjectCode"",T0.""Series""   FROM NNM1 T0  INNER JOIN OPID T1 ON T0.""Indicator"" = T1.""Indicator"" INNER JOIN OFPR T2 ON T1.""Indicator"" = T2.""Indicator"" WHERE T0.""BPLId""=" & aBranch & " and T0.""ObjectCode"" ='" & aType & "' and '" & aDate.ToString("yyyy-MM-dd") & "' between T2.""F_RefDate"" and  T2.""T_RefDate"" and ""Locked"" = 'N'"
        End If
        oSeries1.DoQuery(strQuery)
        If oSeries1.RecordCount > 0 Then
            Return oSeries1.Fields.Item("Series").Value
        Else
            '  Return -1
            Dim oCmpSrv As SAPbobsCOM.CompanyService
            Dim oSeriesService As SAPbobsCOM.SeriesService
            Dim oSeriesCollection As SAPbobsCOM.SeriesCollection
            Dim oSeries As SAPbobsCOM.Series
            Dim oDocumentTypeParams As SAPbobsCOM.DocumentTypeParams
            Dim i As Integer

            'get company service
            oCmpSrv = oApplication.Company.GetCompanyService
            'get series service
            oSeriesService = oCmpSrv.GetBusinessService(SAPbobsCOM.ServiceTypes.SeriesService)
            'get series collection
            oSeriesCollection = oSeriesService.GetDataInterface(SAPbobsCOM.SeriesServiceDataInterfaces.ssdiSeriesCollection)
            'get Document Type Params
            oDocumentTypeParams = oSeriesService.GetDataInterface(SAPbobsCOM.SeriesServiceDataInterfaces.ssdiDocumentTypeParams)
            'set the document type
            '(e.g. SaleInvoice=13 , BoObjectTypes has all document types)
            oDocumentTypeParams.Document = aType

            'get series collection
            oSeries = oSeriesService.GetDefaultSeries(oDocumentTypeParams)
            Return oSeries.Series
            'For i = 0 To oSeriesCollection.Count - 1
            '    'print the series name
            '    Debug.WriteLine(oSeries.Name)
            '    'print the series first number
            '    Debug.WriteLine(oSeries.InitialNumber)

            'Next
        End If
    End Function

    Public Function getDefaultVat(aChoice As String) As String
        Dim strQuery As String
        strQuery = "Select ""DfSVatServ"" ,""DfSVatItem"" from OADM"
        Dim oTest As SAPbobsCOM.Recordset
        oTest = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTest.DoQuery(strQuery)
        If aChoice = "Item" Then
            Return oTest.Fields.Item(1).Value
        Else
            Return oTest.Fields.Item(0).Value

        End If
    End Function

    Public Function CheckMulitiBranch() As Boolean
        Dim oMulBranch As SAPbobsCOM.Recordset
        oMulBranch = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oMulBranch.DoQuery("Select ""MltpBrnchs"" from OADM")
        If oMulBranch.Fields.Item(0).Value = "Y" Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Sub setEditText(ByVal aForm As SAPbouiCOM.Form, ByVal aItem As String, ByVal aValue As String)
        Dim oEdit As SAPbouiCOM.EditText
        oEdit = aForm.Items.Item(aItem).Specific
        oEdit.String = aValue
    End Sub

    Friend Sub ButtonEnableMode(ByVal objForm As SAPbouiCOM.Form, ByVal ItemUId As String, ByVal strMode As Boolean)
        Select Case strMode
            Case True
                objForm.Items.Item(ItemUId).Enabled = True
            Case False
                objForm.Items.Item(ItemUId).Enabled = False
        End Select
    End Sub

#Region "MI Integration"

    Public Sub MIINtegration(aCompany As SAPbobsCOM.Company, aForm As SAPbouiCOM.Form)
        Dim dtDateTime, strTransID, strTRGFileName, strFilename, strExportFilePaty, strSelectedFilepath, strTypefromUDT As String
        Dim dtJEDate, FILedatetiem As Date
        Dim strJNo As String = ""
        Dim strCheckNo As String = ""
        Dim oPath, oDoc As SAPbobsCOM.Recordset
        Dim strGLog, strSLog, strFLog As String
        oPath = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oDoc = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        strExportFilePaty = ""
        strGLog = ""
        strSLog = ""
        strFLog = ""
        Dim oStatic As SAPbouiCOM.StaticText
        oStatic = aForm.Items.Item("50").Specific
        oStatic.Caption = "Processing..."
        oPath.DoQuery("Select * from ""@T_OIFS""")
        If oPath.RecordCount > 0 Then
            While Not oPath.EoF
                Dim strType As String = oPath.Fields.Item("U_Type").Value
                strTypefromUDT = strType
                strExportFilePaty = oPath.Fields.Item("U_OPath").Value
                strGLog = oPath.Fields.Item("U_LOGGPath").Value
                strSLog = oPath.Fields.Item("U_LOGSPath").Value
                strFLog = oPath.Fields.Item("U_LOGFPath").Value
                'Deleting the Files.
                Try
                    Dim strLogFileName_Success As String = "MI Integration_Success_" & strTypefromUDT & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                    Dim strLogFileName_Failure As String = "MI Integration_Failure_" & strTypefromUDT & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                    Dim strSPath_L As String = strSLog & "\" & strLogFileName_Success & ".txt"
                    Dim strFPath_L As String = strFLog & "\" & strLogFileName_Failure & ".txt"
                    If File.Exists(strSPath_L) Then File.Delete(strSPath_L)
                    If File.Exists(strFPath_L) Then File.Delete(strFPath_L)
                Catch ex As Exception
                End Try
                Dim oTemp2 As SAPbobsCOM.Recordset
                oTemp2 = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                Dim GPath, oPath1 As String
                strSelectedFilepath = strExportFilePaty
                Dim strIPath As String = strExportFilePaty ' oEditText.String ''"" 'System.Configuration.ConfigurationManager.AppSettings.Item("IPath")
                Dim strSPath As String = strExportFilePaty & "\Success" ' "" 'System.Configuration.ConfigurationManager.AppSettings.Item("SPath")
                Dim strError As String = strExportFilePaty & "\Error"
                Dim di As New IO.DirectoryInfo(strIPath)
                Dim aryFi As IO.FileInfo() = di.GetFiles("*.txt")
                Dim strLogFileName1 As String = "MI Integration_" & strTypefromUDT & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                If Directory.Exists(strExportFilePaty) = False Then
                    Directory.CreateDirectory(strExportFilePaty)
                End If
                GPath = GetGeneralLogPath(strType, aCompany)

                traceService(strLogFileName1, GPath, "********************************************")
                If strTypefromUDT = "JE" Then
                    traceService(strLogFileName1, GPath, "Importing GL Files  Process  Started :  @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                Else
                    traceService(strLogFileName1, GPath, "Importing Pricing Files  Process  Started :  @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                End If
                traceService(strLogFileName1, GPath, "********************************************")
                For Each fi As FileInfo In aryFi
                    oPath1 = GetOpenPath(strType, aCompany)
                    Dim txtRows() As String
                    Dim strDPath As String = oPath1 & "\" & fi.Name
                    Dim strImportFile As String = oPath1 & "\" & fi.Name
                    txtRows = System.IO.File.ReadAllLines(strDPath)
                    Dim intRow1 As Integer = 0
                    For Each txtrow As String In txtRows
                        If intRow1 = 0 Then
                            Dim strFileType As String = txtrow.Substring(2, 20)
                            If strFileType.Trim() = "SAP PRICING" Then
                                strType = "AR"
                            ElseIf strFileType.Trim() = "SAP SETTLEMENT GL" Then
                                strType = "JE"
                            End If
                            Exit For
                        End If
                    Next
                    If strTypefromUDT = strType Then
                        traceService(strLogFileName1, GPath, "Importing File : " & strImportFile & " start  :  @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                        oStatic = aForm.Items.Item("50").Specific
                        oStatic.Caption = "Processing file : " & fi.Name
                        Dim blnfileImport As Boolean = False
                        If strType = "JE" Then
                            Dim oTe As SAPbobsCOM.Recordset
                            oTe = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oTe.DoQuery("Select * from ""@T_OIHT"" where ""U_FileName1""='" & fi.Name & "'")
                            If oTe.RecordCount > 0 Then
                                traceService(strLogFileName1, GPath, "File : " & strImportFile & " Already Imported  : " & fi.Name & " :  @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                MoveToFolder(strType, fi.Name, "E", oApplication.Company)
                                blnfileImport = True
                            End If
                        End If

                        If blnfileImport = False Then
                            If GetTextDatatoOT(aCompany, strType, fi.Name, strTypefromUDT, aForm) = True Then
                            End If
                        End If
                        traceService(strLogFileName1, GPath, "Importing File : " & strImportFile & "  Completed :  @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                    End If
                Next
                traceService(strLogFileName1, GPath, "********************************************")
                traceService(strLogFileName1, GPath, "Import  Process  Completed :  @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                traceService(strLogFileName1, GPath, "********************************************")

                'Opening the Files.
                Try
                    Dim strLogFileName As String = "MI Integration_" & strTypefromUDT & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                    Dim strLogFileName_Success As String = "MI Integration_Success_" & strTypefromUDT & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                    Dim strLogFileName_Failure As String = "MI Integration_Failure_" & strTypefromUDT & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                    Dim strGPath_L As String = strGLog & "\" & strLogFileName & ".txt"
                    Dim strSPath_L As String = strSLog & "\" & strLogFileName_Success & ".txt"
                    Dim strFPath_L As String = strFLog & "\" & strLogFileName_Failure & ".txt"
                    If File.Exists(strGPath_L) Then Process.Start(strGPath_L)
                    If File.Exists(strSPath_L) Then Process.Start(strSPath_L)
                    If File.Exists(strFPath_L) Then Process.Start(strFPath_L)
                Catch ex As Exception
                End Try
                oPath.MoveNext()

            End While
            oStatic = aForm.Items.Item("50").Specific
            oStatic.Caption = "Processing Completed"
        End If
        

    End Sub

    Private Function GetTextDatatoOT(ByVal aCompany As SAPbobsCOM.Company, ByVal strType As String, aFileName As String, aTypeFromUDT As String, aform As SAPbouiCOM.Form) As Boolean  'Transferring Data from Text file to No Object Table
        Dim FName As String
        Dim _retProcess As Boolean = False
        Dim _retVal As Boolean = True
        Dim _oDt_H As New DataTable
        Dim _oDt_D As New DataTable
        Dim _oDt_F As New DataTable
        Dim intID_S As Integer = 0
        Dim intID_F As Integer = 0
        Dim intStatus As Integer
        Dim OPath As String '" & strTypefromUDT & "_" &
        Dim GPath As String
        Dim strLogFileName As String = "MI Integration_" & strType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
        Dim Ostatic As SAPbouiCOM.StaticText
        Dim intEmpCount As Integer = 0
       
        Try
            Dim oDelRs As SAPbobsCOM.Recordset
            oDelRs = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            If strType = "JE" Then
                '  oDelRs.DoQuery("Delete from ""@T_OIHT""")
                '  oDelRs.DoQuery("Delete from ""@T_OIDT""")
                '  oDelRs.DoQuery("Delete from ""@T_OIFT""")

                OPath = GetOpenPath(strType, aCompany)
                GPath = GetGeneralLogPath(strType, aCompany)
                _oDt_H.TableName = "HD"
                _oDt_H.Columns.Add("RecordID", GetType(String)).Caption = "1-2-2"
                _oDt_H.Columns.Add("FileName", GetType(String)).Caption = "3-22-20"
                _oDt_H.Columns.Add("ICode", GetType(String)).Caption = "23-27-5"
                _oDt_H.Columns.Add("FRefNo", GetType(String)).Caption = "28-39-12"
                _oDt_H.Columns.Add("FileDate", GetType(String)).Caption = "40-47-8"

                _oDt_D.TableName = "DE"
                _oDt_D.Columns.Add("RecordID", GetType(String)).Caption = "1-2-2"
                _oDt_D.Columns.Add("RSequence", GetType(String)).Caption = "3-8-6"
                _oDt_D.Columns.Add("ICode", GetType(String)).Caption = "9-13-5"
                _oDt_D.Columns.Add("ACode", GetType(String)).Caption = "14-18-5"
                _oDt_D.Columns.Add("BPD", GetType(String)).Caption = "19-28-15"
                _oDt_D.Columns.Add("TCode", GetType(String)).Caption = "34-37-4"
                _oDt_D.Columns.Add("DLine", GetType(String)).Caption = "38-43-6"
                _oDt_D.Columns.Add("Description", GetType(String)).Caption = "44-103-60"
                _oDt_D.Columns.Add("NCode", GetType(String)).Caption = "104-107-4"
                _oDt_D.Columns.Add("GLAc", GetType(String)).Caption = "108-122-15"
                _oDt_D.Columns.Add("DCS", GetType(String)).Caption = "123-123-1"
                _oDt_D.Columns.Add("Amount", GetType(String)).Caption = "124-138-15"
                _oDt_D.Columns.Add("Currency", GetType(String)).Caption = "139-141-3"
                _oDt_D.Columns.Add("FileDate", GetType(String)).Caption = "142-149-8"
                _oDt_D.Columns.Add("Text", GetType(String)).Caption = "Text"
                _oDt_D.Columns.Add("Sequence", GetType(String)).Caption = "Sequence"
                _oDt_D.Columns.Add("URef", GetType(String)).Caption = "150-164-15"
                'value date

                _oDt_F.TableName = "FR"
                _oDt_F.Columns.Add("RecordID", GetType(String)).Caption = "1-2-2"
                _oDt_F.Columns.Add("FileName", GetType(String)).Caption = "3-22-20"
                _oDt_F.Columns.Add("ICode", GetType(String)).Caption = "23-27-5"
                _oDt_F.Columns.Add("FRefNo", GetType(String)).Caption = "28-39-12"
                _oDt_F.Columns.Add("FileDate", GetType(String)).Caption = "40-47-8"
                _oDt_F.Columns.Add("NoDR", GetType(String)).Caption = "48-53-6"
                _oDt_F.Columns.Add("TDebitVd", GetType(String)).Caption = "54-68-15"
                _oDt_F.Columns.Add("TCreditV", GetType(String)).Caption = "69-83-15"
                Dim strQuery, strHCode As String

                For intRow As Integer = 1 To 1
                    strHCode = ""
                    If 1 = 1 Then
                        'traceService(strLogFileName, GPath, "********************************************")
                        'traceService(strLogFileName, GPath, "Started Process :  @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                        'traceService(strLogFileName, GPath, "********************************************")
                        FName = aFileName
                        Dim strDPath As String = OPath & "\" + FName
                        traceService(strLogFileName, GPath, "Processing Path :" + strDPath + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                        Try
                            Dim info As New FileInfo(strDPath)
                            Dim strFile As String = info.Name
                            If strDPath.Length > 0 Then
                                Dim txtRows() As String
                                Dim oDr As DataRow
                                traceService(strLogFileName, GPath, "Process File :" + FName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                txtRows = System.IO.File.ReadAllLines(strDPath)
                                Dim intRow1 As Integer = 0
                                traceService(strLogFileName, GPath, "Reading Data :" + FName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                Try
                                    For Each txtrow As String In txtRows
                                        If intRow1 = 0 Then
                                            oDr = _oDt_H.NewRow()
                                            For index As Integer = 0 To _oDt_H.Columns.Count - 1
                                                Dim strParam() As String = _oDt_H.Columns.Item(index).Caption.Split("-")
                                                oDr(index) = txtrow.Substring(CInt(strParam(0)) - 1, CInt(strParam(2))).Replace("'", "''")
                                            Next
                                            _oDt_H.Rows.Add(oDr)
                                            _retProcess = True
                                        ElseIf intRow1 > 0 And intRow1 < txtRows.Length - 1 And _retProcess = True Then
                                            oDr = _oDt_D.NewRow()
                                            For index As Integer = 0 To _oDt_D.Columns.Count - 1
                                                If _oDt_D.Columns.Item(index).Caption = "Sequence" Then
                                                    oDr(index) = intRow1.ToString()
                                                ElseIf _oDt_D.Columns.Item(index).Caption = "Text" Then
                                                    oDr(index) = txtrow.ToString()
                                                Else
                                                    Dim strParam() As String = _oDt_D.Columns.Item(index).Caption.Split("-")
                                                    Try
                                                        oDr(index) = txtrow.Substring(CInt(strParam(0)) - 1, CInt(strParam(2))).Replace("'", "''")
                                                    Catch ex As Exception
                                                    End Try

                                                End If
                                            Next
                                            _oDt_D.Rows.Add(oDr)
                                            _retProcess = True
                                        ElseIf intRow1 = txtRows.Length - 1 And _retProcess = True Then
                                            oDr = _oDt_F.NewRow()
                                            For index As Integer = 0 To _oDt_F.Columns.Count - 1
                                                Dim strParam() As String = _oDt_F.Columns.Item(index).Caption.Split("-")
                                                oDr(index) = txtrow.Substring(CInt(strParam(0)) - 1, CInt(strParam(2))).Replace("'", "''")
                                            Next
                                            _oDt_F.Rows.Add(oDr)
                                        End If
                                        intRow1 = intRow1 + 1
                                    Next
                                    If _retProcess = True Then
                                        traceService(strLogFileName, GPath, "Readed Successfully:" + FName + " @ " + System.DateTime.Now.ToString("hh:mms:s"))
                                    End If
                                Catch ex As Exception
                                    _retProcess = False
                                    traceService(strLogFileName, GPath, "Completed With Error : " + FName + " @ " + System.DateTime.Now.ToString("hh:mms:s"))
                                    traceService(strLogFileName, GPath, "Error :" + ex.Message)
                                End Try
                            End If

                            'Header Table
                            Dim oHeaderTable As SAPbobsCOM.UserTable
                            strQuery = "Select count(*) As ""Code"" From ""@T_OIHT"""
                            oRecordSet.DoQuery(strQuery)
                            strQuery = oApplication.Utilities.getMaxCode("@T_OIHT", "Code")
                            strHCode = strQuery
                            '  strHCode = (CInt(oRecordSet.Fields.Item("Code").Value) + 1).ToString()
                            traceService(strLogFileName, GPath, "Importing Data :" + FName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            Try
                                For index As Integer = 0 To _oDt_H.Rows.Count - 1
                                    oHeaderTable = oApplication.Company.UserTables.Item("T_OIHT")
                                    'Set default, mandatory fields
                                    If oRecordSet.RecordCount > 0 Then
                                        oHeaderTable.Code = strHCode ' (CInt(oRecordSet.Fields.Item("Code").Value) + 1).ToString()
                                        oHeaderTable.Name = strHCode '(CInt(oRecordSet.Fields.Item("Code").Value) + 1).ToString()
                                    Else
                                        oHeaderTable.Code = "1"
                                        oHeaderTable.Name = "1"
                                    End If
                                    'Set user field
                                    oHeaderTable.UserFields.Fields.Item("U_EFName").Value = FName.Trim()
                                    oHeaderTable.UserFields.Fields.Item("U_RecordID").Value = _oDt_H.Rows(index).Item(0).ToString.Trim
                                    oHeaderTable.UserFields.Fields.Item("U_FileName").Value = _oDt_H.Rows(index).Item(1).ToString.Trim
                                    oHeaderTable.UserFields.Fields.Item("U_ICode").Value = _oDt_H.Rows(index).Item(2).ToString.Trim
                                    oHeaderTable.UserFields.Fields.Item("U_FRefNo").Value = _oDt_H.Rows(index).Item(3).ToString.Trim
                                    oHeaderTable.UserFields.Fields.Item("U_FileDate").Value = _oDt_H.Rows(index).Item(4).ToString.Trim
                                    oHeaderTable.UserFields.Fields.Item("U_FileName1").Value = aFileName.ToString
                                    oHeaderTable.Add()
                                Next

                                'Detail Table
                                Dim oDetailTable As SAPbobsCOM.UserTable
                                Dim _intStatus As Integer
                                Dim oRec_A As SAPbobsCOM.Recordset
                                Dim oRec_C As SAPbobsCOM.Recordset
                                Dim oRec_J As SAPbobsCOM.Recordset
                                Dim oExequery As SAPbobsCOM.Recordset
                                Try
                                    oRec_A = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                    oRec_C = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                    oRec_J = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                    Dim strFields, strValues, strUDTCode As String
                                    Dim intNoofRecords As Double = _oDt_D.Rows.Count
                                    For index As Integer = 0 To _oDt_D.Rows.Count - 1
                                        Dim aMainForm As SAPbouiCOM.Form = oApplication.SBO_Application.Forms.GetForm("169", 1)
                                        If aMainForm Is Nothing Then
                                            Return False
                                        Else
                                            If intEmpCount > 50 Then
                                                aMainForm.Update()
                                                oApplication.Utilities.Message("Processing....", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                                intEmpCount = 0
                                            End If
                                            intEmpCount = intEmpCount + 1
                                        End If
                                        strFields = ""
                                        strValues = ""
                                        Ostatic = aform.Items.Item("250").Specific
                                        Ostatic.Caption = "Processing : " & index & " of " & intNoofRecords
                                        oDetailTable = oApplication.Company.UserTables.Item("T_OIDT")
                                        strQuery = "Select count(*) As ""Code"" From ""@T_OIDT"""
                                        oRecordSet.DoQuery(strQuery)
                                        strQuery = oApplication.Utilities.getMaxCode("@T_OIDT", "Code")
                                        strUDTCode = strQuery
                                        'Set default, mandatory fields
                                        If oRecordSet.RecordCount > 0 Then
                                        Else
                                            strUDTCode = 1
                                        End If
                                        strFields = """Code"",""Name"",""U_FileName"""
                                        strValues = "'" & strUDTCode & "','" & strUDTCode & "','" & aFileName.Replace("'", "''") & "'"
                                        strFields = strFields & ",""U_HRef"""
                                        strValues = strValues & ",'" & strHCode & "'"
                                        strFields = strFields & ",""U_RecordID"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(0).ToString.Replace("'", "''").Trim & "'"
                                        strFields = strFields & ",""U_RSequence"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(1).ToString.Replace("'", "''").Trim & "'"
                                        strFields = strFields & ",""U_ICode"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(2).ToString.Replace("'", "''").Trim & "'"
                                        strFields = strFields & ",""U_ACode"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(3).ToString.Replace("'", "''").Trim & "'"
                                        strFields = strFields & ",""U_BPD"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(4).ToString.Replace("'", "''").Trim & "'"
                                        strFields = strFields & ",""U_TCode"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(5).ToString.Replace("'", "''").Trim & "'"
                                        strFields = strFields & ",""U_DLine"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(6).ToString.Replace("'", "''").Trim & "'"
                                        strFields = strFields & ",""U_Description"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(7).ToString.Replace("'", "''").Trim & "'"
                                        strFields = strFields & ",""U_NCode"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(8).ToString.Replace("'", "''").Trim & "'"
                                       
                                        'Account/Customer Check.
                                        Dim strGLAccount As String = _oDt_D.Rows(index).Item(9).ToString.Trim
                                        Dim strTransCurrency As String = _oDt_D.Rows(index).Item(12).ToString.Trim
                                        Dim strTransactionCode As String = _oDt_D.Rows(index).Item(5).ToString.Trim
                                        Dim strBusinessPartner As String = _oDt_D.Rows(index).Item(4).ToString.Trim
                                        Dim strAccountCurrency As String = "X"
                                        Dim blnBPExists As Boolean = False
                                        Dim blnAccountExists As Boolean = False
                                        Dim strPostType As String = "A"
                                        Dim sQuery_A As String = "Select ""AcctCode"",""ActCurr"" From ""OACT"" Where ""FormatCode"" = '" + _oDt_D.Rows(index).Item(9).ToString.Trim + "' "
                                        oRec_A.DoQuery(sQuery_A)
                                        If Not oRec_A.EoF Then
                                            strPostType = "A"
                                            strAccountCurrency = oRec_A.Fields.Item(1).Value
                                            blnAccountExists = True
                                        Else
                                            Dim sQuery_C As String = "Select ""CardCode"" From ""OCRD"" Where ""CardCode"" = '" + _oDt_D.Rows(index).Item(4).ToString.Trim + "' "
                                            oRec_C.DoQuery(sQuery_C)
                                            If Not oRec_C.EoF Then
                                                strPostType = "C"
                                                blnBPExists = True
                                                '  strPostType = "A"
                                            Else
                                                strPostType = "N"
                                            End If
                                        End If
                                        Dim bpCurrency As String = strAccountCurrency
                                        Dim sQuery_C1 As String = "Select ""CardCode"",""Currency"" From ""OCRD"" Where ""CardCode"" = '" + _oDt_D.Rows(index).Item(4).ToString.Trim + "' "
                                        oRec_C.DoQuery(sQuery_C1)
                                        If oRec_C.RecordCount > 0 Then
                                            blnBPExists = True
                                            bpCurrency = oRec_C.Fields.Item(1).Value
                                        Else
                                            blnBPExists = False
                                        End If

                                        If blnAccountExists And blnBPExists = True Then
                                            If strGLAccount.StartsWith("1") Or strGLAccount.StartsWith("2") Then
                                                strPostType = "C"
                                            Else
                                                strPostType = "A"
                                            End If
                                            ' strPostType = "A"
                                        End If



                                        '2016-12-22
                                        If strGLAccount.StartsWith("1") Or strGLAccount.StartsWith("2") Then
                                            If blnAccountExists = True And strBusinessPartner <> "" Then 'IF Account is exists and BPCode is exists in file and Not exists in BP Master
                                                If blnBPExists = False Then
                                                    strPostType = "N"
                                                End If
                                            End If
                                        End If
                                        '2016-12-22
                                        strFields = strFields & ",""U_AType"""
                                        strValues = strValues & ",'" & strPostType & "'"
                                        oRec_C.DoQuery("select * from OCRN where ""U_CurCode"" ='" & strTransCurrency & "'")
                                        If oRec_C.RecordCount > 0 Then
                                            If strAccountCurrency <> "##" And strAccountCurrency.ToUpper = oRec_C.Fields.Item("CurrCode").Value.ToString.ToUpper Then
                                                strFields = strFields & ",""U_CMatch"""
                                                strValues = strValues & ",'N'"
                                            Else
                                                strFields = strFields & ",""U_CMatch"""
                                                strValues = strValues & ",'Y'"
                                            End If
                                        Else
                                            strFields = strFields & ",""U_CMatch"""
                                            strValues = strValues & ",'N'"
                                        End If
                                        'Account/Customer Check.
                                        Dim sQuery_J As String = "Select T0.""Ref1"" From ""OJDT"" T0 JOIN ""JDT1"" T1 ON T0.""TransId"" = T1.""TransId"" "
                                        sQuery_J += " And T0.""Ref1"" = '" + _oDt_H.Rows(0).Item(3).ToString + "' "
                                        sQuery_J += " And T1.""U_RecordSeq"" = '" + _oDt_D.Rows(index).Item(1).ToString.Trim + "' "
                                        oRec_J.DoQuery(sQuery_J)
                                        If Not oRec_J.EoF Then
                                            strFields = strFields & ",""U_DStatus"""
                                            strValues = strValues & ",'Y'"
                                        Else
                                            strFields = strFields & ",""U_DStatus"""
                                            strValues = strValues & ",'N'"
                                        End If
                                        strFields = strFields & ",""U_GLAc"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(9).ToString.Replace("'", "''").Trim & "'"
                                        strFields = strFields & ",""U_DCS"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(10).ToString.Replace("'", "''").Trim & "'"
                                        strFields = strFields & ",""U_Amount"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(11).ToString.Trim & "'"
                                        strFields = strFields & ",""U_Currency"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(12).ToString.Trim & "'"
                                        strFields = strFields & ",""U_FileDate1"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(13).ToString.Replace("'", "''").Trim & "'"
                                        strFields = strFields & ",""U_Text"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(14).ToString.Replace("'", "''") & "'"
                                        strFields = strFields & ",""U_URef"""
                                        strValues = strValues & ",'" & _oDt_D.Rows(index).Item(16).ToString.Replace("'", "''").Trim & "'"

                                        oExequery = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                        strFields = "Insert into ""@T_OIDT"" (" & strFields & ") values (" & strValues & ")"
                                        oExequery.DoQuery(strFields)
                                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oExequery)
                                    Next
                                Catch ex As Exception
                                    _retProcess = False
                                    System.Windows.Forms.MessageBox.Show(ex.Message)
                                    traceService(strLogFileName, GPath, "Completed With Error : " & ex.Message & " File : " + FName + " @ " + System.DateTime.Now.ToString("hh:mms:s"))
                                    traceService(strLogFileName, GPath, "Error :" + ex.Message)
                                Finally
                                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec_A)
                                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec_C)
                                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec_J)
                                    '  System.Runtime.InteropServices.Marshal.ReleaseComObject(oExequery)
                                    GC.Collect()
                                End Try

                                Ostatic = aform.Items.Item("250").Specific
                                Ostatic.Caption = ""

                                'Footer Table
                                Dim oFooterTable As SAPbobsCOM.UserTable
                                For index As Integer = 0 To _oDt_F.Rows.Count - 1
                                    oFooterTable = oApplication.Company.UserTables.Item("T_OIFT")
                                    strQuery = "Select count(*) As ""Code"" From ""@T_OIFT"""
                                    oRecordSet.DoQuery(strQuery)
                                    'Set default, mandatory fields
                                    strQuery = oApplication.Utilities.getMaxCode("@T_OIFT", "Code")
                                    If oRecordSet.RecordCount > 0 Then
                                        oFooterTable.Code = strQuery ' (CInt(oRecordSet.Fields.Item("Code").Value) + 1).ToString()
                                        oFooterTable.Name = strQuery ' (CInt(oRecordSet.Fields.Item("Code").Value) + 1).ToString()
                                    Else
                                        oFooterTable.Code = "1"
                                        oFooterTable.Name = "1"
                                    End If
                                    'Set user field

                                    oFooterTable.UserFields.Fields.Item("U_HRef").Value = strHCode.Trim()
                                    oFooterTable.UserFields.Fields.Item("U_RecordID").Value = _oDt_F.Rows(index).Item(0).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_FileName").Value = _oDt_F.Rows(index).Item(1).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_ICode").Value = _oDt_F.Rows(index).Item(2).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_FRefNo").Value = _oDt_F.Rows(index).Item(3).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_FileDate").Value = _oDt_F.Rows(index).Item(4).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_NoDR").Value = _oDt_F.Rows(index).Item(5).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_TDebitV").Value = _oDt_F.Rows(index).Item(6).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_TCreditV").Value = _oDt_F.Rows(index).Item(7).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_FileName1").Value = aFileName.Replace("'", "''")
                                    oFooterTable.Add()
                                Next
                                If _retProcess = True Then
                                    traceService(strLogFileName, GPath, "Imported Data Successfully:" + FName + " @ " + System.DateTime.Now.ToString("hh:mms:s"))
                                    _oDt_H.Rows.Clear()
                                    _oDt_D.Rows.Clear()
                                    _oDt_F.Rows.Clear()
                                End If
                            Catch ex As Exception
                                _retProcess = False
                                System.Windows.Forms.MessageBox.Show(ex.Message)
                                traceService(strLogFileName, GPath, "Completed With Error : " + FName + " @ " + System.DateTime.Now.ToString("hh:mms:s"))
                                traceService(strLogFileName, GPath, "Error :" + ex.Message)
                                intID_F += 1
                            End Try
                        Catch ex As Exception
                            _retProcess = False
                            System.Windows.Forms.MessageBox.Show(ex.Message)
                        End Try

                        If _retProcess = True Then
                            Ostatic = aform.Items.Item("250").Specific
                            Ostatic.Caption = "File Validation processing....."
                            If FileValidation(aCompany, strType, FName, strHCode) Then
                                Ostatic = aform.Items.Item("250").Specific
                                Ostatic.Caption = "Journal Creation Processing...."
                                JECreation(aCompany, strType, FName, strHCode)
                                Ostatic = aform.Items.Item("250").Specific
                                Ostatic.Caption = " "
                            End If
                            Ostatic = aform.Items.Item("250").Specific
                            Ostatic.Caption = " "
                        Else
                            _retVal = False
                        End If
                    End If
                Next
            Else

                'For Invoices
                OPath = GetOpenPath(strType, aCompany)
                GPath = GetGeneralLogPath(strType, aCompany)

                _oDt_H.TableName = "HD"
                _oDt_H.Columns.Add("RecordID", GetType(String)).Caption = "1-2-2"
                _oDt_H.Columns.Add("FileName", GetType(String)).Caption = "3-22-20"
                _oDt_H.Columns.Add("ICode", GetType(String)).Caption = "23-27-5"
                _oDt_H.Columns.Add("FRefNo", GetType(String)).Caption = "28-39-12"
                _oDt_H.Columns.Add("FileDate", GetType(String)).Caption = "40-46-8"

                _oDt_D.TableName = "DE"
                _oDt_D.Columns.Add("RecordID", GetType(String)).Caption = "1-2-2"
                _oDt_D.Columns.Add("RSequence", GetType(String)).Caption = "3-8-6"
                _oDt_D.Columns.Add("ICode", GetType(String)).Caption = "9-13-5"
                _oDt_D.Columns.Add("ACode", GetType(String)).Caption = "14-18-5"
                _oDt_D.Columns.Add("BPD", GetType(String)).Caption = "19-33-15"
                _oDt_D.Columns.Add("TCode", GetType(String)).Caption = "34-43-10"
                '_oDt_D.Columns.Add("TCode", GetType(String)).Caption = "29-34-6" ' Space is there need to CHeck
                _oDt_D.Columns.Add("Description", GetType(String)).Caption = "44-103-60"
                _oDt_D.Columns.Add("NCode", GetType(String)).Caption = "104-107-4"
                _oDt_D.Columns.Add("Amount", GetType(String)).Caption = "108-122-15"
                _oDt_D.Columns.Add("Currency", GetType(String)).Caption = "123-125-3"
                _oDt_D.Columns.Add("Text", GetType(String)).Caption = "Text"
                _oDt_D.Columns.Add("Sequence", GetType(String)).Caption = "Sequence"

                _oDt_F.TableName = "FR"
                _oDt_F.Columns.Add("RecordID", GetType(String)).Caption = "1-2-2"
                _oDt_F.Columns.Add("FileName", GetType(String)).Caption = "3-22-20"
                _oDt_F.Columns.Add("ICode", GetType(String)).Caption = "23-27-5"
                _oDt_F.Columns.Add("FRefNo", GetType(String)).Caption = "28-39-12"
                _oDt_F.Columns.Add("FileDate", GetType(String)).Caption = "40-47-8"
                _oDt_F.Columns.Add("NoDR", GetType(String)).Caption = "48-53-6"
                _oDt_F.Columns.Add("TAmount", GetType(String)).Caption = "54-68-15"


                Dim strQuery, strHCode As String
                For intRow As Integer = 1 To 1
                    strHCode = ""
                    If 1 = 1 Then
                        'traceService(strLogFileName, GPath, "********************************************")
                        'traceService(strLogFileName, GPath, "Started Process :  @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                        'traceService(strLogFileName, GPath, "********************************************")
                        FName = aFileName
                        Dim strDPath As String = OPath & "\" + FName
                        'oLoadForm = Nothing
                        'oLoadForm = oApplication.Utilities.LoadMessageForm(xml_Load, frm_Load)
                        'oLoadForm = oApplication.SBO_Application.Forms.ActiveForm()
                        'oLoadForm.Items.Item("3").TextStyle = 4
                        'oLoadForm.Items.Item("4").TextStyle = 5
                        'CType(oLoadForm.Items.Item("3").Specific, SAPbouiCOM.StaticText).Caption = "PLEASE WAIT..."
                        'CType(oLoadForm.Items.Item("4").Specific, SAPbouiCOM.StaticText).Caption = "Executing ..." + FName
                        traceService(strLogFileName, GPath, "Processing Path :" + strDPath + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                        Try
                            Dim info As New FileInfo(strDPath)
                            Dim strFile As String = info.Name
                            If strDPath.Length > 0 Then
                                Dim txtRows() As String
                                Dim oDr As DataRow
                                traceService(strLogFileName, GPath, "Process File :" + FName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                txtRows = System.IO.File.ReadAllLines(strDPath)
                                Dim intRow1 As Integer = 0
                                traceService(strLogFileName, GPath, "Reading Data :" + FName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                Try
                                    For Each txtrow As String In txtRows
                                        If intRow1 = 0 Then
                                            oDr = _oDt_H.NewRow()
                                            For index As Integer = 0 To _oDt_H.Columns.Count - 1
                                                Dim strParam() As String = _oDt_H.Columns.Item(index).Caption.Split("-")
                                                oDr(index) = txtrow.Substring(CInt(strParam(0)) - 1, CInt(strParam(2))).Replace("'", "''")
                                            Next
                                            _oDt_H.Rows.Add(oDr)
                                            _retProcess = True
                                        ElseIf intRow1 > 0 And intRow1 < txtRows.Length - 1 And _retProcess = True Then
                                            oDr = _oDt_D.NewRow()
                                            For index As Integer = 0 To _oDt_D.Columns.Count - 1
                                                If _oDt_D.Columns.Item(index).Caption = "Sequence" Then
                                                    oDr(index) = intRow1.ToString()
                                                ElseIf _oDt_D.Columns.Item(index).Caption = "Text" Then
                                                    oDr(index) = txtrow.ToString()
                                                ElseIf _oDt_D.Columns.Item(index).ColumnName = "Currency" Then
                                                    Try
                                                        Dim strParam() As String = _oDt_D.Columns.Item(index).Caption.Split("-")
                                                        oDr(index) = txtrow.Substring(CInt(strParam(0)) - 1, CInt(strParam(2))).Replace("'", "''")
                                                    Catch ex As Exception

                                                    End Try
                                                Else
                                                    Dim strParam() As String = _oDt_D.Columns.Item(index).Caption.Split("-")
                                                    oDr(index) = txtrow.Substring(CInt(strParam(0)) - 1, CInt(strParam(2))).Replace("'", "''")
                                                End If
                                            Next
                                            _oDt_D.Rows.Add(oDr)
                                            _retProcess = True
                                        ElseIf intRow1 = txtRows.Length - 1 And _retProcess = True Then
                                            oDr = _oDt_F.NewRow()
                                            For index As Integer = 0 To _oDt_F.Columns.Count - 1
                                                Dim strParam() As String = _oDt_F.Columns.Item(index).Caption.Split("-")
                                                oDr(index) = txtrow.Substring(CInt(strParam(0)) - 1, CInt(strParam(2))).Replace("'", "''")
                                            Next
                                            _oDt_F.Rows.Add(oDr)
                                        End If
                                        intRow1 = intRow1 + 1
                                    Next

                                    If _retProcess = True Then
                                        traceService(strLogFileName, GPath, "Readed Successfully:" + FName + " @ " + System.DateTime.Now.ToString("hh:mms:s"))
                                    End If
                                Catch ex As Exception
                                    _retProcess = False
                                    'System.Windows.Forms.MessageBox.Show(ex.Message)
                                    traceService(strLogFileName, GPath, "Completed With Error : " + FName + " @ " + System.DateTime.Now.ToString("hh:mms:s"))
                                    traceService(strLogFileName, GPath, "Error :" + ex.Message)
                                End Try

                            End If

                            'Header Table
                            Dim oHeaderTable As SAPbobsCOM.UserTable
                            strQuery = "Select count(*) As ""Code"" From ""@T_OPHT"""
                            oRecordSet.DoQuery(strQuery)
                            strHCode = (CInt(oRecordSet.Fields.Item("Code").Value) + 1).ToString()
                            strHCode = oApplication.Utilities.getMaxCode("@T_OPHT", "Code")
                            traceService(strLogFileName, GPath, "Importing Data :" + FName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            Try
                                For index As Integer = 0 To _oDt_H.Rows.Count - 1
                                    oHeaderTable = oApplication.Company.UserTables.Item("T_OPHT")
                                    'Set default, mandatory fields
                                    If oRecordSet.RecordCount > 0 Then
                                        oHeaderTable.Code = strHCode ' (CInt(oRecordSet.Fields.Item("Code").Value) + 1).ToString()
                                        oHeaderTable.Name = strHCode '(CInt(oRecordSet.Fields.Item("Code").Value) + 1).ToString()
                                    Else
                                        oHeaderTable.Code = "1"
                                        oHeaderTable.Name = "1"
                                    End If

                                    'Set user field
                                    oHeaderTable.UserFields.Fields.Item("U_EFName").Value = FName.Trim()
                                    oHeaderTable.UserFields.Fields.Item("U_RecordID").Value = _oDt_H.Rows(index).Item(0).ToString.Trim
                                    oHeaderTable.UserFields.Fields.Item("U_FileName").Value = _oDt_H.Rows(index).Item(1).ToString.Trim
                                    oHeaderTable.UserFields.Fields.Item("U_ICode").Value = _oDt_H.Rows(index).Item(2).ToString.Trim
                                    oHeaderTable.UserFields.Fields.Item("U_FRefNo").Value = _oDt_H.Rows(index).Item(3).ToString.Trim
                                    oHeaderTable.UserFields.Fields.Item("U_FileDate").Value = _oDt_H.Rows(index).Item(4).ToString.Trim
                                    oHeaderTable.Add()
                                Next

                                'Detail Table
                                Dim oDetailTable As SAPbobsCOM.UserTable
                                Dim _intStatus As Integer
                                Dim oRec_A As SAPbobsCOM.Recordset
                                Dim oRec_C As SAPbobsCOM.Recordset
                                Dim oRec_J As SAPbobsCOM.Recordset
                                oRec_A = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRec_C = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRec_J = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

                                For index As Integer = 0 To _oDt_D.Rows.Count - 1
                                    oDetailTable = oApplication.Company.UserTables.Item("T_OPDT")

                                    strQuery = "Select count(*) As ""Code"" From ""@T_OPDT"""
                                    oRecordSet.DoQuery(strQuery)
                                    strQuery = oApplication.Utilities.getMaxCode("@T_OPDT", "Code")
                                    'Set default, mandatory fields
                                    If oRecordSet.RecordCount > 0 Then
                                        oDetailTable.Code = strQuery ' (CInt(oRecordSet.Fields.Item("Code").Value) + 1).ToString()
                                        oDetailTable.Name = strQuery ' (CInt(oRecordSet.Fields.Item("Code").Value) + 1).ToString()
                                    Else
                                        oDetailTable.Code = "1"
                                        oDetailTable.Name = "1"
                                    End If

                                    'Set user field
                                    oDetailTable.UserFields.Fields.Item("U_HRef").Value = strHCode.Trim()
                                    oDetailTable.UserFields.Fields.Item("U_RecordID").Value = _oDt_D.Rows(index).Item(0).ToString.Trim
                                    oDetailTable.UserFields.Fields.Item("U_RSequence").Value = _oDt_D.Rows(index).Item(1).ToString.Trim
                                    oDetailTable.UserFields.Fields.Item("U_ICode").Value = _oDt_D.Rows(index).Item(2).ToString.Trim
                                    oDetailTable.UserFields.Fields.Item("U_ACode").Value = _oDt_D.Rows(index).Item(3).ToString.Trim
                                    oDetailTable.UserFields.Fields.Item("U_BPD").Value = _oDt_D.Rows(index).Item(4).ToString.Trim

                                    oDetailTable.UserFields.Fields.Item("U_TCode").Value = _oDt_D.Rows(index).Item(5).ToString.Trim
                                    oDetailTable.UserFields.Fields.Item("U_Description").Value = _oDt_D.Rows(index).Item(6).ToString.Trim
                                    oDetailTable.UserFields.Fields.Item("U_NCode").Value = _oDt_D.Rows(index).Item(7).ToString.Trim
                                    oDetailTable.UserFields.Fields.Item("U_Amount").Value = _oDt_D.Rows(index).Item(8).ToString.Trim
                                    oDetailTable.UserFields.Fields.Item("U_Currency").Value = _oDt_D.Rows(index).Item(9).ToString.Trim
                                    oDetailTable.UserFields.Fields.Item("U_Text").Value = _oDt_D.Rows(index).Item(10).ToString.Trim



                                    'Customer Check.
                                    Dim sQuery_C As String = "Select ""CardCode"" From ""OCRD"" Where ""CardCode"" = '" + _oDt_D.Rows(index).Item(4).ToString.Trim + "' "
                                    oRec_C.DoQuery(sQuery_C)
                                    If Not oRec_C.EoF Then
                                        oDetailTable.UserFields.Fields.Item("U_AType").Value = "C"
                                    Else
                                        oDetailTable.UserFields.Fields.Item("U_AType").Value = "N"
                                    End If

                                    'Duplicate Check.
                                    Dim sQuery_J As String = "Select T0.""DocEntry"" From ""ODLN"" T0 JOIN ""DLN1"" T1 ON T0.""DocEntry"" = T1.""DocEntry"" "
                                    sQuery_J += " And T0.""U_FileRef"" = '" + _oDt_H.Rows(0).Item(3).ToString.Trim + "' " ' File Ref
                                    sQuery_J += " And T1.""U_RecordSeq"" = '" + _oDt_D.Rows(index).Item(1).ToString.Trim + "' "
                                    oRec_J.DoQuery(sQuery_J)
                                    If Not oRec_J.EoF Then
                                        oDetailTable.UserFields.Fields.Item("U_DStatus").Value = "Y"
                                    Else
                                        oDetailTable.UserFields.Fields.Item("U_DStatus").Value = "N"
                                    End If

                                    _intStatus = oDetailTable.Add()

                                Next

                                'Footer Table
                                Dim oFooterTable As SAPbobsCOM.UserTable
                                For index As Integer = 0 To _oDt_F.Rows.Count - 1

                                    oFooterTable = oApplication.Company.UserTables.Item("T_OPFT")
                                    strQuery = "Select count(*) As ""Code"" From ""@T_OPFT"""
                                    oRecordSet.DoQuery(strQuery)
                                    strQuery = oApplication.Utilities.getMaxCode("@T_OPFT", "Code")
                                    'Set default, mandatory fields
                                    If oRecordSet.RecordCount > 0 Then
                                        oFooterTable.Code = strQuery ' (CInt(oRecordSet.Fields.Item("Code").Value) + 1).ToString()
                                        oFooterTable.Name = strQuery ' (CInt(oRecordSet.Fields.Item("Code").Value) + 1).ToString()
                                    Else
                                        oFooterTable.Code = "1"
                                        oFooterTable.Name = "1"
                                    End If

                                    'Set user field
                                    oFooterTable.UserFields.Fields.Item("U_HRef").Value = strHCode.Trim()
                                    oFooterTable.UserFields.Fields.Item("U_RecordID").Value = _oDt_F.Rows(index).Item(0).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_FileName").Value = _oDt_F.Rows(index).Item(1).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_ICode").Value = _oDt_F.Rows(index).Item(2).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_FRefNo").Value = _oDt_F.Rows(index).Item(3).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_FileDate").Value = _oDt_F.Rows(index).Item(4).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_NoDR").Value = _oDt_F.Rows(index).Item(5).ToString.Trim
                                    oFooterTable.UserFields.Fields.Item("U_TValue").Value = _oDt_F.Rows(index).Item(6).ToString.Trim

                                    oFooterTable.Add()
                                Next
                                If _retProcess = True Then
                                    traceService(strLogFileName, GPath, "Imported Data Successfully:" + FName + " @ " + System.DateTime.Now.ToString("hh:mms:s"))
                                    _oDt_H.Rows.Clear()
                                    _oDt_D.Rows.Clear()
                                    _oDt_F.Rows.Clear()
                                End If
                            Catch ex As Exception
                                _retProcess = False
                                System.Windows.Forms.MessageBox.Show(ex.Message)
                                traceService(strLogFileName, GPath, "Completed With Error : " + FName + " @ " + System.DateTime.Now.ToString("hh:mms:s"))
                                traceService(strLogFileName, GPath, "Error :" + ex.Message)
                                intID_F += 1
                            End Try
                        Catch ex As Exception
                            _retProcess = False
                            System.Windows.Forms.MessageBox.Show(ex.Message)
                        End Try

                        If _retProcess = True Then

                            If FileValidation(aCompany, strType, FName, strHCode) Then
                                ARCreation(aCompany, strType, FName, strHCode)
                            End If
                        Else
                            _retVal = False
                        End If
                    End If
                Next
            End If
            Return _retVal
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function

    Private Function FileValidation(ByVal acompany As SAPbobsCOM.Company, ByVal strType As String, ByVal strFName As String, ByVal strHCode As String) As Boolean
        Try
            Try
                Dim ELOGPath As String
                Dim GPath As String
                Dim SLOGPath As String
                Dim strLogFileName As String = "MI Integration_" & strType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                Dim strLogFileName_Failure As String = "MI Integration_Failure _" & strType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                Dim _retVal As Boolean = True
                Dim intEmpCount As Integer = 0
                '_" & strTypefromUDT & "_"
                GPath = GetGeneralLogPath(strType, acompany)
                ELOGPath = GetErrorLogPath(strType, acompany)
                SLOGPath = GetSuccessLogPath(strType, acompany)
                ' oDTFailure = oForm.DataSources.DataTables.Item("dtFailure")
                Dim strquery, strqry As String
                Dim blnIsError As Boolean = False
                If strType = "JE" Then
                    oRecordSet = acompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    '1. Checking Account/Customer Code
                    '===============================
                    strquery = "Select Concat('InValid Account/BP Code Code: ' , T0.""U_GLAc"")  As ""Error"",T0.""U_Text"",T0.""U_GLAc"" From ""@T_OIDT"" T0"
                    strquery += " Join ""@T_OIHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                    strquery += " Where (T0.""U_AType"" = 'N')"
                    oRecordSet.DoQuery(strquery)
                    For intRow As Integer = 0 To oRecordSet.RecordCount - 1
                        Dim aMainForm1 As SAPbouiCOM.Form = oApplication.SBO_Application.Forms.GetForm("169", 1)
                        If aMainForm1 Is Nothing Then
                            Return False
                        Else
                            If intEmpCount > 50 Then
                                aMainForm1.Update()
                                oApplication.Utilities.Message("Processing....", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                intEmpCount = 0
                            End If
                            intEmpCount = intEmpCount + 1
                        End If
                        Dim strText As String = oRecordSet.Fields.Item(2).Value & ":" & oRecordSet.Fields.Item(1).Value
                        traceService(strLogFileName, GPath, " : Error: GL Account/BP Code Code Not Exist in Account Code :" & strText & ":" & strFName & " @ " & System.DateTime.Now.ToString("hh:mm:ss"))
                        traceService(strLogFileName_Failure, ELOGPath, " : Error: GL Account/BP Code Code Not Exist in Account Code :" & strText & ":" & strFName & " @ " & System.DateTime.Now.ToString("hh:mm:ss"))
                        blnIsError = True
                        oRecordSet.MoveNext()
                    Next

                    strquery = "Select Concat('InValid Account/BP Code Code: ' , T0.""U_GLAc"")  As ""Error"",T0.""U_Text"",T0.""U_GLAc"" From ""@T_OIDT"" T0"
                    strquery += " Join ""@T_OIHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                    strquery += " Where (T0.""U_AType"" = 'N')"
                    oRecordSet.DoQuery(strquery)
                    If oRecordSet.RecordCount > 0 Then
                        strquery = "Update ""@T_OIHT"" Set ""U_Status""='N' , ""U_Error"" = " + "'GL Account/BP Code Code Not Exist'" + " Where ""Code""='" + strHCode + "'"
                        oRecordSet.DoQuery(strquery)

                        strquery = " Update T1 Set T1.""U_Status"" ='N' , T1.""U_Error""=" + "'GL Account/BP Code Code Not Exist'" + ""
                        strquery += " From ""@T_OIDT"" T1 join ""@T_OIHT"" T2 On T2.""Code""=T1.""U_HRef"""
                        strquery += " Where T2.""Code""='" + strHCode + "'"
                        strquery += " And ""U_AType"" = 'N' "
                        oRecordSet.DoQuery(strquery)
                        ' _retVal = False
                        blnIsError = True
                    End If


                    'Checking Currency Matching
                    strquery = "Select Concat('InValid Account/BP Code Code: ' , T0.""U_GLAc"")  As ""Error"",T0.""U_Text"",T0.""U_GLAc"" From ""@T_OIDT"" T0"
                    strquery += " Join ""@T_OIHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                    strquery += " Where (T0.""U_CMatch"" = 'N' and ""U_AType"" <> 'N')"
                    oRecordSet.DoQuery(strquery)
                    For intRow As Integer = 0 To oRecordSet.RecordCount - 1
                        Dim aMainForm1 As SAPbouiCOM.Form = oApplication.SBO_Application.Forms.GetForm("169", 1)
                        If aMainForm1 Is Nothing Then
                            Return False
                        Else

                            If intEmpCount > 50 Then
                                aMainForm1.Update()
                                oApplication.Utilities.Message("Processing....", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                intEmpCount = 0
                            End If
                            intEmpCount = intEmpCount + 1
                        End If
                        Dim strText As String = oRecordSet.Fields.Item(2).Value & ":" & oRecordSet.Fields.Item(1).Value
                        traceService(strLogFileName, GPath, " : Error: GL Account Currency not matched with Transaction Currency :" & strText & ":" & strFName & " @ " & System.DateTime.Now.ToString("hh:mm:ss"))
                        traceService(strLogFileName_Failure, ELOGPath, " : Error: GL Account Currency not matched with Transaction Currency :" & strText & ":" & strFName & " @ " & System.DateTime.Now.ToString("hh:mm:ss"))
                        blnIsError = True
                        oRecordSet.MoveNext()
                    Next

                    strquery = "Select Concat('InValid Account/BP Code Code: ' , T0.""U_GLAc"")  As ""Error"",T0.""U_Text"",T0.""U_GLAc"" From ""@T_OIDT"" T0"
                    strquery += " Join ""@T_OIHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                    strquery += " Where (T0.""U_CMatch"" = 'N' and ""U_AType"" <> 'N')"
                    oRecordSet.DoQuery(strquery)
                    If oRecordSet.RecordCount > 0 Then
                        strquery = "Update ""@T_OIHT"" Set ""U_Status""='N' , ""U_Error"" = " + "'GL Account Currency not matched with Transaction Currency'" + " Where ""Code""='" + strHCode + "'"
                        oRecordSet.DoQuery(strquery)
                        strquery = " Update T1 Set T1.""U_Status"" ='N' , T1.""U_Error""=" + "'GL Account Currency not matched with Transaction Currency'" + ""
                        strquery += " From ""@T_OIDT"" T1 join ""@T_OIHT"" T2 On T2.""Code""=T1.""U_HRef"""
                        strquery += " Where T2.""Code""='" + strHCode + "'"
                        strquery += " And ""U_CMatch"" = 'N' "
                        oRecordSet.DoQuery(strquery)
                        ' _retVal = False
                        blnIsError = True
                    End If
                    'End Currency Matching
                    Dim aMainForm As SAPbouiCOM.Form = frmMainForm ' oApplication.SBO_Application.Forms.GetForm("169", 1)
                    If aMainForm Is Nothing Then
                        Return False
                    Else
                        If intEmpCount > 50 Then
                            aMainForm.Update()
                            oApplication.Utilities.Message("Processing....", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            intEmpCount = 0
                        End If
                        intEmpCount = intEmpCount + 1
                    End If
                    '===============================

                    '3. Currency Mapping
                    '===============================
                    If 1 = 1 Then '_retVal Then
                        strquery = "Select Concat('InValid Currency Code: ' , T0.""U_Currency"")  As ""Error"",T0.""U_Text"",T0.""U_Currency"" From ""@T_OIDT"" T0"
                        strquery += " Left Outer JOIN OCRN T1 On T1.""U_CurCode"" = T0.""U_Currency"""
                        strquery += " Join ""@T_OIHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                        strquery += " Where (T1.""U_CurCode"" Is Null)"
                        oRecordSet.DoQuery(strquery)
                        For intRow As Integer = 0 To oRecordSet.RecordCount - 1
                            aMainForm = frmMainForm 'oApplication.SBO_Application.Forms.GetForm("169", 1)
                            If aMainForm Is Nothing Then
                                Return False
                            Else

                                If intEmpCount > 50 Then
                                    aMainForm.Update()
                                    oApplication.Utilities.Message("Processing....", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                    intEmpCount = 0
                                End If
                                intEmpCount = intEmpCount + 1
                            End If
                            Dim strText As String = oRecordSet.Fields.Item(2).Value & ":" & oRecordSet.Fields.Item(1).Value
                            traceService(strLogFileName, GPath, " : Error: Currency Not Mapped  Currency Code : " & strText & ": " & strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            traceService(strLogFileName_Failure, ELOGPath, strText & " : Error: Currency Not Mapped Currency Code  " & strText & ":" & strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            blnIsError = True
                            oRecordSet.MoveNext()
                        Next


                        strquery = "Select Concat('InValid Currency Code: ' , T0.""U_Currency"")  As ""Error"" From ""@T_OIDT"" T0"
                        strquery += " Left Outer JOIN OCRN T1 On T1.""U_CurCode"" = T0.""U_Currency"""
                        strquery += " Join ""@T_OIHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                        strquery += " Where (T1.""U_CurCode"" Is Null)"

                        oRecordSet.DoQuery(strquery)

                        If oRecordSet.RecordCount > 0 Then
                            '  traceService(strLogFileName, GPath, "Error:  Currency Not Mapped :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            ' traceService(strLogFileName_Failure, ELOGPath, "Error: Currency Not Mapped :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            '  MoveToFolder(strType, strFName, "E", acompany)
                            strquery = "Update ""@T_OIHT"" Set ""U_Status""='N' , ""U_Error"" = " + "' Currency Not Mapped '" + " Where ""Code""='" + strHCode + "'"
                            oRecordSet.DoQuery(strquery)

                            strquery = "Update T1 Set T1.""U_Status"" ='N' , T1.""U_Error""=" + "' Currency Not Mapped '" + ""
                            strquery += " From ""@T_OIDT"" T1 join ""@T_OIHT"" T2 On T2.""Code""=T1.""U_HRef"""
                            strquery += " Where T2.""Code""='" + strHCode + "'"
                            oRecordSet.DoQuery(strquery)

                            '_retVal = False
                            blnIsError = True
                        End If
                    End If
                    '===============================

                    '4. Transaction Code
                    '===============================
                    If 1 = 1 Then '_retVal Then
                        strquery = " Select Concat('InValid Transaction Code: ' , T0.""U_TCode"")  As ""Error"",T0.""U_Text"",T0.""U_TCode"" From ""@T_OIDT"" T0"
                        strquery += " Left Outer JOIN OTRC T1 On T1.""TrnsCode"" = T0.""U_TCode"""
                        strquery += " Join ""@T_OIHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                        strquery += " Where (T1.""TrnsCode"" Is Null)"
                        oRecordSet.DoQuery(strquery)
                        For intRow As Integer = 0 To oRecordSet.RecordCount - 1
                            aMainForm = frmMainForm ' oApplication.SBO_Application.Forms.GetForm("169", 1)
                            If aMainForm Is Nothing Then
                                Return False
                            Else
                                ' aMainForm.Update()
                                If intEmpCount > 50 Then
                                    aMainForm.Update()
                                    oApplication.Utilities.Message("Processing....", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                    intEmpCount = 0
                                End If
                                intEmpCount = intEmpCount + 1
                            End If
                            Dim strText As String = oRecordSet.Fields.Item(2).Value & ":" & oRecordSet.Fields.Item(1).Value
                            traceService(strLogFileName, GPath, " : Error: Transaction Code Not Defined  : Transaction Code : " & strText & ": " & strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            traceService(strLogFileName_Failure, ELOGPath, strText & " : Error: Transaction Code Not Defined  TransactioN Code :  " & strText & ":" & strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            blnIsError = True
                            oRecordSet.MoveNext()
                        Next
                        strquery = " Select Concat('InValid Transaction Code: ' , T0.""U_TCode"")  As ""Error"",T0.""U_Text"",T0.""U_TCode"" From ""@T_OIDT"" T0"
                        strquery += " Left Outer JOIN OTRC T1 On T1.""TrnsCode"" = T0.""U_TCode"""
                        strquery += " Join ""@T_OIHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                        strquery += " Where (T1.""TrnsCode"" Is Null)"
                        oRecordSet.DoQuery(strquery)
                        If oRecordSet.RecordCount > 0 Then
                            traceService(strLogFileName, GPath, "Error: Transaction Code Not Defined :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            traceService(strLogFileName_Failure, ELOGPath, "Error: Transaction Code Not Defined :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            '  MoveToFolder(strType, strFName, "E", acompany)

                            strquery = "Update ""@T_OIHT"" Set ""U_Status""='N' , ""U_Error"" = " + "'Transaction Not Defined'" + " Where ""Code""='" + strHCode + "'"
                            oRecordSet.DoQuery(strquery)

                            strquery = "Update T1 Set T1.""U_Status"" ='N' , T1.""U_Error""=" + "'Transaction Code Not Defined'" + ""
                            strquery += " From ""@T_OIDT"" T1 join ""@T_OIHT"" T2 On T2.""Code""=T1.""U_HRef"""
                            strquery += " Where T2.""Code""='" + strHCode + "'"
                            oRecordSet.DoQuery(strquery)
                            '  _retVal = False
                            blnIsError = True
                        End If
                    End If
                    '===============================


                    '5. iv.	Duplicate import validation based on File Reference no (header) and Sequence No (Lines)
                    '===============================
                    'If blnIsError = True Then
                    '    _retVal = False
                    'End If
                    If _retVal Then
                        strquery = " Select Concat('Duplicate Import Found : ' , T2.""U_FRefNo"")  As ""Error"" From ""@T_OIDT"" T0"
                        strquery += " Join ""@T_OIHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                        strquery += "AND T0.""U_DStatus"" ='Y' "
                        oRecordSet.DoQuery(strquery)

                        If oRecordSet.RecordCount > 0 Then
                            traceService(strLogFileName, GPath, "Error: Duplicate Import Found : :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            traceService(strLogFileName_Failure, ELOGPath, "Error: Duplicate Import Found : :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))

                            ' MoveToFolder(strType, strFName, "E", acompany)

                            strquery = "Update ""@T_OIHT"" Set ""U_Status""='N' , ""U_Error"" = " + "'Duplicate Import Found'" + " Where ""Code""='" + strHCode + "'"
                            oRecordSet.DoQuery(strquery)

                            strquery = "Update T1 Set T1.""U_Status"" ='N' , T1.""U_Error""=" + "'Duplicate Import Found'" + ""
                            strquery += " From ""@T_OIDT"" T1 join ""@T_OIHT"" T2 On T2.""Code""=T1.""U_HRef"""
                            strquery += " Where T2.""Code""='" + strHCode + "'"
                            strquery += " And ""U_DStatus"" = 'Y' "
                            oRecordSet.DoQuery(strquery)

                            ' _retVal = False
                            blnIsError = True
                        End If
                    End If
                    '===============================


                    '===============================

                    '2. Debit And Credit Balances Validation
                    '===============================
                    If 1 = 1 Then ' _retVal Then
                        strqry = "Select Sum(T1.""U_Amount"")  As ""DAmount"" from ""@T_OIDT"" T1 Join ""@T_OIHT"" T2 "
                        strqry += " On T2.""Code"" =  T1.""U_HRef"" and T2.""Code""='" + strHCode + "'"
                        strqry += "AND T1.""U_DCS""='D'"
                        oRecordSet.DoQuery(strqry)

                        Dim DValue As Double
                        Dim CValue As Double
                        DValue = oRecordSet.Fields.Item("DAmount").Value

                        strqry = "Select Sum(T1.""U_Amount"")  As ""CAmount"" from ""@T_OIDT"" T1 Join ""@T_OIHT"" T2 "
                        strqry += " On T2.""Code"" =  T1.""U_HRef"" and T2.""Code""='" + strHCode + "'"
                        strqry += "AND T1.""U_DCS""='C'"
                        oRecordSet.DoQuery(strqry)

                        CValue = oRecordSet.Fields.Item("CAmount").Value

                        strqry = "Select * from ""@T_OIFT"" where ""U_HRef""='" & strHCode & "'"
                        oRecordSet.DoQuery(strqry)
                        Dim dblFDbit, dblFCredit As Double
                        If oRecordSet.RecordCount > 0 Then
                            dblFDbit = oRecordSet.Fields.Item("U_TDebitV").Value
                            dblFCredit = oRecordSet.Fields.Item("U_TCreditV").Value
                        Else
                            dblFDbit = 0
                            dblFCredit = 0
                        End If

                        If CValue <> dblFCredit Then
                            traceService(strLogFileName, GPath, "ERROR : Total Credit Value  " + CValue.ToString + " not matched with Footer Credit Value : " + dblFCredit.ToString + " :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            traceService(strLogFileName_Failure, ELOGPath, " ERROR : Total Credit Value  " + CValue.ToString + " not matched with Footer Credit Value : " + dblFCredit.ToString + " :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            blnIsError = True
                        End If
                        If DValue <> dblFDbit Then
                            traceService(strLogFileName, GPath, "ERROR :  Total Debit Value " + DValue.ToString + "  not matched with Footer Credit Value :" + dblFDbit.ToString + " :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            traceService(strLogFileName_Failure, ELOGPath, " ERROR :  Total Debit Value " + DValue.ToString + "   not matched with Footer Credit Value:" + dblFDbit.ToString + " :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            blnIsError = True
                        End If

                        strqry = "Select Sum(T1.""U_Amount"")  As ""DAmount"",T1.""U_TCode"",T1.""U_Currency"",T1.""U_URef"" from ""@T_OIDT"" T1 Join ""@T_OIHT"" T2 "
                        strqry += " On T2.""Code"" =  T1.""U_HRef"" and T2.""Code""='" + strHCode + "'"
                        strqry += "   group by T1.""U_TCode"",T1.""U_Currency"",T1.""U_URef"""
                        oRecordSet.DoQuery(strqry)
                        Dim dblDebitTransaction As Double
                        Dim dblCredidtTransaction As Double
                        Dim oDBRec As SAPbobsCOM.Recordset

                        oDBRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

                        For intLoop As Integer = 0 To oRecordSet.RecordCount - 1
                            Dim strTCode, strTCurrency, strURef As String
                            strTCode = oRecordSet.Fields.Item(1).Value
                            strTCurrency = oRecordSet.Fields.Item(2).Value
                            strURef = oRecordSet.Fields.Item("U_Ref").Value

                            strqry = "Select Sum(T1.""U_Amount"")  As ""DAmount"",T1.""U_TCode"",T1.""U_Currency"" from ""@T_OIDT"" T1 Join ""@T_OIHT"" T2 "
                            strqry += " On T2.""Code"" =  T1.""U_HRef"" and T2.""Code""='" + strHCode + "'"
                            strqry += " where  T1.""U_DCS""='D' and T1.""U_TCode""='" & strTCode & "' and T1.""U_URef""='" & strURef & "' and T1.""U_Currency""='" & strTCurrency & "'  group by T1.""U_TCode"",T1.""U_Currency"""
                            oDBRec.DoQuery(strqry)
                            dblDebitTransaction = oDBRec.Fields.Item(0).Value

                            strqry = "Select Sum(T1.""U_Amount"")  As ""DAmount"",T1.""U_TCode"",T1.""U_Currency"" from ""@T_OIDT"" T1 Join ""@T_OIHT"" T2 "
                            strqry += " On T2.""Code"" =  T1.""U_HRef"" and T2.""Code""='" + strHCode + "'"
                            strqry += " where  T1.""U_DCS""='C' and T1.""U_TCode""='" & strTCode & "'  and T1.""U_URef""='" & strURef & "' and T1.""U_Currency""='" & strTCurrency & "'  group by T1.""U_TCode"",T1.""U_Currency"""
                            oDBRec.DoQuery(strqry)
                            dblCredidtTransaction = oDBRec.Fields.Item(0).Value

                            If dblCredidtTransaction <> dblDebitTransaction Then
                                traceService(strLogFileName, GPath, " ERROR : Difference in Debit (" + DValue.ToString + ") And Credit (" + CValue.ToString + ") for Transaction Code : " & strTCode & " Currency Code : " & strTCurrency & " BP Reference : " & strURef & " : Header Ref: " & strHCode & "  Values in :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                traceService(strLogFileName_Failure, ELOGPath, "ERROR :  Difference in Debit (" + DValue.ToString + ") And Credit  (" + CValue.ToString + ") for Transaction Code : " & strTCode & " Currency Code : " & strTCurrency & " BP Reference : " & strURef & " : Header Ref: " & strHCode & "  Values in:" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                '  MoveToFolder(strType, strFName, "E", acompany)
                                strquery = "Update ""@T_OIHT"" Set ""U_Status""='N' , ""U_Error"" = " + "'Difference in Debit And Credit Values'" + " Where ""Code""='" + strHCode + "'"
                                oDBRec.DoQuery(strquery)
                                strquery = "Update T1 Set T1.""U_Status"" ='N' , T1.""U_Error""=" + "'Difference in Debit And Credit Values'" + ""
                                strquery += " From ""@T_OIDT"" T1 join ""@T_OIHT"" T2 On T2.""Code""=T1.""U_HRef"""
                                strquery += " Where T2.""Code""='" & strHCode & "' and T1.""U_URef""='" & strURef & "' and T1.""U_TCode""='" & strTCode & "' and T1.""U_Currency""='" & strTCurrency & "'"
                                oDBRec.DoQuery(strquery)
                                '_retVal = False
                                blnIsError = True
                            End If
                            oRecordSet.MoveNext()
                        Next
                    End If
                Else

                    'For Invoices

                    '1. Checking Customer Code
                    '===============================
                    strquery = "Select Concat('InValid BP Code Code: ' , T0.""U_BPD"")  As ""Error"",T0.""U_BPD"",T0.""U_Text"" From ""@T_OPDT"" T0"
                    strquery += " Join ""@T_OPHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                    strquery += " Where (T0.""U_AType"" = 'N')"
                    oRecordSet.DoQuery(strquery)

                    For intRow As Integer = 0 To oRecordSet.RecordCount - 1
                        Dim strText As String = oRecordSet.Fields.Item(1).Value & ":" & oRecordSet.Fields.Item(2).Value
                        traceService(strLogFileName, GPath, " : Error: BP Code  Not Exist in  : BPCode  : " & strText & ": " & strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                        traceService(strLogFileName_Failure, ELOGPath, strText & " : Error: BP Code  Not Exist in  : BPCode  :  " & strText & ":" & strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                        blnIsError = True
                        oRecordSet.MoveNext()
                    Next


                    strquery = "Select Concat('InValid BP Code Code: ' , T0.""U_BPD"")  As ""Error"",T0.""U_BPD"",T0.""U_Text"" From ""@T_OPDT"" T0"
                    strquery += " Join ""@T_OPHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                    strquery += " Where (T0.""U_AType"" = 'N')"
                    oRecordSet.DoQuery(strquery)

                    If oRecordSet.RecordCount > 0 Then
                        ' traceService(strLogFileName, GPath, "Error: BP Code  Not Exist in :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                        ' traceService(strLogFileName_Failure, ELOGPath, "Error: BP Code Not Exist in :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                        ' MoveToFolder(strType, strFName, "E", acompany)
                        strquery = "Update ""@T_OPHT"" Set ""U_Status""='N' , ""U_Error"" = " + "'BP Code Code Not Exist'" + " Where ""Code""='" + strHCode + "'"
                        oRecordSet.DoQuery(strquery)
                        strquery = " Update T1 Set T1.""U_Status"" ='N' , T1.""U_Error""=" + "'BP Code Code Not Exist'" + ""
                        strquery += " From ""@T_OPDT"" T1 join ""@T_OPHT"" T2 On T2.""Code""=T1.""U_HRef"""
                        strquery += " Where T2.""Code""='" + strHCode + "'"
                        strquery += " And ""U_AType"" = 'N' "
                        oRecordSet.DoQuery(strquery)
                        ' _retVal = False
                        blnIsError = True

                    End If

                    '4. Transaction Code
                    '===============================
                    If _retVal Then
                        strquery = " Select Concat('InValid Item Code: ' , T0.""U_TCode"")  As ""Error"",T0.""U_TCode"",T0.""U_Text"" From ""@T_OPDT"" T0"
                        strquery += " Left Outer JOIN OITM T1 On T1.""ItemCode"" = T0.""U_TCode"""
                        strquery += " Join ""@T_OPHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                        strquery += " Where (T1.""ItemCode"" Is Null)"
                        oRecordSet.DoQuery(strquery)

                        For intRow As Integer = 0 To oRecordSet.RecordCount - 1
                            Dim strText As String = oRecordSet.Fields.Item(1).Value & ":" & oRecordSet.Fields.Item(2).Value
                            traceService(strLogFileName, GPath, " : Error: ItemCode Not Exist in  : Item Code : " & strText & ": " & strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            traceService(strLogFileName_Failure, ELOGPath, strText & " : Error: ItemCode Not Exist in  : Item Code :  " & strText & ":" & strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            blnIsError = True
                            oRecordSet.MoveNext()
                        Next

                        strquery = " Select Concat('InValid Item Code: ' , T0.""U_TCode"")  As ""Error"",T0.""U_TCode"",T0.""U_Text"" From ""@T_OPDT"" T0"
                        strquery += " Left Outer JOIN OITM T1 On T1.""ItemCode"" = T0.""U_TCode"""
                        strquery += " Join ""@T_OPHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                        strquery += " Where (T1.""ItemCode"" Is Null)"
                        oRecordSet.DoQuery(strquery)
                        If oRecordSet.RecordCount > 0 Then
                            ' traceService(strLogFileName, GPath, "Error: Item Code Not Defined :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            ' traceService(strLogFileName_Failure, ELOGPath, "Error: Item Code Not Defined :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            '  MoveToFolder(strType, strFName, "E", acompany)
                            strquery = "Update ""@T_OPHT"" Set ""U_Status""='N' , ""U_Error"" = " + "'Item Code Not Defined'" + " Where ""Code""='" + strHCode + "'"
                            oRecordSet.DoQuery(strquery)
                            strquery = "Update T1 Set T1.""U_Status"" ='N' , T1.""U_Error""=" + "'Item Code Not Defined'" + ""
                            strquery += " From ""@T_OPDT"" T1 join ""@T_OPHT"" T2 On T2.""Code""=T1.""U_HRef"""
                            strquery += " Where T2.""Code""='" + strHCode + "'"
                            oRecordSet.DoQuery(strquery)
                            '  _retVal = False
                            blnIsError = True
                        End If
                    End If
                    '===============================


                    '5. iv.	Duplicate import validation based on File Reference no (header) and Sequence No (Lines)
                    '===============================
                    If blnIsError = True Then
                        _retVal = False
                    End If
                    If _retVal Then
                        strquery = " Select Concat('Duplicate Import Found : ' , T2.""U_FRefNo"")  As ""Error"" From ""@T_OPDT"" T0"
                        strquery += " Join ""@T_OPHT"" T2 On T2.""Code"" =  T0.""U_HRef"" and T2.""Code"" = '" + strHCode + "'"
                        strquery += "AND T0.""U_DStatus"" ='Y' "

                        oRecordSet.DoQuery(strquery)

                        If oRecordSet.RecordCount > 0 Then

                            traceService(strLogFileName, GPath, "Error: Duplicate Import Found : :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            traceService(strLogFileName_Failure, ELOGPath, "Error: Duplicate Import Found : :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))

                            ' MoveToFolder(strType, strFName, "E", acompany)

                            strquery = "Update ""@T_OPHT"" Set ""U_Status""='N' , ""U_Error"" = " + "'Duplicate Import Found'" + " Where ""Code""='" + strHCode + "'"
                            oRecordSet.DoQuery(strquery)

                            strquery = "Update T1 Set T1.""U_Status"" ='N' , T1.""U_Error""=" + "'Duplicate Import Found'" + ""
                            strquery += " From ""@T_OPDT"" T1 join ""@T_OPHT"" T2 On T2.""Code""=T1.""U_HRef"""
                            strquery += " Where T2.""Code""='" + strHCode + "'"
                            strquery += " And ""U_DStatus"" = 'Y' "
                            oRecordSet.DoQuery(strquery)

                            ' _retVal = False
                            blnIsError = True
                        End If
                    End If
                    '===============================

                End If

                If blnIsError = True Then
                    Dim oTest As SAPbobsCOM.Recordset
                    oTest = acompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oTest.DoQuery("Delete from ""@T_OIFT"" where ""U_HRef""='" & strHCode & "'")
                    oTest.DoQuery("Delete from ""@T_OIDT"" where ""U_HRef""='" & strHCode & "'")
                    oTest.DoQuery("Delete from ""@T_OIHT"" where ""Code""='" & strHCode & "'")
                    MoveToFolder(strType, strFName, "E", acompany)
                    _retVal = False
                End If
                If _retVal Then
                    traceService(strLogFileName, SLOGPath, "Validated Data Successfully... @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                End If

                Return _retVal

            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function

    Private Sub JECreation(ByVal acompany As SAPbobsCOM.Company, ByVal strType As String, ByVal strFName As String, ByVal strHCode As String)
        Try
            Try
                Dim ELOGPath As String
                Dim GPath As String
                Dim SLOGPath As String
                Dim strLogFileName As String = "MI Integration_" & strType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                Dim strLogFileName_Success As String = "MI Integration_Success_" & strType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                Dim strLogFileName_Failure As String = "MI Integration_Failure_" & strType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                Dim _retVal As Boolean = False
                Dim intEmpCount As Integer = 0

              
                GPath = GetGeneralLogPath(strType, acompany)
                ELOGPath = GetErrorLogPath(strType, acompany)
                SLOGPath = GetSuccessLogPath(strType, acompany)
                If strType = "JE" Then
                    traceService(strLogFileName, GPath, "Creating Journal Entries for :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                    'Create Journal Entries
                    Dim intVal As Integer = 1 ' oApplication.SBO_Application.MessageBox("Want to Create Journal Voucher?", 2, "Yes", "No", "")
                    If intVal = 1 Then
                        Try
                            Dim oJERef As String
                            If (addJournal_GL(acompany, strFName, strHCode, oJERef, strType)) Then
                                traceService(strLogFileName, GPath, "Journal Entry Created Successfully :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                traceService(strLogFileName_Success, SLOGPath, "Journal Entry Created Successfully :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                'Move the File to Success
                                MoveToFolder(strType, strFName, "S", acompany)
                            Else
                                traceService(strLogFileName, GPath, "JE Creation Failed : Documents are Rolled Back " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                traceService(strLogFileName_Failure, ELOGPath, "JE Creation Failed :Documents are Rolled Back " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                'Move the File to Error Folder
                                MoveToFolder(strType, strFName, "E", acompany)
                            End If
                        Catch ex As Exception
                            traceService(strLogFileName, GPath, "JE Creation Failed : Error : " + ex.Message + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            traceService(strLogFileName_Failure, ELOGPath, "JE Creation Failed : Error : " + ex.Message + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            'Move the File to Error Folder
                            MoveToFolder(strType, strFName, "E", acompany)
                        End Try
                    End If
                End If

            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ARCreation(ByVal acompany As SAPbobsCOM.Company, ByVal strType As String, ByVal strFName As String, ByVal strHCode As String)
        Try
            Try
                Dim ELOGPath As String
                Dim GPath As String
                Dim SLOGPath As String
                Dim strLogFileName As String = "MI Integration_" & strType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                Dim strLogFileName_Success As String = "MI Integration_Success_" & strType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                Dim strLogFileName_Failure As String = "MI Integration_Failure_" & strType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
                Dim _retVal As Boolean = False
                GPath = GetGeneralLogPath(strType, acompany)
                ELOGPath = GetErrorLogPath(strType, acompany)
                SLOGPath = GetSuccessLogPath(strType, acompany)
                If strType = "AR" Then
                    traceService(strLogFileName, GPath, "Creating Delivery Documents for :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                    'Create Journal Entries
                    Dim intVal As Integer = 1 ' oApplication.SBO_Application.MessageBox("Want to Create Journal Voucher?", 2, "Yes", "No", "")
                    If intVal = 1 Then
                        Try
                            Dim oARRef As String = String.Empty
                            If (addInvoice_AR(acompany, strFName, strHCode, oARRef)) Then
                                traceService(strLogFileName, GPath, "Delivery Document Created Successfully :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                traceService(strLogFileName_Success, SLOGPath, "Delivery Documents Successfully :" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                'Move the File to Success
                                MoveToFolder(strType, strFName, "S", acompany)
                            Else
                                traceService(strLogFileName, GPath, "Delivery Document Creation Failed : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                traceService(strLogFileName_Failure, ELOGPath, "Delivery Document Creation Failed : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                'Move the File to Error Folder
                                MoveToFolder(strType, strFName, "E", acompany)
                            End If
                        Catch ex As Exception
                            traceService(strLogFileName, GPath, "Delivery Document Creation Failed : " + ex.Message + ":" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            traceService(strLogFileName_Failure, ELOGPath, "Delivery Document Creation Failed : " + ex.Message + ":" + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            'Move the File to Error Folder
                            MoveToFolder(strType, strFName, "E", acompany)
                        End Try
                    End If
                End If

            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub CreateMissingJournalEntries(aCompany As SAPbobsCOM.Company, aType As String, aRef As String)
        Dim oMRec As SAPbobsCOM.Recordset
        oMRec = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        If blnIsHANA = True Then
            oMRec.DoQuery("Select ""U_FileName"",""U_HRef"",Count(*) from ""@T_OIDT"" where ""U_HRef""='" & aRef & "' and  IFNULL(""U_JRef"",'0')='0' group by ""U_FileName"",""U_HRef""")
        Else
            oMRec.DoQuery("Select ""U_FileName"",""U_HRef"",Count(*) from ""@T_OIDT"" where ""U_HRef""='" & aRef & "' and  IsNULL(""U_JRef"",'0')='0' group by ""U_FileName"",""U_HRef""")
        End If
        For intMainLoop As Integer = 0 To oMRec.RecordCount - 1
            addJournal_GL(aCompany, oMRec.Fields.Item(0).Value, oMRec.Fields.Item(1).Value, "", "JE")
            oMRec.MoveNext()
        Next

        ''Opening the Files.
        Try
            '    Dim ELOGPath As String
            '    Dim GPath, strGLog As String
            '    Dim SLOGPath As String
            '    Dim strType As String = "JE"
            '    Dim strSLog, strFLog As String
            '    Dim oPath As SAPbobsCOM.Recordset
            '    oPath = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            '    strGLog = ""
            '    strSLog = ""
            '    strFLog = ""
            '    GPath = GetGeneralLogPath(strType, aCompany)
            '    ELOGPath = GetErrorLogPath(strType, aCompany)
            '    SLOGPath = GetSuccessLogPath(strType, aCompany)
            '    ' strGLog = oPath.Fields.Item("U_LOGGPath").Value
            '    oPath.DoQuery("Select * from ""@T_OIFS""")
            '    strGLog = oPath.Fields.Item("U_LOGGPath").Value
            '    strSLog = oPath.Fields.Item("U_LOGSPath").Value
            '    strFLog = oPath.Fields.Item("U_LOGFPath").Value
            '    Dim strTypeFromUDT As String = "JE"
            '    Dim strLogFileName As String = "MI Integration_" & strTypeFromUDT & "_" & System.DateTime.Now.ToString("yyyyMMdd")
            '    Dim strLogFileName_Success As String = "MI Integration_Success_" & strTypeFromUDT & "_" & System.DateTime.Now.ToString("yyyyMMdd")
            '    Dim strLogFileName_Failure As String = "MI Integration_Failure_" & strTypeFromUDT & "_" & System.DateTime.Now.ToString("yyyyMMdd")
            '    Dim strGPath_L As String = strGLog & "\" & strLogFileName & ".txt"
            '    Dim strSPath_L As String = strSLog & "\" & strLogFileName_Success & ".txt"
            '    Dim strFPath_L As String = strFLog & "\" & strLogFileName_Failure & ".txt"
            '    If File.Exists(strGPath_L) Then Process.Start(strGPath_L)
            '    If File.Exists(strSPath_L) Then Process.Start(strSPath_L)
            '    If File.Exists(strFPath_L) Then Process.Start(strFPath_L)
        Catch ex As Exception
        End Try
    End Sub

    Public Function addMissingJournal_GL(aCompany As SAPbobsCOM.Company, ByVal strFName As String, ByVal strHCode As String, ByRef oJERef As String, aType As String) As Boolean
        Try
            Dim _retVal As Boolean = False
            Dim intStatus As Int16 = 0
            Dim strQuery As String = String.Empty
            Dim intRow As Integer = 1
            Dim dblAmount As Double
            Dim blnHasRow As Boolean = False
            Dim intRCount As Integer
            Dim strTCode, strType, strTCurrency, strURef As String
            Dim ELOGPath As String
            Dim GPath As String
            Dim SLOGPath As String
            Dim blnisError As Boolean = False
            Dim oRecordSet, oRS As SAPbobsCOM.Recordset
            Dim oJE As SAPbobsCOM.JournalEntries
            strType = aType
            Dim strLC As String = aCompany.GetCompanyService().GetAdminInfo().LocalCurrency
            Dim strLogFileName As String = "MI Integration_" & aType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
            Dim strLogFileName_Success As String = "MI Integration_Success_" & strType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
            Dim strLogFileName_Failure As String = "MI Integration_Failure_" & strType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
            strType = "JE"
            GPath = GetGeneralLogPath(strType, aCompany)
            ELOGPath = GetErrorLogPath(strType, aCompany)
            SLOGPath = GetSuccessLogPath(strType, aCompany)
            oJE = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)
            oRecordSet = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strQuery = " Select Count(*), T1.""U_TCode"",T1.""U_Currency"",T1.""U_URef"" "
            strQuery += " From ""@T_OIDT"" T1 Join ""@T_OIHT"" T2 "
            If blnIsHANA = True Then
                strQuery += " On T2.""Code"" =  T1.""U_HRef"" where IFNULL(T1.""U_JRef"",'0')='0' and  T2.""Code""='" + strHCode + "' group by T1.""U_TCode"",T1.""U_Currency"" ,T1.""U_URef"""
            Else
                strQuery += " On T2.""Code"" =  T1.""U_HRef"" where ISNULL(T1.""U_JRef"",'0')='0' and  T2.""Code""='" + strHCode + "' group by T1.""U_TCode"",T1.""U_Currency"" ,T1.""U_URef"""
            End If

            oRecordSet.DoQuery(strQuery)
            Dim oStatic As SAPbouiCOM.StaticText
            If oRecordSet.RecordCount > 0 Then
                intRCount = oRecordSet.RecordCount
                Dim intEmpCount2 As Integer = 0
                oApplication.Utilities.Message("Processing GL Creation Ref : " & strHCode, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                For iRow As Integer = 0 To oRecordSet.RecordCount - 1 ' intRCount - 1
                    intRow = 1
                    strTCode = oRecordSet.Fields.Item("U_TCode").Value
                    strTCurrency = oRecordSet.Fields.Item("U_Currency").Value
                    strURef = oRecordSet.Fields.Item("U_URef").Value
                    traceService(strLogFileName, GPath, "JE Creation Processing : Transaction Code : " & strTCode & " : Currency : " & strTCurrency & ": Journal Reference : " & strURef & " : File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                    Dim aMainForm2 As SAPbouiCOM.Form = frmMainForm ' oApplication.SBO_Application.Forms.GetForm("169", 1)
                    If aMainForm2 Is Nothing Then
                        ' Return False
                    Else
                        If intEmpCount2 > 5 Then
                            aMainForm2.Update()
                            oApplication.Utilities.Message("Processing GL Creation Ref : " & strHCode & ": Transaction Code : " & strTCode, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            intEmpCount2 = 0
                        End If
                        intEmpCount2 = intEmpCount2 + 1
                    End If

                    Try
                        frmSourceForm.Select()
                        oStatic = frmSourceForm.Items.Item("50").Specific
                        oStatic.Caption = "Processing GL Creation Ref : " & strHCode & ": Transaction Code : " & strTCode & " Currency : " & strTCurrency
                    Catch ex As Exception
                    End Try

                    Try
                        oRS = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        oJE = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)
                        strQuery = " Select T1.""U_GLAc"" AS ""ACCode"", T1.""U_DCS"" AS ""DCS"",T1.""U_Amount"" AS ""Amount"", T1.""U_Description"", "
                        strQuery += " T3.""CurrCode"",T1.""U_RSequence"",T2.""U_FileName"",T2.""U_ICode"",T2.""U_FRefNo"",T2.""U_FileDate"",T1.""U_NCode"",T1.""U_DLine"",T1.""U_AType"",T1.""U_ACode""  "
                        strQuery += " ,T1.""U_FileDate1"",T1.""U_BPD"",T1.""U_ICode"" ""TCode"" From ""@T_OIDT"" T1 Join ""@T_OIHT"" T2 "
                        strQuery += " On T2.""Code"" =  T1.""U_HRef"" "
                        strQuery += " Join OCRN T3 On T3.""U_CurCode"" =  T1.""U_Currency"""
                        If blnisError = True Then
                            strQuery += " Where IFNULL(T1.""U_URef"",'')='" & strURef & "' and  IFNULL(T1.""U_JRef"",'0')='0' and  T1.""U_Currency""='" + strTCurrency + "' and  T1.""U_TCode"" ='" + strTCode + "' and T2.""Code""='" + strHCode + "'"
                        Else
                            strQuery += " Where ISNULL(T1.""U_URef"",'')='" & strURef & "' and  ISNULL(T1.""U_JRef"",'0')='0' and  T1.""U_Currency""='" + strTCurrency + "' and  T1.""U_TCode"" ='" + strTCode + "' and T2.""Code""='" + strHCode + "'"
                        End If
                        oRS.DoQuery(strQuery)
                        If Not oRS.EoF Then
                            oJE = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)
                            Dim strDate As String = oRS.Fields.Item("U_FileDate").Value
                            Dim strValueDate As String = oRS.Fields.Item("U_FileDate1").Value
                            Dim intDay, intmonth, intyear As Integer
                            intDay = CInt(strDate.Substring(0, 2))
                            intmonth = CInt(strDate.Substring(2, 2))
                            intyear = CInt(strDate.Substring(4, 4))
                            Dim dtDate As DateTime = New DateTime(intyear, intmonth, intDay) ' ConvertStrToDate(strDate, "yyyyMMdd")
                            intDay = CInt(strValueDate.Substring(0, 2))
                            intmonth = CInt(strValueDate.Substring(2, 2))
                            intyear = CInt(strValueDate.Substring(4, 4))
                            Dim dtDate1 As DateTime = New DateTime(intyear, intmonth, intDay) ' ConvertStrToDate(strDate, "yyyyMMdd")
                            oJE.ReferenceDate = dtDate
                            oJE.TaxDate = dtDate
                            oJE.DueDate = dtDate
                            oJE.TransactionCode = strTCode
                            oJE.Reference = oRS.Fields.Item("U_FRefNo").Value
                            oJE.Reference2 = oRS.Fields.Item("U_DLine").Value
                            oJE.UserFields.Fields.Item("U_FileName").Value = oRS.Fields.Item("U_FileName").Value
                            oJE.UserFields.Fields.Item("U_InstituteCode").Value = oRS.Fields.Item("U_ICode").Value
                            oJE.UserFields.Fields.Item("U_Network").Value = oRS.Fields.Item("U_NCode").Value
                            '  oJE.Memo = oRS.Fields.Item("U_Description").Value
                            oJE.UserFields.Fields.Item("U_URef").Value = strURef
                            Dim intEmpCount As Integer = 0
                            Dim aMainForm As SAPbouiCOM.Form = frmMainForm ' oApplication.SBO_Application.Forms.GetForm("169", 1)
                            If aMainForm Is Nothing Then
                                ' Return False
                            Else
                                If intEmpCount > 20 Then
                                    aMainForm.Update()
                                    frmSourceForm.Select()
                                    oApplication.Utilities.Message("Processing....", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                    intEmpCount = 0
                                End If
                                intEmpCount = intEmpCount + 1
                            End If
                            Try
                                oStatic = frmSourceForm.Items.Item("50").Specific
                                oStatic.Caption = "Processing GL Creation Ref : " & strHCode & ": Transaction Code : " & strTCode & " Currency : " & strTCurrency
                            Catch ex As Exception

                            End Try
                            Dim intSLine As Integer = 0
                            While Not oRS.EoF
                                Dim aMainForm21 As SAPbouiCOM.Form = frmMainForm ' oApplication.SBO_Application.Forms.GetForm("169", 1)
                                If aMainForm21 Is Nothing Then
                                    ' Return False
                                Else
                                    If intEmpCount > 50 Then
                                        aMainForm21.Update()
                                        frmSourceForm.Select()
                                        frmSourceForm.Items.Item("50").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                        oApplication.Utilities.Message("Processing....", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                        intEmpCount = 0
                                    End If
                                    intEmpCount = intEmpCount + 1
                                End If
                                blnHasRow = True
                                If intRow > 1 Then
                                    oJE.Lines.Add()
                                End If
                                oJE.Lines.SetCurrentLine(intSLine)
                                If oRS.Fields.Item("U_AType").Value = "A" Then
                                    oJE.Lines.AccountCode = oRS.Fields.Item("ACCode").Value.ToString()
                                ElseIf oRS.Fields.Item("U_AType").Value = "C" Then
                                    oJE.Lines.ShortName = oRS.Fields.Item("U_BPD").Value.ToString()
                                    oJE.Lines.ControlAccount = oRS.Fields.Item("ACCode").Value.ToString()
                                End If
                                dblAmount = CDbl((oRS.Fields.Item("Amount").Value) / 1000)
                                If strLC = oRS.Fields.Item("CurrCode").Value Then
                                    If oRS.Fields.Item("DCS").Value = "D" Then
                                        oJE.Lines.Debit = dblAmount
                                    Else
                                        oJE.Lines.Credit = dblAmount
                                    End If
                                Else
                                    oJE.Lines.FCCurrency = oRS.Fields.Item("CurrCode").Value
                                    If oRS.Fields.Item("DCS").Value = "D" Then
                                        oJE.Lines.FCDebit = dblAmount
                                    Else
                                        oJE.Lines.FCCredit = dblAmount
                                    End If
                                End If
                                oJE.Lines.DueDate = dtDate1
                                oJE.Lines.CostingCode = oRS.Fields.Item("U_ACode").Value
                                oJE.Lines.Reference1 = oRS.Fields.Item("U_FRefNo").Value
                                oJE.Lines.Reference2 = oRS.Fields.Item("U_DLine").Value
                                Dim strMemo As String = oRS.Fields.Item("U_Description").Value
                                If strMemo.Length >= 50 Then
                                    strMemo = strMemo.Substring(0, 49)
                                End If
                                oJE.Lines.LineMemo = strMemo ' oRS.Fields.Item("U_Description").Value
                                oJE.Lines.UserFields.Fields.Item("U_Description").Value = oRS.Fields.Item("U_Description").Value
                                'oJE.Lines.CostingCode2 = oRS.Fields.Item("U_ICode").Value
                                Dim strICode As String = oRS.Fields.Item("TCode").Value

                                oJE.Lines.CostingCode2 = oRS.Fields.Item("TCode").Value
                                oJE.Lines.UserFields.Fields.Item("U_Network").Value = oRS.Fields.Item("U_NCode").Value
                                '  oJE.Lines.CostingCode2 = oRS.Fields.Item("U_ACode").Value
                                oJE.Lines.UserFields.Fields.Item("U_RecordSeq").Value = oRS.Fields.Item("U_RSequence").Value
                                oRS.MoveNext()
                                intRow = intRow + 1
                                intSLine = intSLine + 1
                            End While
                            If blnHasRow Then
                                If intRow > 100 Then
                                    'System.Threading.Thread.Sleep(100 * 60)
                                    intStatus = oJE.Add()
                                    oApplication.SBO_Application.RemoveWindowsMessage(SAPbouiCOM.BoWindowsMessageType.bo_WM_TIMER, True)
                                Else
                                    intStatus = oJE.Add()
                                End If
                                If intStatus <> 0 Then
                                    blnisError = True
                                    traceService(strLogFileName, GPath, "JE Creation Failed : " & aCompany.GetLastErrorDescription & " : Journal Ref : " & strURef & " :  File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                    traceService(strLogFileName_Failure, ELOGPath, "JE Creation Failed : " & aCompany.GetLastErrorDescription & "  : Journal Ref : " & strURef & " :  File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                    traceService(strLogFileName, GPath, "JE Creation Failed : Documents are rolled back. File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                Else
                                    Dim StrDocnum As String
                                    aCompany.GetNewObjectCode(StrDocnum)
                                    Dim oJERec As SAPbobsCOM.Recordset
                                    oJERec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                    oJERec.DoQuery("Update ""@T_OIDT"" set ""U_JRef""='Y' , ""U_JRef""='" & StrDocnum & "' where T1.""U_URef""='" & strURef & "' and  ""U_Currency""='" & strTCurrency & "' and  ""U_HRef""='" & strHCode & "' and ""U_TCode""='" & strTCode & "'")
                                    traceService(strLogFileName, GPath, "Journal Entry Created successfully : Transaction Code : " & strTCode & " Currency : " & strTCurrency & "  : Journal Ref : " & strURef & " :  JE No : " & StrDocnum & " File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                    traceService(strLogFileName_Success, SLOGPath, "Journal Entry Created successfully : " & StrDocnum & " File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                        Return False
                    Finally
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oJE)
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oRS)
                    End Try
                    oRecordSet.MoveNext()
                Next
            End If
            If blnisError = True Then
                Return False
            End If

            Return True

        Catch ex As Exception

            Throw ex
        End Try
    End Function
    Public Function addJournal_GL(aCompany As SAPbobsCOM.Company, ByVal strFName As String, ByVal strHCode As String, ByRef oJERef As String, aType As String) As Boolean
        Try
            Dim _retVal As Boolean = False
            Dim intStatus As Int16 = 0
            Dim strQuery As String = String.Empty
            Dim intRow As Integer = 1
            Dim dblAmount As Double
            Dim blnHasRow As Boolean = False
            Dim intRCount As Integer
            Dim strTCode, strType, strTCurrency, strURef As String
            Dim ELOGPath As String
            Dim GPath As String
            Dim SLOGPath As String
            Dim blnisError As Boolean = False
            Dim oRecordSet, oRS As SAPbobsCOM.Recordset
            Dim oJE As SAPbobsCOM.JournalEntries
            strType = aType
            Dim strLC As String = aCompany.GetCompanyService().GetAdminInfo().LocalCurrency
            Dim strLogFileName As String = "MI Integration_" & aType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
            Dim strLogFileName_Success As String = "MI Integration_Success_" & strType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
            Dim strLogFileName_Failure As String = "MI Integration_Failure_" & strType & "_" & System.DateTime.Now.ToString("yyyyMMdd")
            strType = "JE"
            GPath = GetGeneralLogPath(strType, aCompany)
            ELOGPath = GetErrorLogPath(strType, aCompany)
            SLOGPath = GetSuccessLogPath(strType, aCompany)
            oJE = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)
            oRecordSet = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strQuery = " Select Count(*), T1.""U_TCode"",T1.""U_Currency"",T1.""U_URef"" "
            strQuery += " From ""@T_OIDT"" T1 Join ""@T_OIHT"" T2 "
            If blnIsHANA = True Then
                strQuery += " On T2.""Code"" =  T1.""U_HRef"" where IFNULL(T1.""U_JRef"",'0')='0' and  T2.""Code""='" + strHCode + "' group by T1.""U_TCode"",T1.""U_Currency"" ,T1.""U_URef"" order by Count(*) "
            Else
                strQuery += " On T2.""Code"" =  T1.""U_HRef"" where ISNULL(T1.""U_JRef"",'0')='0' and  T2.""Code""='" + strHCode + "' group by T1.""U_TCode"",T1.""U_Currency"",T1.""U_URef"" order by Count(*) "
            End If

            oRecordSet.DoQuery(strQuery)
            Dim oStatic As SAPbouiCOM.StaticText
            If oRecordSet.RecordCount > 0 Then
                intRCount = oRecordSet.RecordCount
                Dim intEmpCount2 As Integer = 0
                oApplication.Utilities.Message("Processing GL Creation Ref : " & strHCode, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                For iRow As Integer = 0 To oRecordSet.RecordCount - 1 ' intRCount - 1
                    intRow = 1
                    strTCode = oRecordSet.Fields.Item("U_TCode").Value
                    strTCurrency = oRecordSet.Fields.Item("U_Currency").Value
                    strURef = oRecordSet.Fields.Item("U_URef").Value

                    traceService(strLogFileName, GPath, "JE Creation Processing : Transaction Code : " & strTCode & " : Currency : " & strTCurrency & " : Line Reference : " & strURef & ": File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                    Dim aMainForm2 As SAPbouiCOM.Form = frmMainForm ' oApplication.SBO_Application.Forms.GetForm("169", 1)
                    If aMainForm2 Is Nothing Then
                        ' Return False
                    Else
                        If intEmpCount2 > 5 Then
                            aMainForm2.Update()
                            oApplication.Utilities.Message("Processing GL Creation Ref : " & strHCode & ": Transaction Code : " & strTCode, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            intEmpCount2 = 0
                        End If
                        intEmpCount2 = intEmpCount2 + 1
                    End If

                    Try
                        frmSourceForm.Select()
                        oStatic = frmSourceForm.Items.Item("50").Specific
                        oStatic.Caption = "Processing GL Creation Ref : " & strHCode & ": Transaction Code : " & strTCode & " Currency : " & strTCurrency & " : Journal Reference : " & strURef
                    Catch ex As Exception

                    End Try

                    Try
                        oRS = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        oJE = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)
                        strQuery = " Select T1.""U_GLAc"" AS ""ACCode"", T1.""U_DCS"" AS ""DCS"",T1.""U_Amount"" AS ""Amount"", T1.""U_Description"", "
                        strQuery += " T3.""CurrCode"",T1.""U_RSequence"",T2.""U_FileName"",T2.""U_ICode"",T2.""U_FRefNo"",T2.""U_FileDate"",T1.""U_NCode"",T1.""U_DLine"",T1.""U_AType"",T1.""U_ACode""  "
                        strQuery += " ,T1.""U_FileDate1"",T1.""U_BPD"",T1.""U_ICode"" ""TCode"" From ""@T_OIDT"" T1 Join ""@T_OIHT"" T2 "
                        strQuery += " On T2.""Code"" =  T1.""U_HRef"" "
                        strQuery += " Join OCRN T3 On T3.""U_CurCode"" =  T1.""U_Currency"""
                        If blnIsHANA = True Then
                            strQuery += " Where T1.""U_URef""='" & strURef & "' and  IFNULL(T1.""U_JRef"",'0')='0' and  T1.""U_Currency""='" + strTCurrency + "' and  T1.""U_TCode"" ='" + strTCode + "' and T2.""Code""='" + strHCode + "'"
                        Else
                            strQuery += " Where T1.""U_URef""='" & strURef & "' and  ISNULL(T1.""U_JRef"",'0')='0' and  T1.""U_Currency""='" + strTCurrency + "' and  T1.""U_TCode"" ='" + strTCode + "' and T2.""Code""='" + strHCode + "'"
                        End If
                        oRS.DoQuery(strQuery)
                        If Not oRS.EoF Then
                            Dim IntRowCount As Integer = oRS.RecordCount
                            oJE = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)

                            oRS.DoQuery(strQuery)
                            Dim strDate As String = oRS.Fields.Item("U_FileDate").Value
                            Dim strValueDate As String = oRS.Fields.Item("U_FileDate1").Value
                            Dim intDay, intmonth, intyear As Integer
                            intDay = CInt(strDate.Substring(0, 2))
                            intmonth = CInt(strDate.Substring(2, 2))
                            intyear = CInt(strDate.Substring(4, 4))
                            Dim dtDate As DateTime = New DateTime(intyear, intmonth, intDay) ' ConvertStrToDate(strDate, "yyyyMMdd")
                            intDay = CInt(strValueDate.Substring(0, 2))
                            intmonth = CInt(strValueDate.Substring(2, 2))
                            intyear = CInt(strValueDate.Substring(4, 4))
                            Dim dtDate1 As DateTime = New DateTime(intyear, intmonth, intDay) ' ConvertStrToDate(strDate, "yyyyMMdd")
                            oJE.ReferenceDate = dtDate
                            oJE.TaxDate = dtDate
                            oJE.DueDate = dtDate
                            oJE.TransactionCode = strTCode
                            oJE.Reference = oRS.Fields.Item("U_FRefNo").Value
                            oJE.Reference2 = oRS.Fields.Item("U_DLine").Value
                            oJE.UserFields.Fields.Item("U_FileName").Value = oRS.Fields.Item("U_FileName").Value
                            oJE.UserFields.Fields.Item("U_InstituteCode").Value = oRS.Fields.Item("U_ICode").Value
                            oJE.UserFields.Fields.Item("U_Network").Value = oRS.Fields.Item("U_NCode").Value
                            oJE.UserFields.Fields.Item("U_URef").Value = strURef
                            '  oJE.Memo = oRS.Fields.Item("U_Description").Value
                            If strLC = oRS.Fields.Item("CurrCode").Value Then
                                oJE.Indicator = "LC"
                            End If
                            Dim intEmpCount As Integer = 0
                            Dim aMainForm As SAPbouiCOM.Form = frmMainForm ' oApplication.SBO_Application.Forms.GetForm("169", 1)
                            If aMainForm Is Nothing Then
                                ' Return False
                            Else
                                If intEmpCount > 20 Then
                                    aMainForm.Update()
                                    frmSourceForm.Select()
                                    oApplication.Utilities.Message("Processing....", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                    intEmpCount = 0
                                End If
                                intEmpCount = intEmpCount + 1
                            End If
                            Try
                                oStatic = frmSourceForm.Items.Item("50").Specific
                                oStatic.Caption = "Processing GL Creation Ref : " & strHCode & ": Transaction Code : " & strTCode & " Currency : " & strTCurrency & " : Journal Reference : " & strURef
                            Catch ex As Exception

                            End Try
                            Dim intSLine As Integer = 0
                            While Not oRS.EoF
                                oStatic = frmSourceForm.Items.Item("50").Specific
                                oStatic.Caption = "Processing GL Creation Ref : " & strHCode & ": Transaction Code : " & strTCode & " Currency : " & strTCurrency & " : Journal Reference : " & strURef & " : Lines : " & intSLine & " of " & IntRowCount
                                Dim aMainForm21 As SAPbouiCOM.Form = frmMainForm ' oApplication.SBO_Application.Forms.GetForm("169", 1)
                                If aMainForm21 Is Nothing Then
                                    ' Return False
                                Else
                                    If intEmpCount > 50 Then
                                        aMainForm21.Update()
                                        frmSourceForm.Select()
                                        frmSourceForm.Items.Item("50").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                        oApplication.Utilities.Message("Processing....", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                        intEmpCount = 0
                                    End If
                                    intEmpCount = intEmpCount + 1
                                End If
                                blnHasRow = True
                                If intSLine > 0 Then
                                    oJE.Lines.Add()
                                End If
                                oJE.Lines.SetCurrentLine(intSLine)
                                If oRS.Fields.Item("U_AType").Value = "A" Then
                                    oJE.Lines.AccountCode = oRS.Fields.Item("ACCode").Value.ToString()
                                ElseIf oRS.Fields.Item("U_AType").Value = "C" Then
                                    oJE.Lines.ShortName = oRS.Fields.Item("U_BPD").Value.ToString()
                                    oJE.Lines.ControlAccount = oRS.Fields.Item("ACCode").Value.ToString()
                                End If
                                dblAmount = CDbl((oRS.Fields.Item("Amount").Value) / 1000)
                                If strLC = oRS.Fields.Item("CurrCode").Value Then
                                    If oRS.Fields.Item("DCS").Value = "D" Then
                                        oJE.Lines.Debit = dblAmount
                                    Else
                                        oJE.Lines.Credit = dblAmount
                                    End If
                                Else
                                    oJE.Lines.FCCurrency = oRS.Fields.Item("CurrCode").Value
                                    If oRS.Fields.Item("DCS").Value = "D" Then
                                        oJE.Lines.FCDebit = dblAmount
                                    Else
                                        oJE.Lines.FCCredit = dblAmount
                                    End If
                                End If
                                oJE.Lines.DueDate = dtDate1
                                oJE.Lines.CostingCode = oRS.Fields.Item("U_ACode").Value
                                oJE.Lines.Reference1 = oRS.Fields.Item("U_FRefNo").Value
                                oJE.Lines.Reference2 = oRS.Fields.Item("U_DLine").Value
                                Dim strMemo As String = oRS.Fields.Item("U_Description").Value
                                If strMemo.Length >= 50 Then
                                    strMemo = strMemo.Substring(0, 49)
                                End If
                                oJE.Lines.LineMemo = strMemo ' oRS.Fields.Item("U_Description").Value
                                oJE.Lines.UserFields.Fields.Item("U_Description").Value = oRS.Fields.Item("U_Description").Value
                                Dim strICode As String = oRS.Fields.Item("TCode").Value
                                oJE.Lines.CostingCode2 = oRS.Fields.Item("TCode").Value
                                oJE.Lines.UserFields.Fields.Item("U_Network").Value = oRS.Fields.Item("U_NCode").Value
                                '  oJE.Lines.CostingCode2 = oRS.Fields.Item("U_ACode").Value
                                oJE.Lines.UserFields.Fields.Item("U_RecordSeq").Value = oRS.Fields.Item("U_RSequence").Value
                                oJE.Lines.UserFields.Fields.Item("U_Currency").Value = strTCurrency
                                oRS.MoveNext()
                                intRow = intRow + 1
                                intSLine = intSLine + 1
                            End While
                            If blnHasRow Then
                                If intSLine > 100 Then
                                    'System.Threading.Thread.Sleep(100 * 60)
                                    intStatus = oJE.Add()
                                    oApplication.SBO_Application.RemoveWindowsMessage(SAPbouiCOM.BoWindowsMessageType.bo_WM_TIMER, True)
                                Else
                                    intStatus = oJE.Add()
                                End If
                                If intStatus <> 0 Then
                                    blnisError = True
                                    traceService(strLogFileName, GPath, "JE Creation Failed  : Transaction Code : " & strTCode & " Currency : " & strTCurrency & " : Journal Reference : " & strURef & " Error :" & aCompany.GetLastErrorDescription & " File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                    traceService(strLogFileName_Failure, ELOGPath, "JE Creation Failed : Transaction Code : " & strTCode & " Currency : " & strTCurrency & " : Journal Reference : " & strURef & " Error: " & aCompany.GetLastErrorDescription & " File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                    '    traceService(strLogFileName, GPath, "JE Creation Failed : Documents are rolled back. File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                Else
                                    Dim StrDocnum As String
                                    aCompany.GetNewObjectCode(StrDocnum)
                                    Dim oJERec As SAPbobsCOM.Recordset
                                    oJERec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                    oJERec.DoQuery("Update ""@T_OIDT"" set ""U_JEStatus""='Y' , ""U_JRef""='" & StrDocnum & "' where ""U_URef""='" & strURef & "' and  ""U_Currency""='" & strTCurrency & "' and  ""U_HRef""='" & strHCode & "' and ""U_TCode""='" & strTCode & "'")
                                    traceService(strLogFileName, GPath, "Journal Entry Created successfully : Transaction Code : " & strTCode & " Currency : " & strTCurrency & " : Journal Reference : " & strURef & ":  JE No : " & StrDocnum & " File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                    traceService(strLogFileName_Success, SLOGPath, "Journal Entry Created successfully : " & StrDocnum & " File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                    If frmSourceForm.TypeEx = frm_MissingJE Then
                                        '  Dim oobj As New clsMissingJE
                                        '  oobj.LoadTransactionDetails(frmSourceForm)
                                    End If
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                        Return False
                    Finally
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oJE)
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oRS)
                    End Try
                    oRecordSet.MoveNext()
                Next
            End If
            'If blnisError = True Then
            '    Return False
            'End If
            If frmSourceForm.TypeEx = frm_MissingJE Then
                oStatic = frmSourceForm.Items.Item("50").Specific
                oStatic.Caption = "Processing GL Creation Completed"
            End If
            
            Return True
            'Return _retVal
        Catch ex As Exception
            'If oApplication.Company.InTransaction Then
            '    oApplication.Company.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
            'End If
            'If aCompany.InTransaction Then
            '    aCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
            'End If
            Throw ex
        End Try
    End Function

    Private Function addInvoice_AR(aCompany As SAPbobsCOM.Company, ByVal strFName As String, ByVal strHCode As String, ByRef oARRef As String) As Boolean
        Try
            Dim _retVal As Boolean = False
            Dim intStatus As Int16 = 0
            Dim strQuery As String = String.Empty
            Dim intRow As Integer = 1
            Dim dblAmount As Double
            Dim blnHasRow As Boolean = False
            Dim intRCount As Integer
            Dim strTCode, strType As String
            Dim ELOGPath As String
            Dim GPath As String
            Dim SLOGPath As String
            Dim blnisError As Boolean = False

            Dim oRecordSet, oRS As SAPbobsCOM.Recordset
            Dim oAR As SAPbobsCOM.Documents
            oAR = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices)

            oRecordSet = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim strLC As String = aCompany.GetCompanyService().GetAdminInfo().LocalCurrency

            Dim strLogFileName As String = "MI Integration_AR_" & System.DateTime.Now.ToString("yyyyMMdd")
            Dim strLogFileName_Success As String = "MI Integration_Success_AR_" & System.DateTime.Now.ToString("yyyyMMdd")
            Dim strLogFileName_Failure As String = "MI Integration_Failure_AR_" & System.DateTime.Now.ToString("yyyyMMdd")

            strType = "AR"

            GPath = GetGeneralLogPath(strType, aCompany)
            ELOGPath = GetErrorLogPath(strType, aCompany)
            SLOGPath = GetSuccessLogPath(strType, aCompany)


            strQuery = " Select Distinct T1.""U_RecordID"" "
            strQuery += " From ""@T_OPDT"" T1 Join ""@T_OPHT"" T2 "
            strQuery += " On T2.""Code"" =  T1.""U_HRef"" and T2.""Code""='" + strHCode + "'"
            oRecordSet.DoQuery(strQuery)

            If oRecordSet.RecordCount > 0 Then
                intRCount = oRecordSet.RecordCount

                oApplication.Company.StartTransaction()

                For iRow As Integer = 0 To intRCount - 1
                    intRow = 1

                    strTCode = oRecordSet.Fields.Item("U_RecordID").Value

                    'oAR = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices)
                    oAR = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes)

                    strQuery = " Select T1.""U_BPD"" AS ""U_BPD"", T1.""U_Amount"" AS ""Amount"", T1.""U_Description"", "
                    strQuery += " T3.""CurrCode"",T1.""U_RSequence"",T2.""U_FileName"",T2.""U_ICode"",T2.""U_FRefNo"",T2.""U_FileDate"",T1.""U_NCode"",T1.""U_AType"",T1.""U_ACode"",T1.""U_TCode""  "
                    strQuery += " From ""@T_OPDT"" T1 Join ""@T_OPHT"" T2 "
                    strQuery += " On T2.""Code"" =  T1.""U_HRef"" and T2.""Code""='" + strHCode + "'"
                    strQuery += " Left Outer Join OCRN T3 On T3.""U_CurCode"" =  T1.""U_Currency"""
                    strQuery += " Where T1.""U_RecordID"" ='" + strTCode + "'"

                    oRS.DoQuery(strQuery)
                    If Not oRS.EoF Then

                        Dim strDate As String = oRS.Fields.Item("U_FileDate").Value
                        Dim intDay, intmonth, intyear As Integer
                        intDay = CInt(strDate.Substring(0, 2))
                        intmonth = CInt(strDate.Substring(2, 2))
                        intyear = CInt(strDate.Substring(4, 4))
                        Dim dtDate As DateTime = New DateTime(intyear, intmonth, intDay) ' ConvertStrToDate(strDate, "yyyyMMdd")

                        oAR.CardCode = oRS.Fields.Item("U_BPD").Value
                        oAR.DocDate = dtDate
                        oAR.TaxDate = dtDate
                        oAR.DocDueDate = dtDate
                        oAR.DocCurrency = oRS.Fields.Item("CurrCode").Value
                        oAR.UserFields.Fields.Item("U_FileName").Value = oRS.Fields.Item("U_FileName").Value
                        oAR.UserFields.Fields.Item("U_InstituteCode").Value = oRS.Fields.Item("U_ICode").Value
                        oAR.UserFields.Fields.Item("U_FileRef").Value = oRS.Fields.Item("U_FRefNo").Value

                        Dim intSLine As Integer = 0
                        While Not oRS.EoF

                            blnHasRow = True
                            If intRow > 1 Then
                                oAR.Lines.Add()
                            End If
                            oAR.Lines.SetCurrentLine(intSLine)
                            oAR.Lines.ItemCode = oRS.Fields.Item("U_TCode").Value.ToString()
                            oAR.Lines.ItemDescription = oRS.Fields.Item("U_Description").Value.ToString()
                            oAR.Lines.Quantity = oRS.Fields.Item("Amount").Value.ToString()
                            oAR.Lines.CostingCode2 = oRS.Fields.Item("U_ICode").Value
                            oAR.Lines.CostingCode = oRS.Fields.Item("U_ACode").Value
                            oAR.Lines.UserFields.Fields.Item("U_RecordSeq").Value = oRS.Fields.Item("U_RSequence").Value
                            oAR.Lines.UserFields.Fields.Item("U_Network").Value = oRS.Fields.Item("U_NCode").Value
                            oRS.MoveNext()
                            intRow = intRow + 1
                            intSLine = intSLine + 1
                        End While
                        If blnHasRow Then
                            intStatus = oAR.Add()
                            If intStatus <> 0 Then
                                blnisError = True
                                traceService(strLogFileName, GPath, "Delivery Document Creation Failed : " & aCompany.GetLastErrorDescription & " File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                traceService(strLogFileName_Failure, ELOGPath, "Delivery Document Creation Failed : " & aCompany.GetLastErrorDescription & " File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                traceService(strLogFileName, GPath, "Delivery Document Creation Failed : Documents are rolled back. File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            Else
                                Dim StrDocnum As String
                                aCompany.GetNewObjectCode(StrDocnum)
                                traceService(strLogFileName, GPath, "Delivery Document Created successfully : " & StrDocnum & " File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                                traceService(strLogFileName_Success, SLOGPath, "Delivery Document Created successfully : " & StrDocnum & " File Name : " + strFName + " @ " + System.DateTime.Now.ToString("hh:mm:ss"))
                            End If
                        End If
                    End If
                    oRecordSet.MoveNext()
                Next
            End If
            If blnisError = True Then
                If oApplication.Company.InTransaction Then
                    oApplication.Company.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                End If
                Return False
            End If
            If aCompany.InTransaction Then
                aCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
            End If
            Return True
            'Return _retVal
        Catch ex As Exception
            If oApplication.Company.InTransaction Then
                oApplication.Company.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
            End If
            Throw ex
        End Try
    End Function
    Private Sub MoveToFolder(ByVal strType As String, ByVal FName As String, ByVal Status As String, aCompany As SAPbobsCOM.Company)
        Try
            Dim OpenFolder As String
            Dim SuccessFolder As String
            Dim FailureFolder As String
            OpenFolder = GetOpenPath(strType, aCompany)
            Dim strFileName As String = OpenFolder & "\" + FName
            If Status = "S" Then
                SuccessFolder = GetSuccessPath(strType, aCompany)
                If File.Exists(strFileName) = True Then
                    If File.Exists(SuccessFolder + "\" + FName) Then
                        File.Delete(SuccessFolder + "\" + FName)
                    End If
                    System.IO.File.Move(strFileName, SuccessFolder + "\" + FName)
                End If
            Else
                FailureFolder = GetErrorPath(strType, aCompany)
                If File.Exists(strFileName) = True Then
                    If File.Exists(FailureFolder + "\" + FName) Then
                        File.Delete(FailureFolder + "\" + FName)
                    End If

                    System.IO.File.Move(strFileName, FailureFolder + "\" + FName)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetOpenPath(ByVal Type As String, aCompany As SAPbobsCOM.Company) As String
        Dim _retVal As String
        Try
            oRecordSet = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRecordSet.DoQuery("Select T1.""U_OPath"" From ""@T_OIFS"" T1 Where T1.""U_Type""='" & Type & "'")
            If Not oRecordSet.EoF Then
                _retVal = oRecordSet.Fields.Item(0).Value
            Else
                Throw New Exception("")
            End If
        Catch generatedExceptionName As Exception
            Throw
        End Try
        Return _retVal
    End Function

    Public Function GetSuccessPath(ByVal Type As String, aCompany As SAPbobsCOM.Company) As String
        Dim _retVal As String
        Try
            oRecordSet = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRecordSet.DoQuery("Select T1.""U_SPath"" From ""@T_OIFS"" T1 Where T1.""U_Type""='" & Type & "'")
            If Not oRecordSet.EoF Then
                _retVal = oRecordSet.Fields.Item(0).Value
            Else
                Throw New Exception("")
            End If
        Catch generatedExceptionName As Exception
            Throw
        End Try
        Return _retVal
    End Function

    Public Function GetErrorPath(ByVal Type As String, aCompany As SAPbobsCOM.Company) As String
        Dim _retVal As String
        Try
            oRecordSet = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRecordSet.DoQuery("Select T1.""U_FPath"" From ""@T_OIFS"" T1 Where T1.""U_Type""='" & Type & "'")
            If Not oRecordSet.EoF Then
                _retVal = oRecordSet.Fields.Item(0).Value
            Else
                Throw New Exception("")
            End If
        Catch generatedExceptionName As Exception
            Throw
        End Try
        Return _retVal
    End Function

    Public Function GetErrorLogPath(ByVal Type As String, aCompany As SAPbobsCOM.Company) As String
        Dim _retVal As String
        Try
            oRecordSet = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRecordSet.DoQuery("Select T1.""U_LOGFPath"" From ""@T_OIFS"" T1 Where T1.""U_Type""='" & Type & "'")
            If Not oRecordSet.EoF Then
                _retVal = oRecordSet.Fields.Item(0).Value
            Else
                Throw New Exception("")
            End If
        Catch generatedExceptionName As Exception
            Throw
        End Try
        Return _retVal
    End Function

    Public Function GetSuccessLogPath(ByVal Type As String, aCompany As SAPbobsCOM.Company) As String
        Dim _retVal As String
        Try
            oRecordSet = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRecordSet.DoQuery("Select T1.""U_LOGSPath"" From ""@T_OIFS"" T1 Where T1.""U_Type""='" & Type & "'")
            If Not oRecordSet.EoF Then
                _retVal = oRecordSet.Fields.Item(0).Value
            Else
                Throw New Exception("")
            End If
        Catch generatedExceptionName As Exception
            Throw
        End Try
        Return _retVal
    End Function

    Public Function GetGeneralLogPath(ByVal Type As String, aCompany As SAPbobsCOM.Company) As String
        Dim _retVal As String
        Try
            oRecordSet = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRecordSet.DoQuery("Select T1.""U_LOGGPath"" From ""@T_OIFS"" T1 Where T1.""U_Type""='" & Type & "'")
            If Not oRecordSet.EoF Then
                _retVal = oRecordSet.Fields.Item(0).Value
            Else
                Throw New Exception("")
            End If
        Catch generatedExceptionName As Exception
            Throw
        End Try
        Return _retVal
    End Function

#End Region


    Public Function generateBarCodes(ByVal aform As SAPbouiCOM.Form) As Boolean
        Dim strFromItem, strToItem, strBrand, strSeason, strSQL As String
        Dim ostatic As SAPbouiCOM.StaticText
        Dim oTempRec As SAPbobsCOM.Recordset
        Try
            strFromItem = getEditTextvalue(aform, "4")
            strToItem = getEditTextvalue(aform, "6")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = aform.Items.Item("8").Specific
            Try
                strSeason = oCombo.Selected.Value ' getEditTextvalue(aform, "8")
            Catch ex As Exception
                strSeason = ""
            End Try

            oCombo = aform.Items.Item("10").Specific
            Try
                strBrand = oCombo.Selected.Value ' getEditTextvalue(aform, "10")
            Catch ex As Exception
                strBrand = ""
            End Try

            If strFromItem = "" Then
                strFromItem = " (1=1"
            Else
                strFromItem = "( ""ItemCode"" >='" & strFromItem & "'"
            End If

            If strToItem = "" Then
                strFromItem = strFromItem & " and  1=1 )"
            Else
                strFromItem = strFromItem & " and  ""ItemCode"" <='" & strToItem & "')"
            End If
            If strSeason = "" Then
                strSeason = " and 1=1"
            Else
                strSeason = " and ""U_SEASON""='" & strSeason & "'"
            End If
            If strBrand = "" Then
                strBrand = " and 1=1"
            Else
                strBrand = " and ""U_BRAND""='" & strBrand & "'"
            End If
            strSQL = "Select ""ItemCode"",""ItemName"" from OITM where " & strFromItem & strSeason & strBrand
            oTempRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oTempRec.DoQuery(strSQL)
            ostatic = aform.Items.Item("11").Specific

            Dim ORec As SAPbobsCOM.Recordset
            ORec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            ORec.DoQuery("Select IFNULL(""U_S_BINCODE"",'') from OADM")
            Dim aBinCode As String
            aBinCode = ORec.Fields.Item(0).Value
            If aBinCode = "" Then
                oApplication.Utilities.Message("BinCode not defined in the Company Setup", SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return False
            Else
                aBinCode = aBinCode + getmaxBarCode("OBCD", "BcdCode")
                '  aBarCode = GenerateCheckDidgit(aBinCode)
            End If
            Dim intBarCode, aBarCode As String
            Dim dtTable As SAPbouiCOM.DataTable
            Dim ogrid As SAPbouiCOM.Grid

            ogrid = aform.Items.Item("12").Specific
            ogrid.DataTable = aform.DataSources.DataTables.Item("DT_0")
            aform.Items.Item("12").Visible = False
            dtTable = aform.DataSources.DataTables.Item("DT_1")
            dtTable.Rows.Clear()
            ' dtTable.ExecuteQuery("Select ItemCode,ItemName,CodeBars from OITM where ItemCode='dd'")
            For intRow As Integer = 0 To oTempRec.RecordCount - 1
                ostatic.Caption = "Processing ItemCode : " & oTempRec.Fields.Item(0).Value
                '     GenerateBarCode(oTempRec.Fields.Item(0).Value, "test")
                ORec.DoQuery("Select * from OBCD where ""ItemCode""='" & oTempRec.Fields.Item(0).Value & "'")
                If ORec.RecordCount <= 0 Then

                    aBarCode = GenerateCheckDidgit(aBinCode)
                    aBinCode = Convert.ToDouble(aBinCode) + 1
                    dtTable.Rows.Add()
                    dtTable.SetValue(0, dtTable.Rows.Count - 1, oTempRec.Fields.Item(0).Value)
                    dtTable.SetValue(1, dtTable.Rows.Count - 1, oTempRec.Fields.Item(1).Value)
                    dtTable.SetValue(2, dtTable.Rows.Count - 1, aBarCode)
                    ostatic.Caption = "Processing ItemCode : " & oTempRec.Fields.Item(0).Value & "BarCode : " & aBarCode
                End If
                oTempRec.MoveNext()
            Next
            ostatic.Caption = "Barcode prepared successfully"
            ogrid = aform.Items.Item("12").Specific
            ogrid.DataTable = dtTable
            ogrid.Columns.Item(0).TitleObject.Caption = "Item Code"
            Dim oedittext As SAPbouiCOM.EditTextColumn
            oedittext = ogrid.Columns.Item(0)
            oedittext.LinkedObjectType = "4"
            ogrid.Columns.Item(1).TitleObject.Caption = "Item Name"
            ogrid.Columns.Item(2).TitleObject.Caption = "BarCode"
            ogrid.AutoResizeColumns()
            ogrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_None
            aform.Items.Item("12").Visible = True
            Return True
        Catch ex As Exception
            Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        End Try
    End Function


    Public Function PrintBarCode(ByVal aform As SAPbouiCOM.Form) As Boolean
        Dim strFromItem, strToItem, strBrand, strSeason, strSQL As String
        Dim ostatic As SAPbouiCOM.StaticText
        Dim oTempRec As SAPbobsCOM.Recordset
        Try
            strFromItem = getEditTextvalue(aform, "4")
            strToItem = getEditTextvalue(aform, "6")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = aform.Items.Item("8").Specific
            Try
                strSeason = oCombo.Selected.Value ' getEditTextvalue(aform, "8")
            Catch ex As Exception
                strSeason = ""
            End Try

            oCombo = aform.Items.Item("10").Specific
            Try
                strBrand = oCombo.Selected.Value ' getEditTextvalue(aform, "10")
            Catch ex As Exception
                strBrand = ""
            End Try

            If strFromItem = "" Then
                strFromItem = " (1=1"
            Else
                strFromItem = "( ""ItemCode"" >='" & strFromItem & "'"
            End If

            If strToItem = "" Then
                strFromItem = strFromItem & " and  1=1 )"
            Else
                strFromItem = strFromItem & " and  ""ItemCode"" <='" & strToItem & "')"
            End If
            If strSeason = "" Then
                strSeason = " and 1=1"
            Else
                strSeason = " and ""U_SEASON""='" & strSeason & "'"
            End If
            If strBrand = "" Then
                strBrand = " and 1=1"
            Else
                strBrand = " and ""U_BRAND""='" & strBrand & "'"
            End If
            strSQL = "Select ""CodeBars"" ""BarCode"",""ItemCode"",""ItemName"",""U_SEASON"",""U_BRAND""  from OITM where " & strFromItem & strSeason & strBrand
            Dim ogrid As SAPbouiCOM.Grid
            ogrid = aform.Items.Item("12").Specific
            ogrid.DataTable = aform.DataSources.DataTables.Item("DT_0")
            ogrid.DataTable.ExecuteQuery(strSQL)
            aform.Items.Item("12").Visible = False
            ogrid = aform.Items.Item("12").Specific
            ogrid.DataTable.ExecuteQuery(strSQL)
            ogrid.Columns.Item("ItemCode").TitleObject.Caption = "Item Code"
            ogrid.Columns.Item("U_SEASON").TitleObject.Caption = "Season"
            ogrid.Columns.Item("U_BRAND").TitleObject.Caption = "Brand"
            Dim oedittext As SAPbouiCOM.EditTextColumn
            oedittext = ogrid.Columns.Item("ItemCode")
            oedittext.LinkedObjectType = "4"
            ogrid.AutoResizeColumns()
            ogrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_None
            aform.Items.Item("12").Visible = True
            Return True
        Catch ex As Exception
            Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        End Try
    End Function

    Public Function PrintBarCode_PO(ByVal aform As SAPbouiCOM.Form) As Boolean
        Dim strFromItem, strToItem, strBrand, strSeason, strSQL As String
        Dim ostatic As SAPbouiCOM.StaticText
        Dim oTempRec As SAPbobsCOM.Recordset
        Try
            strFromItem = getEditTextvalue(aform, "4")
            strToItem = getEditTextvalue(aform, "6")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = aform.Items.Item("8").Specific
            Try
                strSeason = oCombo.Selected.Value ' getEditTextvalue(aform, "8")
            Catch ex As Exception
                strSeason = ""
            End Try

            oCombo = aform.Items.Item("10").Specific
            Try
                strBrand = oCombo.Selected.Value ' getEditTextvalue(aform, "10")
            Catch ex As Exception
                strBrand = ""
            End Try

            If strFromItem = "" Then
                strFromItem = " (1=1"
            Else
                strFromItem = "( T0.""DocEntry"" ='" & strFromItem & "')"
            End If

            'If strToItem = "" Then
            '    strFromItem = strFromItem & " and  1=1 )"
            'Else
            '    strFromItem = strFromItem & " and  T0.""DocEntry"" <='" & strToItem & "')"
            'End If
            If strSeason = "" Then
                strSeason = " and 1=1"
            Else
                strSeason = " and ""U_SEASON""='" & strSeason & "'"
            End If
            If strBrand = "" Then
                strBrand = " and 1=1"
            Else
                strBrand = " and ""U_BRAND""='" & strBrand & "'"
            End If
            'strSQL = "Select ""CodeBars"" ""BarCode"",""ItemCode"",""ItemName"",""U_SEASON"" from OITM where " & strFromItem & strSeason & strBrand
            strSQL = "SELECT T1.""CodeBars"" ""BarCode"",T1.""ItemCode"",""ItemName"",T1.""Quantity"",T2.""U_SEASON"",T2.""U_BRAND"" FROM OPOR T0  INNER JOIN POR1 T1 ON T0.""DocEntry"" = T1.""DocEntry"" INNER JOIN OITM T2 ON T1.""ItemCode"" = T2.""ItemCode"" where " & strFromItem

            Dim ogrid As SAPbouiCOM.Grid
            ogrid = aform.Items.Item("12").Specific
            ogrid.DataTable = aform.DataSources.DataTables.Item("DT_0")
            ogrid.DataTable.ExecuteQuery(strSQL)
            aform.Items.Item("12").Visible = False
            ogrid = aform.Items.Item("12").Specific
            ogrid.DataTable.ExecuteQuery(strSQL)
            ogrid.Columns.Item("U_SEASON").TitleObject.Caption = "Season"
            ogrid.Columns.Item("U_BRAND").TitleObject.Caption = "Brand"
            Dim oedittext As SAPbouiCOM.EditTextColumn
            oedittext = ogrid.Columns.Item("ItemCode")
            oedittext.LinkedObjectType = "4"
            ogrid.AutoResizeColumns()
            ogrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_None
            aform.Items.Item("12").Visible = True
            Return True
        Catch ex As Exception
            Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        End Try
    End Function

    Public Sub PrintbarCode_Report_PO(ByVal aform As SAPbouiCOM.Form)
        Dim oRec, oRecBP, oTemp As SAPbobsCOM.Recordset
        Dim strSQL As String

        Dim dblTotal As Double = 0

        Dim strFromItem, strToItem, strBrand, strSeason As String
       
        Try
            strFromItem = getEditTextvalue(aform, "4")
            strToItem = getEditTextvalue(aform, "6")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = aform.Items.Item("8").Specific
            Try
                strSeason = oCombo.Selected.Value ' getEditTextvalue(aform, "8")
            Catch ex As Exception
                strSeason = ""
            End Try

            oCombo = aform.Items.Item("10").Specific
            Try
                strBrand = oCombo.Selected.Value ' getEditTextvalue(aform, "10")
            Catch ex As Exception
                strBrand = ""
            End Try

            If strFromItem = "" Then
                strFromItem = " (1=1"
            Else
                strFromItem = "( T0.""DocEntry"" ='" & strFromItem & "')"
            End If

            'If strToItem = "" Then
            '    strFromItem = strFromItem & " and  1=1 )"
            'Else
            '    strFromItem = strFromItem & " and  ""DocEntry"" <='" & strToItem & "')"
            'End If
            'If strSeason = "" Then
            '    strSeason = " and 1=1"
            'Else
            '    strSeason = " and ""U_SEASON""='" & strSeason & "'"
            'End If
            'If strBrand = "" Then
            '    strBrand = " and 1=1"
            'Else
            '    strBrand = " and ""U_BRAND""='" & strBrand & "'"
            'End If
            '  strSQL = "Select ""CodeBars"" ""BarCode"",""ItemCode"",""ItemName"",""U_SEASON"" from OITM where " & strFromItem & strSeason & strBrand
            ' strSQL = "SELECT T1.""CodeBars"" ""BarCode"",""ItemCode"",""ItemName"",T2.""U_SEASON"" FROM OPO""CodeBars"" ""BarCode"",""ItemCode"",""ItemName"",""U_SEASON""R T0  INNER JOIN POR1 T1 ON T0.""DocEntry"" = T1.""DocEntry"" INNER JOIN OITM T2 ON T1.""ItemCode"" = T2.""ItemCode"" where " & strFromItem
            strSQL = "SELECT T1.""CodeBars"" ""BarCode"",T1.""ItemCode"",""ItemName"",T2.""U_SEASON"",T1.""Quantity"",T2.""U_BRAND"" FROM OPOR T0  INNER JOIN POR1 T1 ON T0.""DocEntry"" = T1.""DocEntry"" INNER JOIN OITM T2 ON T1.""ItemCode"" = T2.""ItemCode"" where " & strFromItem
        Catch ex As Exception
            Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try

        oApplication.Utilities.Message("Processing...", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTemp = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRecBP = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRecBP.DoQuery(strSQL)

        If 1 = 2 Then ' oRec.RecordCount <= 0 Then
            oApplication.Utilities.Message("Payroll not generated for selected month and year", SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Exit Sub
        Else
            ds.Clear()
            ds.Clear()
            oTemp.DoQuery(strSQL)
            Dim dblQuantity As Double
            For introw As Integer = 0 To oTemp.RecordCount - 1
                dblQuantity = oTemp.Fields.Item("Quantity").Value
                For intLoop As Integer = 1 To dblQuantity


                    oDRow = ds.Tables("Barcode").NewRow()
                    oDRow.Item("Barcode") = oTemp.Fields.Item("BarCode").Value
                    oDRow.Item("ItemCode") = oTemp.Fields.Item("ItemCode").Value
                    oDRow.Item("ItemName") = oTemp.Fields.Item("ItemName").Value
                    oDRow.Item("Season") = oTemp.Fields.Item("U_SEASON").Value
                    '  MsgBox(oTemp.Fields.Item("U_BRAND").Value)
                    oDRow.Item("Brand") = oTemp.Fields.Item("U_BRAND").Value
                    oDRow.Item("Quantity") = 1 ' oTemp.Fields.Item("Quantity").Value
                    oRecBP.DoQuery("Select * from ""@S_OBPR""")
                    If oRecBP.RecordCount > 0 Then
                        oRecBP.DoQuery("Select * from ITM1 where ""ItemCode""='" & oTemp.Fields.Item("ItemCode").Value & "' and ""PriceList""=" & oRecBP.Fields.Item("Code").Value)
                    Else
                        oRecBP.DoQuery("Select * from ITM1 where ""ItemCode""='" & oTemp.Fields.Item("ItemCode").Value & "' and ""PriceList""=12")
                    End If

                    If oRecBP.RecordCount > 0 Then
                        oDRow.Item("Price") = oRecBP.Fields.Item("Price").Value
                    Else
                        oDRow.Item("Price") = 0
                    End If

                    ds.Tables("Barcode").Rows.Add(oDRow)
                Next
                oTemp.MoveNext()
            Next
            '   addCrystal(ds, "BarCode")
        End If
        ' oApplication.Utilities.Message("", SAPbouiCOM.BoStatusBarMessageType.smt_None)
    End Sub

    Public Sub PrintbarCode_Report(ByVal aform As SAPbouiCOM.Form)
        Dim oRec, oRecBP, oTemp As SAPbobsCOM.Recordset
      
        Dim strSQL As String
      

        Dim strFromItem, strToItem, strBrand, strSeason As String

        Try
            strFromItem = getEditTextvalue(aform, "4")
            strToItem = getEditTextvalue(aform, "6")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = aform.Items.Item("8").Specific
            Try
                strSeason = oCombo.Selected.Value ' getEditTextvalue(aform, "8")
            Catch ex As Exception
                strSeason = ""
            End Try

            oCombo = aform.Items.Item("10").Specific
            Try
                strBrand = oCombo.Selected.Value ' getEditTextvalue(aform, "10")
            Catch ex As Exception
                strBrand = ""
            End Try

            If strFromItem = "" Then
                strFromItem = " (1=1"
            Else
                strFromItem = "( ""ItemCode"" >='" & strFromItem & "'"
            End If

            If strToItem = "" Then
                strFromItem = strFromItem & " and  1=1 )"
            Else
                strFromItem = strFromItem & " and  ""ItemCode"" <='" & strToItem & "')"
            End If
            If strSeason = "" Then
                strSeason = " and 1=1"
            Else
                strSeason = " and ""U_SEASON""='" & strSeason & "'"
            End If
            If strBrand = "" Then
                strBrand = " and 1=1"
            Else
                strBrand = " and ""U_BRAND""='" & strBrand & "'"
            End If
            strSQL = "Select ""CodeBars"" ""BarCode"",""ItemCode"",""ItemName"",""U_SEASON"",""U_BRAND"" from OITM where " & strFromItem & strSeason & strBrand

        Catch ex As Exception
            Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)

        End Try

        oApplication.Utilities.Message("Processing...", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTemp = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRecBP = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRecBP.DoQuery(strSQL)

        If 1 = 2 Then ' oRec.RecordCount <= 0 Then
            oApplication.Utilities.Message("Payroll not generated for selected month and year", SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Exit Sub
        Else
            ds.Clear()
            ds.Clear()
            oTemp.DoQuery(strSQL)
            For introw As Integer = 0 To oTemp.RecordCount - 1
                oDRow = ds.Tables("Barcode").NewRow()
                oDRow.Item("Barcode") = oTemp.Fields.Item("BarCode").Value
                oDRow.Item("ItemCode") = oTemp.Fields.Item("ItemCode").Value
                oDRow.Item("ItemName") = oTemp.Fields.Item("ItemName").Value
                oDRow.Item("Season") = oTemp.Fields.Item("U_SEASON").Value
                oDRow.Item("Brand") = oTemp.Fields.Item("U_BRAND").Value

                ' oRecBP.DoQuery("Select * from ITM1 where ItemCode='" & oTemp.Fields.Item("ItemCode").Value & "' and PriceList=1")
                oRecBP.DoQuery("Select * from ""@S_OBPR""")
                If oRecBP.RecordCount > 0 Then
                    oRecBP.DoQuery("Select * from ITM1 where ""ItemCode""='" & oTemp.Fields.Item("ItemCode").Value & "' and ""PriceList""=" & oRecBP.Fields.Item("Code").Value)
                Else
                    oRecBP.DoQuery("Select * from ITM1 where ""ItemCode""='" & oTemp.Fields.Item("ItemCode").Value & "' and ""PriceList""=11")
                End If
                If oRecBP.RecordCount > 0 Then
                    oDRow.Item("Price") = oRecBP.Fields.Item("Price").Value
                Else
                    oDRow.Item("Price") = 0
                End If

                oDRow.Item("Quantity") = 1
                ds.Tables("Barcode").Rows.Add(oDRow)
                oTemp.MoveNext()
            Next
            '  addCrystal(ds, "BarCode")
        End If
        ' oApplication.Utilities.Message("", SAPbouiCOM.BoStatusBarMessageType.smt_None)
    End Sub
    'Private Sub addCrystal(ByVal ds1 As DataSet, ByVal aChoice As String)
    '    Dim strFilename, stfilepath As String
    '    Dim strReportFileName As String
    '    Dim strCompany As String
    '    strCompany = oApplication.Company.CompanyDB
    '    If aChoice = "BarCode" Then
    '        If strBarCodeFormat = "G" Then
    '            strReportFileName = "rptBarcode_" & strCompany & ".rpt"
    '        Else
    '            strReportFileName = "rptBarcode_Accessories_" & strCompany & ".rpt"
    '        End If

    '        strFilename = System.Windows.Forms.Application.StartupPath & "\BarCode"
    '    ElseIf aChoice = "Agreement" Then
    '        strReportFileName = "Agreement.rpt"
    '        strFilename = System.Windows.Forms.Application.StartupPath & "\Rental_Agreement"
    '    Else
    '        strReportFileName = "AcctStatement.rpt"
    '        strFilename = System.Windows.Forms.Application.StartupPath & "\AccountStatement"
    '    End If
    '    strReportFileName = strReportFileName
    '    strFilename = strFilename & ".pdf"
    '    stfilepath = System.Windows.Forms.Application.StartupPath & "\Reports\" & strReportFileName
    '    If File.Exists(stfilepath) = False Then
    '        oApplication.Utilities.Message("Report does not exists", SAPbouiCOM.BoStatusBarMessageType.smt_Error)
    '        Exit Sub
    '    End If
    '    If File.Exists(strFilename) Then
    '        File.Delete(strFilename)
    '    End If
    '    ' If ds1.Tables.Item("AccountBalance").Rows.Count > 0 Then
    '    If 1 = 1 Then
    '        cryRpt.Load(System.Windows.Forms.Application.StartupPath & "\Reports\" & strReportFileName)
    '        cryRpt.SetDataSource(ds1)
    '        If "T" = "T" Then
    '            Dim mythread As New System.Threading.Thread(AddressOf openFileDialog)
    '            mythread.SetApartmentState(ApartmentState.STA)
    '            mythread.Start()
    '            mythread.Join()
    '            ds1.Clear()
    '        Else
    '            Dim CrExportOptions As ExportOptions
    '            Dim CrDiskFileDestinationOptions As New  _
    '            DiskFileDestinationOptions()
    '            Dim CrFormatTypeOptions As New PdfRtfWordFormatOptions()
    '            CrDiskFileDestinationOptions.DiskFileName = strFilename
    '            CrExportOptions = cryRpt.ExportOptions
    '            With CrExportOptions
    '                .ExportDestinationType = ExportDestinationType.DiskFile
    '                .ExportFormatType = ExportFormatType.PortableDocFormat
    '                .DestinationOptions = CrDiskFileDestinationOptions
    '                .FormatOptions = CrFormatTypeOptions
    '            End With
    '            cryRpt.Export()
    '            cryRpt.Close()
    '            Dim x As System.Diagnostics.ProcessStartInfo
    '            x = New System.Diagnostics.ProcessStartInfo
    '            x.UseShellExecute = True
    '            x.FileName = strFilename
    '            System.Diagnostics.Process.Start(x)
    '            x = Nothing
    '            ' objUtility.ShowSuccessMessage("Report exported into PDF File")
    '        End If

    '    Else
    '        ' objUtility.ShowWarningMessage("No data found")
    '    End If

    'End Sub
    'Private Sub openFileDialog()
    '    Dim objPL As New frmReportViewer
    '    objPL.iniViewer = AddressOf objPL.GenerateReport
    '    objPL.rptViewer.ReportSource = cryRpt
    '    objPL.rptViewer.Refresh()
    '    objPL.WindowState = FormWindowState.Maximized
    '    objPL.ShowDialog()
    '    System.Threading.Thread.CurrentThread.Abort()
    'End Sub
    Public Function GenerateBarCode_Bulk(ByVal aItemCode As String, ByVal aBarCode As String) As String
        Dim ORec As SAPbobsCOM.Recordset
        Dim aBinCode As String
        Try
            ORec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            ORec.DoQuery("Select * from OBCD where ""ItemCode""='" & aItemCode & "'")
            If ORec.RecordCount > 0 Then
                ' Message("Barcode already exists", SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return ""
            Else
                ORec.DoQuery("Select IFNULL(""U_S_BinCode"",'') from OADM")
                aBinCode = ORec.Fields.Item(0).Value
                aBarCode = ""
                If aBinCode = "" Then
                    oApplication.Utilities.Message("BinCode not defined in the Company Setup", SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    Return ""
                Else
                    ' aBinCode = aBinCode + getmaxBarCode("OBCD", "BcdCode")
                    aBarCode = GenerateCheckDidgit(aBinCode)
                End If
                Return aBarCode

                'If AddBarCode(aItemCode, aBarCode) = True Then
                '    Return True
                'Else
                '    Return False
                'End If
            End If
        Catch ex As Exception
            Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        End Try
    End Function
    Public Function GenerateBarCode(ByVal aItemCode As String, ByVal aBarCode As String) As Boolean
        Dim ORec As SAPbobsCOM.Recordset
        Dim aBinCode As String
        Try
            ORec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            ORec.DoQuery("Select * from OBCD where ""ItemCode""='" & aItemCode & "'")
            If ORec.RecordCount > 0 Then
                ' Message("Barcode already exists", SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return True
            Else
                ORec.DoQuery("Select IFNULL(""U_S_BINCODE"",'') from OADM")
                aBinCode = ORec.Fields.Item(0).Value
                If aBinCode = "" Then
                    oApplication.Utilities.Message("BinCode not defined in the Company Setup", SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    Return False
                Else
                    aBinCode = aBinCode + getmaxBarCode("OBCD", "BcdCode")
                    aBarCode = GenerateCheckDidgit(aBinCode)
                End If
                If AddBarCode(aItemCode, aBarCode) = True Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        End Try
    End Function


    Private Function GenerateCheckDidgit(ByVal aNumber As String) As String
        Dim strCheckDigit As String = "0"
        Dim intOdd, intEven, intvalue As Integer
        Dim strOdd, strEven As String
        intOdd = 0
        intEven = 0
        strOdd = ""
        strEven = ""
        For index As Integer = aNumber.Length - 1 To 0 Step -1 ' aNumber.Length - 1
            intvalue = aNumber.Substring(index, 1)
            Select Case index
                Case 1, 3, 5, 7, 9, 11
                    strOdd = strOdd & "," & aNumber.Substring(index, 1)
                    intOdd = intOdd + CInt(aNumber.Substring(index, 1))
                Case 0, 2, 4, 6, 8, 10
                    strEven = strEven & "," & aNumber.Substring(index, 1)
                    intEven = intEven + CInt(aNumber.Substring(index, 1))
            End Select
        Next

        Dim intCheckDigit As Integer = 0
        intCheckDigit = (10 - ((3 * intOdd + intEven) Mod 10)) Mod 10
        strCheckDigit = aNumber + intCheckDigit.ToString
        Return strCheckDigit
    End Function


    Public Function getmaxBarCode(ByVal sTable As String, ByVal sColumn As String) As String
        Dim oRS As SAPbobsCOM.Recordset
        Dim MaxCode As Integer
        Dim sCode As String
        Dim strSQL, aBinCode As String
        Try
            oRS = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("Select IFNULL(""U_S_BINCODE"",'') from OADM")
            aBinCode = oRS.Fields.Item(0).Value
            strSQL = "SELECT IFNULL(MAX(Cast(CAST(subString(""BcdCode"",0,13) AS Varchar) as Numeric)),0) FROM OBCD where ""BcdCode"" like '" & aBinCode & "%'"
            ExecuteSQL(oRS, strSQL)

            If Convert.ToString(oRS.Fields.Item(0).Value).Length > 0 Then
                sCode = oRS.Fields.Item(0).Value ' + 1
                Try
                    sCode = sCode.Substring(6, 6)
                Catch ex As Exception

                End Try

                MaxCode = Convert.ToInt64(sCode) + 1
            Else
                MaxCode = 1
            End If

            sCode = Format(MaxCode, "000000")
            Return sCode
        Catch ex As Exception
            Throw ex
        Finally
            oRS = Nothing
        End Try
    End Function
    Public Function AddBarCode(ByVal aItemCode As String, ByVal aBarCode As String, Optional ByVal aUOMEntry As Integer = 0) As Boolean
        Dim lpCmpSer As SAPbobsCOM.ICompanyService
        Dim lpBCSer As SAPbobsCOM.IBarCodesService
        Dim lpBCPar As SAPbobsCOM.IBarCodeParams
        Dim lpBC As SAPbobsCOM.IBarCode
        Dim lRS As SAPbobsCOM.IRecordset
        Dim lUomEntry As Long, lBcdEntry As Long
        Dim oItem As SAPbobsCOM.Items
        oItem = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems)
        Dim oRec1 As SAPbobsCOM.Recordset
        oRec1 = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Try
            If oItem.GetByKey(aItemCode) Then
                lUomEntry = oItem.DefaultPurchasingUoMEntry
                lRS = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                ' lUomEntry = aUOMEntry
                lpCmpSer = oApplication.Company.GetCompanyService
                lpBCSer = lpCmpSer.GetBusinessService(SAPbobsCOM.ServiceTypes.BarCodesService)
                lpBC = lpBCSer.GetDataInterface(SAPbobsCOM.BarCodesServiceDataInterfaces.bsBarCode)
                lpBC.ItemNo = aItemCode
                lpBC.UoMEntry = lUomEntry
                lpBC.BarCode = aBarCode
                lpBCPar = lpBCSer.Add(lpBC)
                oRec1.DoQuery("Select * from OBCD where ""ItemCode""='" & aItemCode & "' and ""BcdCode""='" & aBarCode & "'")
                If oRec1.RecordCount > 0 Then
                    If oItem.GetByKey(aItemCode) Then
                        oItem.BarCode = aBarCode
                        oItem.Update()
                    End If
                End If
            End If
            '  MsgBox(lpBCPar.AbsEntry)
        Catch ex As Exception
            Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        End Try





        Return True
    End Function

#Region "Update LOC"
    Public Function GetBankBalance(ByVal aCode As String) As Double
        Dim oRec, oTest As SAPbobsCOM.Recordset
        Dim dblTotalLimit, dblUtilizedAmount As Double
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTest = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTest.DoQuery("Select IFNULL(U_CreditLmt,0) 'U_CreditLmt' from DSC1 where BankCode='" & aCode & "'")
        oRec.DoQuery("Select sum(U_CreditAmt) from [@Z_OSCL] where U_SubBank='" & aCode & "' and U_Status='U'")
        dblTotalLimit = 0
        dblUtilizedAmount = 0
        dblTotalLimit = oTest.Fields.Item("U_CreditLmt").Value
        dblUtilizedAmount = oRec.Fields.Item(0).Value
        dblUtilizedAmount = dblTotalLimit - dblUtilizedAmount
        Return dblUtilizedAmount
    End Function

    Public Sub UpdateBankBalance()
        Dim oRec, oTest As SAPbobsCOM.Recordset
        Dim dblTotalLimit, dblUtilizedAmount As Double
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTest = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTest.DoQuery("Select * from DSC1")
        For intRow As Integer = 0 To oTest.RecordCount - 1
            oRec.DoQuery("Select sum(U_CreditAmt) from [@Z_OSCL] where U_SubBank='" & oTest.Fields.Item("BankCode").Value & "'  and U_Status='U'")
            dblTotalLimit = 0
            dblUtilizedAmount = 0
            dblTotalLimit = oTest.Fields.Item("U_CreditLmt").Value
            dblUtilizedAmount = oRec.Fields.Item(0).Value
            dblUtilizedAmount = dblTotalLimit - dblUtilizedAmount
            oRec.DoQuery("Update DSC1 set U_CreditBal='" & dblUtilizedAmount & "' where BankCode='" & oTest.Fields.Item("BankCode").Value & "'")
            oTest.MoveNext()
        Next



    End Sub
#End Region

#Region "Connect to Company"
    Public Sub Connect()
        Dim strCookie As String
        Dim strConnectionContext As String

        Try
            strCookie = oApplication.Company.GetContextCookie
            strConnectionContext = oApplication.SBO_Application.Company.GetConnectionContext(strCookie)

            If oApplication.Company.SetSboLoginContext(strConnectionContext) <> 0 Then
                Throw New Exception("Wrong login credentials.")
            End If

            'Open a connection to company
            If oApplication.Company.Connect() <> 0 Then
                Throw New Exception("Cannot connect to company database. ")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Genral Functions"

#Region "Get MaxCode"
    Public Function getMaxCode(ByVal sTable As String, ByVal sColumn As String) As String
        Dim oRS As SAPbobsCOM.Recordset
        Dim MaxCode As Integer
        Dim sCode As String
        Dim strSQL As String
        Try
            ' strSQL = "SELECT MAX(CAST(" & sColumn & " AS Numeric)) FROM [" & sTable & "]"
            If blnIsHANA = True Then
                strSQL = "SELECT MAX(CAST(""" & sColumn & """ AS Numeric)) FROM """ & sTable & """"
            Else
                strSQL = "SELECT MAX(CAST(" & sColumn & " AS Numeric)) FROM [" & sTable & "]"
            End If

            ExecuteSQL(oRS, strSQL)

            If Convert.ToString(oRS.Fields.Item(0).Value).Length > 0 Then
                MaxCode = oRS.Fields.Item(0).Value + 1
            Else
                MaxCode = 1
            End If

            sCode = Format(MaxCode, "00000000000000")
            Return sCode
        Catch ex As Exception
            Throw ex
        Finally
            oRS = Nothing
        End Try
    End Function
#End Region

#Region "Status Message"
    Public Sub Message(ByVal sMessage As String, ByVal StatusType As SAPbouiCOM.BoStatusBarMessageType)
        oApplication.SBO_Application.StatusBar.SetText(sMessage, SAPbouiCOM.BoMessageTime.bmt_Short, StatusType)
    End Sub
#End Region

#Region "Add Choose from List"
    Public Sub AddChooseFromList(ByVal FormUID As String, ByVal CFL_Text As String, ByVal CFL_Button As String, _
                                        ByVal ObjectType As SAPbouiCOM.BoLinkedObject, _
                                            Optional ByVal AliasName As String = "", Optional ByVal CondVal As String = "", _
                                                    Optional ByVal Operation As SAPbouiCOM.BoConditionOperation = SAPbouiCOM.BoConditionOperation.co_EQUAL)

        Dim oCFLs As SAPbouiCOM.ChooseFromListCollection
        Dim oCons As SAPbouiCOM.Conditions
        Dim oCon As SAPbouiCOM.Condition
        Dim oCFL As SAPbouiCOM.ChooseFromList
        Dim oCFLCreationParams As SAPbouiCOM.ChooseFromListCreationParams
        Try
            oCFLs = oApplication.SBO_Application.Forms.Item(FormUID).ChooseFromLists
            oCFLCreationParams = oApplication.SBO_Application.CreateObject( _
                                    SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)

            ' Adding 2 CFL, one for the button and one for the edit text.
            If ObjectType = SAPbouiCOM.BoLinkedObject.lf_Items Then
                oCFLCreationParams.MultiSelection = True
            Else
                oCFLCreationParams.MultiSelection = False
            End If

            oCFLCreationParams.ObjectType = ObjectType
            oCFLCreationParams.UniqueID = CFL_Text

            oCFL = oCFLs.Add(oCFLCreationParams)

            ' Adding Conditions to CFL1

            oCons = oCFL.GetConditions()

            If Not AliasName = "" Then
                oCon = oCons.Add()
                oCon.Alias = AliasName
                oCon.Operation = Operation
                oCon.CondVal = CondVal
                oCFL.SetConditions(oCons)
            End If

            oCFLCreationParams.UniqueID = CFL_Button
            oCFL = oCFLs.Add(oCFLCreationParams)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Get Linked Object Type"
    Public Function getLinkedObjectType(ByVal Type As SAPbouiCOM.BoLinkedObject) As String
        Return CType(Type, String)
    End Function

#End Region

#Region "Execute Query"
    Public Sub ExecuteSQL(ByRef oRecordSet As SAPbobsCOM.Recordset, ByVal SQL As String)
        Try
            If oRecordSet Is Nothing Then
                oRecordSet = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            End If

            oRecordSet.DoQuery(SQL)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Get Application path"
    Public Function getApplicationPath() As String

        Return Application.StartupPath.Trim

        'Return IO.Directory.GetParent(Application.StartupPath).ToString
    End Function
#End Region

#Region "Date Manipulation"

#Region "Convert SBO Date to System Date"
    '********************************************************************
    'Type		            :   Public Procedure     
    'Name               	:	ConvertStrToDate
    'Parameter          	:   ByVal oDate As String, ByVal strFormat As String
    'Return Value       	:	
    'Author             	:	Manu
    'Created Date       	:	07/12/05
    'Last Modified By	    :	
    'Modified Date        	:	
    'Purpose             	:	To convert Date according to current culture info
    '********************************************************************
    Public Function ConvertStrToDate(ByVal strDate As String, ByVal strFormat As String) As DateTime
        Try
            Dim oDate As DateTime
            Dim ci As New System.Globalization.CultureInfo("en-GB", False)
            Dim newCi As System.Globalization.CultureInfo = CType(ci.Clone(), System.Globalization.CultureInfo)

            System.Threading.Thread.CurrentThread.CurrentCulture = newCi
            oDate = oDate.ParseExact(strDate, strFormat, ci.DateTimeFormat)

            Return oDate
        Catch ex As Exception
            Throw ex
        End Try

    End Function
#End Region

#Region " Get SBO Date Format in String (ddmmyyyy)"
    '********************************************************************
    'Type		            :   Public Procedure     
    'Name               	:	StrSBODateFormat
    'Parameter          	:   none
    'Return Value       	:	
    'Author             	:	Manu
    'Created Date       	:	
    'Last Modified By	    :	
    'Modified Date        	:	
    'Purpose             	:	To get date Format(ddmmyy value) as applicable to SBO
    '********************************************************************
    Public Function StrSBODateFormat() As String
        Try
            Dim rsDate As SAPbobsCOM.Recordset
            Dim strsql As String, GetDateFormat As String
            Dim DateSep As Char

            strsql = "Select DateFormat,DateSep from OADM"
            rsDate = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            rsDate.DoQuery(strsql)
            DateSep = rsDate.Fields.Item(1).Value

            Select Case rsDate.Fields.Item(0).Value
                Case 0
                    GetDateFormat = "dd" & DateSep & "MM" & DateSep & "yy"
                Case 1
                    GetDateFormat = "dd" & DateSep & "MM" & DateSep & "yyyy"
                Case 2
                    GetDateFormat = "MM" & DateSep & "dd" & DateSep & "yy"
                Case 3
                    GetDateFormat = "MM" & DateSep & "dd" & DateSep & "yyyy"
                Case 4
                    GetDateFormat = "yyyy" & DateSep & "dd" & DateSep & "MM"
                Case 5
                    GetDateFormat = "dd" & DateSep & "MMM" & DateSep & "yyyy"
            End Select
            Return GetDateFormat

        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

#Region "Get SBO date Format in Number"
    '********************************************************************
    'Type		            :   Public Procedure     
    'Name               	:	IntSBODateFormat
    'Parameter          	:   none
    'Return Value       	:	
    'Author             	:	Manu
    'Created Date       	:	
    'Last Modified By	    :	
    'Modified Date        	:	
    'Purpose             	:	To get date Format(integer value) as applicable to SBO
    '********************************************************************
    Public Function NumSBODateFormat() As String
        Try
            Dim rsDate As SAPbobsCOM.Recordset
            Dim strsql As String
            Dim DateSep As Char

            strsql = "Select DateFormat,DateSep from OADM"
            rsDate = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            rsDate.DoQuery(strsql)
            DateSep = rsDate.Fields.Item(1).Value

            Select Case rsDate.Fields.Item(0).Value
                Case 0
                    NumSBODateFormat = 3
                Case 1
                    NumSBODateFormat = 103
                Case 2
                    NumSBODateFormat = 1
                Case 3
                    NumSBODateFormat = 120
                Case 4
                    NumSBODateFormat = 126
                Case 5
                    NumSBODateFormat = 130
            End Select
            Return NumSBODateFormat

        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

#End Region

#Region "Get Rental Period"
    Public Function getRentalDays(ByVal Date1 As String, ByVal Date2 As String, ByVal IsWeekDaysBilling As Boolean) As Integer
        Dim TotalDays, TotalDaysincSat, TotalBillableDays As Integer
        Dim TotalWeekEnds As Integer
        Dim StartDate As Date
        Dim EndDate As Date
        Dim oRecordset As SAPbobsCOM.Recordset

        StartDate = CType(Date1.Insert(4, "/").Insert(7, "/"), Date)
        EndDate = CType(Date2.Insert(4, "/").Insert(7, "/"), Date)

        TotalDays = DateDiff(DateInterval.Day, StartDate, EndDate)

        If IsWeekDaysBilling Then
            strSQL = " select dbo.WeekDays('" & Date1 & "','" & Date2 & "')"
            oApplication.Utilities.ExecuteSQL(oRecordset, strSQL)
            If oRecordset.RecordCount > 0 Then
                TotalBillableDays = oRecordset.Fields.Item(0).Value
            End If
            Return TotalBillableDays
        Else
            Return TotalDays + 1
        End If

    End Function

    Public Function WorkDays(ByVal dtBegin As Date, ByVal dtEnd As Date) As Long
        Try
            Dim dtFirstSunday As Date
            Dim dtLastSaturday As Date
            Dim lngWorkDays As Long

            ' get first sunday in range
            dtFirstSunday = dtBegin.AddDays((8 - Weekday(dtBegin)) Mod 7)

            ' get last saturday in range
            dtLastSaturday = dtEnd.AddDays(-(Weekday(dtEnd) Mod 7))

            ' get work days between first sunday and last saturday
            lngWorkDays = (((DateDiff(DateInterval.Day, dtFirstSunday, dtLastSaturday)) + 1) / 7) * 5

            ' if first sunday is not begin date
            If dtFirstSunday <> dtBegin Then

                ' assume first sunday is after begin date
                ' add workdays from begin date to first sunday
                lngWorkDays = lngWorkDays + (7 - Weekday(dtBegin))

            End If

            ' if last saturday is not end date
            If dtLastSaturday <> dtEnd Then

                ' assume last saturday is before end date
                ' add workdays from last saturday to end date
                lngWorkDays = lngWorkDays + (Weekday(dtEnd) - 1)

            End If

            WorkDays = lngWorkDays
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Function

#End Region

#Region "Get Item Price with Factor"
    Public Function getPrcWithFactor(ByVal CardCode As String, ByVal ItemCode As String, ByVal RntlDays As Integer, ByVal Qty As Double) As Double
        Dim oItem As SAPbobsCOM.Items
        Dim Price, Expressn As Double
        Dim oDataSet, oRecSet As SAPbobsCOM.Recordset

        oItem = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems)
        oApplication.Utilities.ExecuteSQL(oDataSet, "Select U_RentFac, U_NumDys From [@REN_FACT] order by U_NumDys ")
        If oItem.GetByKey(ItemCode) And oDataSet.RecordCount > 0 Then

            oApplication.Utilities.ExecuteSQL(oRecSet, "Select ListNum from OCRD where CardCode = '" & CardCode & "'")
            oItem.PriceList.SetCurrentLine(oRecSet.Fields.Item(0).Value - 1)
            Price = oItem.PriceList.Price
            Expressn = 0
            oDataSet.MoveFirst()

            While RntlDays > 0

                If oDataSet.EoF Then
                    oDataSet.MoveLast()
                End If

                If RntlDays < oDataSet.Fields.Item(1).Value Then
                    Expressn += (oDataSet.Fields.Item(0).Value * RntlDays * Price * Qty)
                    RntlDays = 0
                    Exit While
                End If
                Expressn += (oDataSet.Fields.Item(0).Value * oDataSet.Fields.Item(1).Value * Price * Qty)
                RntlDays -= oDataSet.Fields.Item(1).Value
                oDataSet.MoveNext()

            End While

        End If
        If oItem.UserFields.Fields.Item("U_Rental").Value = "Y" Then
            Return CDbl(Expressn / Qty)
        Else
            Return Price
        End If


    End Function
#End Region

#Region "Get WareHouse List"
    Public Function getUsedWareHousesList(ByVal ItemCode As String, ByVal Quantity As Double) As DataTable
        Dim oDataTable As DataTable
        Dim oRow As DataRow
        Dim rswhs As SAPbobsCOM.Recordset
        Dim LeftQty As Double
        Try
            oDataTable = New DataTable
            oDataTable.Columns.Add(New System.Data.DataColumn("ItemCode"))
            oDataTable.Columns.Add(New System.Data.DataColumn("WhsCode"))
            oDataTable.Columns.Add(New System.Data.DataColumn("Quantity"))

            strSQL = "Select WhsCode, ItemCode, (OnHand + OnOrder - IsCommited) As Available From OITW Where ItemCode = '" & ItemCode & "' And " & _
                        "WhsCode Not In (Select Whscode From OWHS Where U_Reserved = 'Y' Or U_Rental = 'Y') Order By (OnHand + OnOrder - IsCommited) Desc "

            ExecuteSQL(rswhs, strSQL)
            LeftQty = Quantity

            While Not rswhs.EoF
                oRow = oDataTable.NewRow()

                oRow.Item("WhsCode") = rswhs.Fields.Item("WhsCode").Value
                oRow.Item("ItemCode") = rswhs.Fields.Item("ItemCode").Value

                LeftQty = LeftQty - CType(rswhs.Fields.Item("Available").Value, Double)

                If LeftQty <= 0 Then
                    oRow.Item("Quantity") = CType(rswhs.Fields.Item("Available").Value, Double) + LeftQty
                    oDataTable.Rows.Add(oRow)
                    Exit While
                Else
                    oRow.Item("Quantity") = CType(rswhs.Fields.Item("Available").Value, Double)
                End If

                oDataTable.Rows.Add(oRow)
                rswhs.MoveNext()
                oRow = Nothing
            End While

            'strSQL = ""
            'For count As Integer = 0 To oDataTable.Rows.Count - 1
            '    strSQL += oDataTable.Rows(count).Item("WhsCode") & " : " & oDataTable.Rows(count).Item("Quantity") & vbNewLine
            'Next
            'MessageBox.Show(strSQL)

            Return oDataTable

        Catch ex As Exception
            Throw ex
        Finally
            oRow = Nothing
        End Try
    End Function
#End Region

#End Region

    Public Function validateAuthorization(ByVal aUserId As String, ByVal aFormUID As String) As Boolean
        Dim oAuth As SAPbobsCOM.Recordset
        oAuth = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Dim struserid As String
        '    Return False
        struserid = oApplication.Company.UserName
        oAuth.DoQuery("select * from UPT1 where ""FormId""='" & aFormUID & "'")
        If (oAuth.RecordCount <= 0) Then
            Return True
        Else
            Dim st As String
            st = oAuth.Fields.Item("PermId").Value
            st = "Select * from USR3 where ""PermId""='" & st & "' and ""UserLink""=" & aUserId
            oAuth.DoQuery(st)
            If oAuth.RecordCount > 0 Then
                If oAuth.Fields.Item("Permission").Value = "N" Then
                    Return False
                End If
                Return True
            Else
                Return True
            End If

        End If

        Return True

    End Function
    Public Sub assignMatrixLineno(ByVal aGrid As SAPbouiCOM.Grid, ByVal aform As SAPbouiCOM.Form)
        aform.Freeze(True)
        Try
            For intNo As Integer = 0 To aGrid.DataTable.Rows.Count - 1
                aGrid.RowHeaders.SetText(intNo, intNo + 1)
            Next
        Catch ex As Exception
        End Try
        aGrid.Columns.Item("RowsHeader").TitleObject.Caption = "#"
        aform.Freeze(False)
    End Sub

#Region "Functions related to Load XML"

#Region "Add/Remove Menus "
    Public Sub AddRemoveMenus(ByVal sFileName As String)
        Dim oXMLDoc As New Xml.XmlDocument
        Dim sFilePath As String
        Try
            sFilePath = getApplicationPath() & "\XML Files\" & sFileName
            oXMLDoc.Load(sFilePath)
            oApplication.SBO_Application.LoadBatchActions(oXMLDoc.InnerXml)
        Catch ex As Exception
            Throw ex
        Finally
            oXMLDoc = Nothing
        End Try
    End Sub
#End Region

#Region "Load XML File "
    Private Function LoadXMLFiles(ByVal sFileName As String) As String
        Dim oXmlDoc As Xml.XmlDocument
        Dim oXNode As Xml.XmlNode
        Dim oAttr As Xml.XmlAttribute
        Dim sPath As String
        Dim FrmUID As String
        Try
            oXmlDoc = New Xml.XmlDocument

            sPath = getApplicationPath() & "\XML Files\" & sFileName

            oXmlDoc.Load(sPath)
            oXNode = oXmlDoc.GetElementsByTagName("form").Item(0)
            oAttr = oXNode.Attributes.GetNamedItem("uid")
            oAttr.Value = oAttr.Value & FormNum
            FormNum = FormNum + 1
            oApplication.SBO_Application.LoadBatchActions(oXmlDoc.InnerXml)
            FrmUID = oAttr.Value

            Return FrmUID

        Catch ex As Exception
            Throw ex
        Finally
            oXmlDoc = Nothing
        End Try
    End Function
#End Region

    Public Function LoadForm(ByVal XMLFile As String, ByVal FormType As String) As SAPbouiCOM.Form
        'Return LoadForm(XMLFile, FormType.ToString(), FormType & "_" & oApplication.SBO_Application.Forms.Count.ToString)
        LoadXMLFiles(XMLFile)
        Return Nothing
    End Function

    '*****************************************************************
    'Type               : Function   
    'Name               : LoadForm
    'Parameter          : XmlFile,FormType,FormUID
    'Return Value       : SBO Form
    'Author             : Senthil Kumar B Senthil Kumar B
    'Created Date       : 
    'Last Modified By   : 
    'Modified Date      : 
    'Purpose            : To Load XML file 
    '*****************************************************************

    Public Function LoadForm(ByVal XMLFile As String, ByVal FormType As String, ByVal FormUID As String) As SAPbouiCOM.Form

        Dim oXML As System.Xml.XmlDocument
        Dim objFormCreationParams As SAPbouiCOM.FormCreationParams
        Try
            oXML = New System.Xml.XmlDocument
            oXML.Load(XMLFile)
            objFormCreationParams = (oApplication.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams))
            objFormCreationParams.XmlData = oXML.InnerXml
            objFormCreationParams.FormType = FormType
            objFormCreationParams.UniqueID = FormUID
            Return oApplication.SBO_Application.Forms.AddEx(objFormCreationParams)
        Catch ex As Exception
            Throw ex

        End Try

    End Function



#Region "Load Forms"
    Public Sub LoadForm(ByRef oObject As Object, ByVal XmlFile As String)
        Try
            oObject.FrmUID = LoadXMLFiles(XmlFile)
            oObject.Form = oApplication.SBO_Application.Forms.Item(oObject.FrmUID)
            If Not oApplication.Collection.ContainsKey(oObject.FrmUID) Then
                oApplication.Collection.Add(oObject.FrmUID, oObject)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Load Combobox"
    Public Sub LoadCombo(ByVal aCombo As SAPbouiCOM.ComboBox, ByVal aQuery As String)
        Dim oRS As SAPbobsCOM.Recordset
        oRS = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        For intRow As Integer = aCombo.ValidValues.Count - 1 To 0 Step -1
            Try
                aCombo.ValidValues.Remove(intRow, SAPbouiCOM.BoSearchKey.psk_Index)
            Catch ex As Exception

            End Try
        Next

        aCombo.ValidValues.Add("", "")
        oRS.DoQuery(aQuery)
        For intRow As Integer = 0 To oRS.RecordCount - 1
            aCombo.ValidValues.Add(oRS.Fields.Item(0).Value, oRS.Fields.Item(1).Value)
            oRS.MoveNext()
        Next
        aCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
    End Sub
#End Region


#End Region
    Public Sub setEdittextvalue(ByVal aform As SAPbouiCOM.Form, ByVal UID As String, ByVal newvalue As String)
        Dim objEdit As SAPbouiCOM.EditText
        objEdit = aform.Items.Item(UID).Specific
        objEdit.String = newvalue
    End Sub
#Region "Functions related to System Initilization"

#Region "Create Tables"
    Public Sub CreateTables()
        Dim oCreateTable As clsTable
        Try
            oCreateTable = New clsTable
            oCreateTable.CreateTables()
        Catch ex As Exception
            Throw ex
        Finally
            oCreateTable = Nothing
        End Try
    End Sub
#End Region

    Public Function CreateJournalEntry(aCompany As SAPbobsCOM.Company, aDocEntry As String) As Boolean
        Dim oOrder As SAPbobsCOM.JournalEntries
        Dim oRec, oTemp As SAPbobsCOM.Recordset
        Dim blnOrderExists, blnLineExists As Boolean
        oRec = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTemp = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        blnOrderExists = False
        blnLineExists = False
      
        Dim strRefdate, strTaxDate, strVatDate, strFormatCode, strJDTNum, strOcrCode1, strOcrCode2, strOcrCode3, strRef3 As String
        Dim dblDebit, dblCredit As Double

        Dim dtPosting, dtTaxDate, dtVatDate As Date

        Message("Creating Journal Entries...", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        'If blnIsHANA = True Then
        '    oTemp.DoQuery("Select ""U_JdtNum"",Count(*) from ""@SJE_OJDT"" T0 Inner Join ""@SJE_JDT1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where IFNULL(""U_JdtNum"",'')<>'' and  IFNULL(""U_JERef"",'')='' and    T1.""U_Status""='O' and T0.""DocEntry""=" & aDocEntry & " group by ""U_JdtNum""")
        'Else
        '    oTemp.DoQuery("Select ""U_JdtNum"",Count(*) from ""@SJE_OJDT"" T0 Inner Join ""@SJE_JDT1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where ISNULL(""U_JdtNum"",'')<>'' and  ISNULL(""U_JERef"",'')='' and    T1.""U_Status""='O' and T0.""DocEntry""=" & aDocEntry & " group by ""U_JdtNum""")
        'End If

        If blnIsHANA = True Then
            oTemp.DoQuery("Select T1.""U_OcrCode1"",Sum(""U_Debit"") from ""@SJE_OJDT"" T0 Inner Join ""@SJE_JDT1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where IFNULL(""U_JdtNum"",'')<>'' and  IFNULL(""U_JERef"",'')='' and    T1.""U_Status""='O' and T0.""DocEntry""=" & aDocEntry & " group by T1.""U_OcrCode1""")
        Else
            oTemp.DoQuery("Select T1.""U_OcrCode1"",Sum(""U_Debit"") from ""@SJE_OJDT"" T0 Inner Join ""@SJE_JDT1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where ISNULL(""U_JdtNum"",'')<>'' and  ISNULL(""U_JERef"",'')='' and    T1.""U_Status""='O' and T0.""DocEntry""=" & aDocEntry & " group by T1.""U_OcrCode1""")
        End If


        For intRow As Integer = 0 To oTemp.RecordCount - 1
            strJDTNum = oTemp.Fields.Item("U_OcrCode1").Value
            'If blnIsHANA = True Then
            '    oRec.DoQuery("Select T0.""DocEntry"",T0.""U_FileName"",T1.""U_SAPAccount"",T1.""U_Debit"",T1.""U_Credit"",T1.""U_Memo"",T1.""U_RefDate"",T1.""U_TaxDate"",T1.""U_VatDate"",T1.""U_OcrCode1"",T1.""U_OcrCode2"",T1.""U_OcrCode3"",T1.""U_SAPOcrCode1"",T1.""U_SAPOcrCode2"",T1.""U_SAPOcrCode3"" from ""@SJE_OJDT"" T0 Inner Join ""@SJE_JDT1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where IFNULL(""U_JdtNum"",'') ='" & strJDTNum & "' and  IFNULL(""U_JERef"",'')='' and    T1.""U_Status""='O' and T0.""DocEntry""=" & aDocEntry)
            'Else
            '    oRec.DoQuery("Select T0.""DocEntry"",T0.""U_FileName"",T1.""U_SAPAccount"",T1.""U_Debit"",T1.""U_Credit"",T1.""U_Memo"",T1.""U_RefDate"",T1.""U_TaxDate"",T1.""U_VatDate"",T1.""U_OcrCode1"",T1.""U_OcrCode2"",T1.""U_OcrCode3"",T1.""U_SAPOcrCode1"",T1.""U_SAPOcrCode2"",T1.""U_SAPOcrCode3"" from ""@SJE_OJDT"" T0 Inner Join ""@SJE_JDT1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where ISNULL(""U_JdtNum"",'')='" & strJDTNum & "' and  ISNULL(""U_JERef"",'')='' and    T1.""U_Status""='O' and T0.""DocEntry""=" & aDocEntry)
            'End If

            If blnIsHANA = True Then
                oRec.DoQuery("Select T0.""DocEntry"",T0.""U_FileName"",T1.""U_SAPAccount"",T1.""U_Debit"",T1.""U_Credit"",T1.""U_Memo"",T1.""U_RefDate"",T1.""U_TaxDate"",T1.""U_VatDate"",T1.""U_OcrCode1"",T1.""U_OcrCode2"",T1.""U_OcrCode3"",T1.""U_SAPOcrCode1"",T1.""U_SAPOcrCode2"",T1.""U_SAPOcrCode3"",T1.""U_Ref3"" from ""@SJE_OJDT"" T0 Inner Join ""@SJE_JDT1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where T1.""U_OcrCode1""='" & strJDTNum & "' and  IFNULL(""U_JdtNum"",'')<>'' and  IFNULL(""U_JERef"",'')='' and    T1.""U_Status""='O' and T0.""DocEntry""=" & aDocEntry & " order by ""U_JdtNum""")
            Else
                oRec.DoQuery("Select T0.""DocEntry"",T0.""U_FileName"",T1.""U_SAPAccount"",T1.""U_Debit"",T1.""U_Credit"",T1.""U_Memo"",T1.""U_RefDate"",T1.""U_TaxDate"",T1.""U_VatDate"",T1.""U_OcrCode1"",T1.""U_OcrCode2"",T1.""U_OcrCode3"",T1.""U_SAPOcrCode1"",T1.""U_SAPOcrCode2"",T1.""U_SAPOcrCode3"",T1.""U_Ref3"" from ""@SJE_OJDT"" T0 Inner Join ""@SJE_JDT1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where T1.""U_OcrCode1""='" & strJDTNum & "' and  ISNULL(""U_JdtNum"",'')<>'' and  ISNULL(""U_JERef"",'')='' and    T1.""U_Status""='O' and T0.""DocEntry""=" & aDocEntry & " order by ""U_JdtNum""")
            End If
            oOrder = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)
            Dim blnLineCount As Integer = 0
            Dim strMemo As String
            For innerLoop As Integer = 0 To oRec.RecordCount - 1
                strRefdate = oRec.Fields.Item("U_RefDate").Value
                strTaxDate = oRec.Fields.Item("U_TaxDate").Value
                strVatDate = oRec.Fields.Item("U_VatDate").Value
                strFormatCode = oRec.Fields.Item("U_SAPAccount").Value
                dblDebit = oRec.Fields.Item("U_Debit").Value
                dblCredit = oRec.Fields.Item("U_Credit").Value
                strOcrCode1 = oRec.Fields.Item("U_SAPOcrCode1").Value
                strOcrCode2 = oRec.Fields.Item("U_SAPOcrCode2").Value
                strOcrCode3 = oRec.Fields.Item("U_SAPOcrCode3").Value
                strRef3 = oRec.Fields.Item("U_Ref3").Value

                strRef3 = strJDTNum
                If strRef3.Length > 26 Then
                    strRef3 = strRef3.Substring(0, 26)
                End If
                ' strPostingDate = strArrivalDate.Substring(0, 10)
                dtPosting = New Date(strRefdate.Substring(0, 4), strRefdate.Substring(4, 2), strRefdate.Substring(6, 2))
                dtTaxDate = New Date(strTaxDate.Substring(0, 4), strTaxDate.Substring(4, 2), strTaxDate.Substring(6, 2))
                dtVatDate = New Date(strVatDate.Substring(0, 4), strVatDate.Substring(4, 2), strVatDate.Substring(6, 2))

                oOrder.ReferenceDate = dtPosting
                oOrder.TaxDate = dtTaxDate
                oOrder.VatDate = dtVatDate
                strMemo = oRec.Fields.Item("U_FileName").Value.ToString
                If strMemo.Length > 29 Then
                    strMemo = strMemo.Substring(0, 29)
                End If
                '               oOrder.Memo = "Based On: " & aDocEntry & " : File Name : " & oRec.Fields.Item("U_FileName").Value.ToString
                oOrder.Memo = strMemo
                oOrder.Reference3 = strRef3



                blnLineExists = True
                If blnLineCount > 0 Then
                    oOrder.Lines.Add()
                End If
                oOrder.Lines.SetCurrentLine(blnLineCount)
                oOrder.Lines.AccountCode = getAccount(strFormatCode)
                If dblDebit > 0 Then
                    oOrder.Lines.Debit = dblDebit
                End If
                If dblCredit > 0 Then
                    oOrder.Lines.Credit = dblCredit
                End If
                If strOcrCode1 <> "" Then
                    oOrder.Lines.CostingCode = strOcrCode1
                End If
                If strOcrCode2 <> "" Then
                    If blnIsHANA = True Then
                        oRec.DoQuery("Select IFNULL(""U_EA_CC2"") from OPRC where ""PrcCode""='" & strOcrCode1 & "'")
                    Else
                        oRec.DoQuery("Select ISNULL(""U_EA_CC2"") from OPRC where ""PrcCode""='" & strOcrCode1 & "'")
                    End If
                    If oRec.Fields.Item(0).Value <> "" Then
                        oOrder.Lines.CostingCode2 = oRec.Fields.Item(0).Value
                    Else
                        oOrder.Lines.CostingCode2 = strOcrCode2
                    End If
                End If
                If strOcrCode3 <> "" Then
                    oOrder.Lines.CostingCode3 = strOcrCode3
                End If
                oOrder.Lines.AdditionalReference = strRef3
                blnLineCount = blnLineCount + 1
                oRec.MoveNext()
            Next
            If blnLineExists = True Then
                If oOrder.Add <> 0 Then
                    oApplication.Utilities.Message(aCompany.GetLastErrorDescription, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    Return False
                Else
                    Dim strDocNum As String
                    aCompany.GetNewObjectCode(strDocNum)
                    ' oRec.DoQuery("Update ""@SJE_JDT1"" set ""U_Status""='C',  ""U_JERef""='" & strDocNum & "' where ""DocEntry""=" & aDocEntry & " and ""U_JdtNum""='" & strJDTNum & "'")
                    oRec.DoQuery("Update ""@SJE_JDT1"" set ""U_Status""='C',  ""U_JERef""='" & strDocNum & "' where ""U_OcrCode1""='" & strJDTNum & "' and  ""DocEntry""=" & aDocEntry & " and ""U_JdtNum""<>''")

                End If
            End If
            oTemp.MoveNext()
        Next
        Return True
    End Function

    Public Function getAccount(aCode As String) As String
        Dim oAct As SAPbobsCOM.Recordset
        oAct = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oAct.DoQuery("Select ""AcctCode"" from OACT where ""FormatCode""='" & aCode & "'")
        Return oAct.Fields.Item(0).Value
    End Function

    Public Function CreateDocuments(aCompany As SAPbobsCOM.Company, aDocEntry As String) As Boolean
        Dim oOrder As SAPbobsCOM.Documents
        Dim oRec, oTemp As SAPbobsCOM.Recordset
        Dim blnOrderExists, blnLineExists As Boolean
        oRec = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTemp = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

        blnOrderExists = False
        blnLineExists = False

        Dim strOrderBPCode, strOrderItem, strPerformerItem, strVehicleItem, strWarehouse As String
        Dim strOrderRef, strArrivalDate, strPostingDate, strShiptoAddress, strNumAtCard As String
        Dim strDocDate As String()
        Dim dtPosting As Date
        Dim dblQty As Double
        strOrderItem = getItemCode("Order")
        strPerformerItem = getItemCode("Performer")
        strVehicleItem = getItemCode("Vehicle")
        Message("Creating Sales Orders...", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        'oRec.DoQuery("Select T1.""DocEntry"" ""DocEntry1"",* from ""@MAX_ORDR"" T0 Inner Join ""@MAX_RDR1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where T1.""U_Capacity"">0 and  T1.""U_Status""='O' and T0.""DocEntry""=" & aDocEntry)
        oRec.DoQuery("Select T1.""DocEntry"" ""DocEntry1"",* from ""@MAX_ORDR"" T0 Inner Join ""@MAX_RDR1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where ""U_OrderRef""<>'' and    T1.""U_Status""='O' and T0.""DocEntry""=" & aDocEntry)
        For intRow As Integer = 0 To oRec.RecordCount - 1
            dblQty = oRec.Fields.Item("U_Capacity").Value
            If dblQty > 0 Then
                strOrderRef = oRec.Fields.Item("U_OrderRef").Value
                strOrderBPCode = oRec.Fields.Item("U_OrderBPCode").Value
                strArrivalDate = oRec.Fields.Item("U_ArrSched").Value
                strWarehouse = oRec.Fields.Item("U_WhsCode").Value
                strShiptoAddress = oRec.Fields.Item("U_ShiptoAddress").Value
                strPostingDate = strArrivalDate.Substring(0, 10)
                strDocDate = strArrivalDate.Split("/") ' strPostingDate.Split("/")
                dtPosting = New Date(strDocDate(2).Substring(0, 4), strDocDate(1), strDocDate(0))

                oOrder = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders)
                oOrder.CardCode = strOrderBPCode
                oOrder.DocDate = dtPosting
                oOrder.DocDueDate = dtPosting
                oOrder.TaxDate = dtPosting
                strNumAtCard = strOrderRef
                oOrder.Comments = "Based on Order Import : " & aDocEntry
                If strShiptoAddress <> "" Then
                    oOrder.ShipToCode = strShiptoAddress
                    oTemp.DoQuery("Select * from CRD1 where ""CardCode""='" & strOrderBPCode & "' and ""AdresType""='S'  and ""Address""='" & strShiptoAddress & "'")
                    If oTemp.RecordCount > 0 Then
                        strNumAtCard = strNumAtCard & "-" & oTemp.Fields.Item("GlblLocNum").Value
                    End If
                End If
                oOrder.NumAtCard = strNumAtCard
                oOrder.UserFields.Fields.Item("U_OrderRef").Value = strOrderRef
                oOrder.PickRemark = oRec.Fields.Item("U_VehicleName").Value
                oOrder.Lines.ItemCode = strOrderItem
                oOrder.Lines.WarehouseCode = strWarehouse
                oOrder.Lines.Quantity = dblQty
                oOrder.Lines.FreeText = oRec.Fields.Item("U_CustLocName").Value
                If oOrder.Add <> 0 Then
                    oApplication.Utilities.Message(aCompany.GetLastErrorDescription, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    Return False
                Else
                    Dim strDocNum As String
                    aCompany.GetNewObjectCode(strDocNum)
                    oTemp.DoQuery("Update ""@MAX_RDR1"" set ""U_Status""='C', ""U_SOrderRefNum""='" & strDocNum & "' where ""DocEntry""=" & oRec.Fields.Item("DocEntry1").Value & " and ""LineId""=" & oRec.Fields.Item("LineId").Value)
                    oTemp.DoQuery("Select * from ORDR where ""DocEntry""=" & strDocNum)
                    If oTemp.RecordCount > 0 Then
                        oTemp.DoQuery("Update ""@MAX_RDR1"" set  ""U_SOrderRefNum""='" & oTemp.Fields.Item("DocNum").Value & "',""U_SOrderRef""='" & oTemp.Fields.Item("DocEntry").Value & "' where ""DocEntry""=" & oRec.Fields.Item("DocEntry1").Value & " and ""LineId""=" & oRec.Fields.Item("LineId").Value)
                    End If
                End If
            Else
                oTemp.DoQuery("Update ""@MAX_RDR1"" set ""U_Status""='C', ""U_SOrderRefNum""='' where ""DocEntry""=" & oRec.Fields.Item("DocEntry1").Value & " and ""LineId""=" & oRec.Fields.Item("LineId").Value)
            End If

            oRec.MoveNext()
        Next
        Return True
    End Function

    Public Function getConsolidatedRef(acompany As SAPbobsCOM.Company, aDocEntry As String) As String
        Dim oRec, oTemp As SAPbobsCOM.Recordset
        Dim blnOrderExists, blnLineExists As Boolean
        oRec = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTemp = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Dim strOrderRefConsolidated As String = ""
        Dim strOrderRef As String
        oRec.DoQuery("Select T1.""DocEntry"" ""DocEntry1"",* from ""@MAX_ORDR"" T0 Inner Join ""@MAX_RDR1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where T1.""U_Capacity"">0 and  T1.""U_Status""='C' and T0.""DocEntry""=" & aDocEntry)
        For intRow As Integer = 0 To oRec.RecordCount - 1
            strOrderRef = oRec.Fields.Item("U_OrderRef").Value
            If strOrderRefConsolidated = "" Then
                strOrderRefConsolidated = strOrderRef
            Else
                strOrderRefConsolidated = strOrderRefConsolidated & "," & strOrderRef
            End If
            oRec.MoveNext()
        Next
        Return strOrderRefConsolidated

    End Function

    Public Function CreateTransportDocuments(aCompany As SAPbobsCOM.Company, aDocEntry As String) As Boolean
        Dim oOrder, oPO, oVehicle As SAPbobsCOM.Documents
        Dim oRec, oTemp As SAPbobsCOM.Recordset
        Dim blnOrderExists, blnLineExists As Boolean
        oRec = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTemp = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        blnOrderExists = False
        blnLineExists = False
        Dim strOrderBPCode, strPerformerBPCode, strVehicleBPCode, strOrderItem, strPerformerItem, strVehicleItem, strWarehouse As String
        Dim strOrderRef, strArrivalDate, strPostingDate As String
        Dim strDocDate As String()
        Dim dtPosting As Date
        Dim dblQty As Double
        strOrderItem = getItemCode("Order")
        strPerformerItem = getItemCode("Performer")
        strVehicleItem = getItemCode("Vehicle")
        Message("Creating Transport Orders...", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        If blnIsHANA = True Then
            oRec.DoQuery("Select * from ""@MAX_ORDR"" T0 Inner Join ""@MAX_RDR1"" T1 on T1.""DocEntry""=T0.""DocEntry""  where  IFNULL(""U_OrderRef"",'')<>'' and IFNULL(""U_VehicleRef"",'')='' and  T0.""U_Status""='O' and  T0.""DocEntry""=" & aDocEntry & " Order by ""U_Sequence""")
        Else
            oRec.DoQuery("Select * from ""@MAX_ORDR"" T0 Inner Join ""@MAX_RDR1"" T1 on T1.""DocEntry""=T0.""DocEntry""  where ISNULL(""U_OrderRef"",'')<>'' and   ISNULL(""U_VehicleRef"",'')='' and  T0.""U_Status""='O' and  T0.""DocEntry""=" & aDocEntry & " Order by ""U_Sequence""")
        End If
        Dim strScheduledArrival, strScheduledDepature As String
        Dim intLineCount As Integer = 0
        If oRec.RecordCount > 0 Then
            strArrivalDate = oRec.Fields.Item("U_ArrSched").Value
            strScheduledArrival = oRec.Fields.Item("U_ArrSched").Value ' oRec.Fields.Item("U_ScArTime").Value
            oOrder = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders)
            Dim strOrderRefConsolidated As String = ""
            Dim strDocEntry1 As String
            'oRec.DoQuery("Select T1.""DocEntry"" ""DocEntry1"",* from ""@MAX_ORDR"" T0 Inner Join ""@MAX_RDR1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where T1.""U_Capacity"">0 and  T1.""U_Status""='C' and T0.""DocEntry""=" & aDocEntry & " Order by ""U_Sequence""")
            If blnIsHANA = True Then
                oRec.DoQuery("Select T1.""DocEntry"" ""DocEntry1"",* from ""@MAX_ORDR"" T0 Inner Join ""@MAX_RDR1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where   IFNULL(""U_OrderRef"",'')<>'' and IFNULL(""U_VehicleRef"",'')='' and  T1.""U_Status""='C' and T0.""DocEntry""=" & aDocEntry & " Order by ""U_Sequence""")
            Else
                oRec.DoQuery("Select T1.""DocEntry"" ""DocEntry1"",* from ""@MAX_ORDR"" T0 Inner Join ""@MAX_RDR1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where   ISNULL(""U_OrderRef"",'')<>'' and ISNULL(""U_VehicleRef"",'')='' and  T1.""U_Status""='C' and T0.""DocEntry""=" & aDocEntry & " Order by ""U_Sequence""")
            End If
            For intRow As Integer = 0 To oRec.RecordCount - 1
                strScheduledDepature = oRec.Fields.Item("U_DepSched").Value ' oRec.Fields.Item("U_ScDepTime").Value
                strDocEntry1 = oRec.Fields.Item("DocEntry1").Value
                strOrderRef = oRec.Fields.Item("U_OrderRef").Value
                If strOrderRefConsolidated = "" Then
                    strOrderRefConsolidated = strOrderRef
                Else
                    strOrderRefConsolidated = strOrderRefConsolidated & "," & strOrderRef
                End If
                strOrderBPCode = oRec.Fields.Item("U_VehicleBPCode").Value
                strWarehouse = oRec.Fields.Item("U_WhsCode").Value
                strPostingDate = strArrivalDate.Substring(0, 10)
                strDocDate = strArrivalDate.Split("/") ' strPostingDate.Split("/")
                dtPosting = New Date(strDocDate(2).Substring(0, 4), strDocDate(1), strDocDate(0))
                dblQty = oRec.Fields.Item("U_ScMillage").Value
                oOrder.CardCode = strOrderBPCode
                oOrder.DocDate = dtPosting
                oOrder.DocDueDate = dtPosting
                oOrder.TaxDate = dtPosting
                oOrder.Comments = "Based on Order Import : " & aDocEntry
                'ssss oOrder.NumAtCard = strOrderRef
                Dim strIndicator As String
                oTemp.DoQuery("Select * from OIDC where ""Code""='TR'")
                If oTemp.RecordCount > 0 Then
                    oOrder.Indicator = "TR"
                End If
                blnLineExists = True
                If intRow > 0 Then '
                    oOrder.Lines.Add()
                End If
                oOrder.Lines.SetCurrentLine(intRow)
                oOrder.PickRemark = oRec.Fields.Item("U_VehicleName").Value
                oOrder.Lines.ItemCode = strVehicleItem
                oOrder.Lines.WarehouseCode = strWarehouse
                oOrder.Lines.Quantity = dblQty
                oOrder.Lines.FreeText = strOrderRef & "," & oRec.Fields.Item("U_CustLocName").Value & "," & oRec.Fields.Item("U_CustLocAdd").Value
                intLineCount = intLineCount + 1
                oRec.MoveNext()
            Next
            If blnLineExists = True Then
                Dim dtScheduledArrival, dtScheduledDepature As Date
                If 1 = 1 Then 'strScheduledArrival.Length >= 10 And strScheduledDepature.Length >= 10 Then
                    ' strScheduledArrival = strScheduledArrival.Substring(0, 10)
                    ' strDocDate = strScheduledArrival.Split("-")
                    strScheduledArrival = strScheduledArrival.Substring(0, 10)
                    strDocDate = strScheduledArrival.Split("/") ' strPostingDate.Split("/")
                    dtScheduledArrival = New Date(strDocDate(2).Substring(0, 4), strDocDate(1), strDocDate(0))


                    '  dtScheduledArrival = New Date(strDocDate(0), strDocDate(1), strDocDate(2))
                    ' dtScheduledArrival = New Date(strDocDate(2), strDocDate(1), strDocDate(0))
                    '  strScheduledDepature = strScheduledDepature.Substring(0, 10)


                    ' strDocDate = strScheduledDepature.Split("-")
                    '  dtScheduledDepature = New Date(strDocDate(0), strDocDate(1), strDocDate(2))

                    strScheduledDepature = strScheduledDepature.Substring(0, 10)
                    strDocDate = strScheduledDepature.Split("/") ' strPostingDate.Split("/")
                    dtScheduledDepature = New Date(strDocDate(2).Substring(0, 4), strDocDate(1), strDocDate(0))


                    ' dtScheduledDepature = New Date(strDocDate(2), strDocDate(1), strDocDate(0))
                    Dim strDayItem As String = getItemCode("Day")
                    Dim strOverNight As String = getItemCode("OverNight")
                    Dim intDay As Integer = DateDiff(DateInterval.Day, dtScheduledArrival, dtScheduledDepature)
                    If intDay > 0 Then
                        oOrder.Lines.Add()
                        oOrder.Lines.SetCurrentLine(intLineCount)
                        oOrder.Lines.ItemCode = strDayItem
                        oOrder.Lines.WarehouseCode = strWarehouse
                        oOrder.Lines.Quantity = intDay + 1
                        blnLineExists = True
                        If intDay > 0 Then
                            oOrder.Lines.Add()
                            oOrder.Lines.SetCurrentLine(intLineCount + 1)
                            oOrder.Lines.ItemCode = strOverNight
                            oOrder.Lines.WarehouseCode = strWarehouse
                            oOrder.Lines.Quantity = intDay
                            blnLineExists = True
                        End If
                    End If

                    oOrder.UserFields.Fields.Item("U_OrderRefs").Value = strOrderRefConsolidated
                    If blnLineExists = True Then
                        If oOrder.Add <> 0 Then
                            oApplication.Utilities.Message(aCompany.GetLastErrorDescription, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                            Return False
                        Else
                            Dim strDocNum As String
                            aCompany.GetNewObjectCode(strDocNum)
                            'oTemp.DoQuery("Update ""@MAX_RDR1"" set ""U_Status""='C'  where ""DocEntry""=" & oRec.Fields.Item("DocEntry1").Value & " and ""LineId""=" & oRec.Fields.Item("LineId").Value)
                            oTemp.DoQuery("Select * from OPOR where ""DocEntry""=" & strDocNum)
                            If oTemp.RecordCount > 0 Then
                                oTemp.DoQuery("Update ""@MAX_ORDR"" set ""U_VehicleRef""='" & oTemp.Fields.Item("DocEntry").Value & "', ""U_VehicleRefNum""='" & oTemp.Fields.Item("DocNum").Value & "' where ""DocEntry""=" & strDocEntry1)
                            End If
                        End If
                    End If
                End If
            End If
        End If
        Return True
    End Function

    Public Function CreateManufactureOrder(aCompany As SAPbobsCOM.Company, aDocEntry As String) As Boolean
        Dim oOrder, oPO, oVehicle As SAPbobsCOM.Documents
        Dim oRec, oTemp As SAPbobsCOM.Recordset
        Dim blnOrderExists, blnLineExists As Boolean
        oRec = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTemp = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        blnOrderExists = False
        blnLineExists = False
        Dim strOrderBPCode, strPerformerBPCode, strVehicleBPCode, strOrderItem, strPerformerItem, strVehicleItem, strWarehouse As String
        Dim strOrderRef, strArrivalDate, strPostingDate As String
        Dim strDocDate As String()
        Dim dtPosting As Date
        Dim dblQty As Double
        strOrderItem = getItemCode("Order")
        strPerformerItem = getItemCode("Performer")
        strVehicleItem = getItemCode("Vehicle")
        Message("Creating Manufacture Orders...", SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        If blnIsHANA = True Then
            oRec.DoQuery("Select * from ""@MAX_ORDR"" T0  Inner Join ""@MAX_RDR1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where IFNULL(""U_OrderRef"",'')<>'' and  IFNULL(T0.""U_ManuFatRef"",'')='' and T0.""U_Status""='O' and  T0.""DocEntry""=" & aDocEntry)
        Else
            oRec.DoQuery("Select * from ""@MAX_ORDR"" T0 Inner Join ""@MAX_RDR1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where ISNULL(""U_OrderRef"",'')<>'' and  ISNULL(T0.""U_ManuFatRef"",'')='' and T0.""U_Status""='O' and  T0.""DocEntry""=" & aDocEntry)
        End If
        If oRec.RecordCount > 0 Then
            strArrivalDate = oRec.Fields.Item("U_ArrSched").Value

            Dim s As String = "Select Sum(T1.""U_Capacity"") ""U_Capacity"",T1.""U_PerformerBPCode"",T1.""U_WhsCode"" from ""@MAX_ORDR"" T0 Inner Join ""@MAX_RDR1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where T1.""U_Capacity"">0 and  T1.""U_Status""='C' and T0.""DocEntry""=" & aDocEntry & " group  by T1.""U_PerformerBPCode"",T1.""U_WhsCode"""

            If blnIsHANA = True Then
                oRec.DoQuery("Select Sum(T1.""U_Capacity"") ""U_Capacity"",T1.""U_PerformerBPCode"",T1.""U_WhsCode"" from ""@MAX_ORDR"" T0 Inner Join ""@MAX_RDR1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where IFNULL(""U_OrderRef"",'')<>'' and  IFNULL(T0.""U_ManuFatRef"",'')='' and T1.""U_Capacity"">0 and  T1.""U_Status""='C' and T0.""DocEntry""=" & aDocEntry & " group by  T1.""U_PerformerBPCode"",T1.""U_WhsCode""")
            Else
                oRec.DoQuery("Select Sum(T1.""U_Capacity"") ""U_Capacity"",T1.""U_PerformerBPCode"",T1.""U_WhsCode"" from ""@MAX_ORDR"" T0 Inner Join ""@MAX_RDR1"" T1 on T1.""DocEntry""=T0.""DocEntry"" where ISNULL(""U_OrderRef"",'')<>'' and  ISNULL(T0.""U_ManuFatRef"",'')='' and T1.""U_Capacity"">0 and  T1.""U_Status""='C' and T0.""DocEntry""=" & aDocEntry & " group by  T1.""U_PerformerBPCode"",T1.""U_WhsCode""")
            End If

            For intRow As Integer = 0 To oRec.RecordCount - 1
                strOrderBPCode = oRec.Fields.Item("U_PerformerBPCode").Value
                strWarehouse = oRec.Fields.Item("U_WhsCode").Value
                strPostingDate = strArrivalDate.Substring(0, 10)
                strDocDate = strArrivalDate.Split("/") ' strPostingDate.Split("/")
                dtPosting = New Date(strDocDate(2).Substring(0, 4), strDocDate(1), strDocDate(0))
                dblQty = oRec.Fields.Item("U_Capacity").Value
                oOrder = aCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders)
                oOrder.CardCode = strOrderBPCode
                oOrder.DocDate = dtPosting
                oOrder.DocDueDate = dtPosting
                oOrder.TaxDate = dtPosting
                oOrder.NumAtCard = "" ' strOrderRef
                oOrder.UserFields.Fields.Item("U_OrderRefs").Value = getConsolidatedRef(aCompany, aDocEntry)
                oOrder.Comments = "Based on Order Import : " & aDocEntry

                'oOrder.PickRemark = oRec.Fields.Item("U_VehicleName").Value
                oOrder.Lines.ItemCode = strPerformerItem
                oOrder.Lines.WarehouseCode = strWarehouse
                oOrder.Lines.Quantity = dblQty
                ' oOrder.Lines.FreeText = oRec.Fields.Item("U_CustLocName").Value
                If oOrder.Add <> 0 Then
                    oApplication.Utilities.Message(aCompany.GetLastErrorDescription, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    Return False
                Else
                    Dim strDocNum As String
                    aCompany.GetNewObjectCode(strDocNum)
                    'oTemp.DoQuery("Update ""@MAX_RDR1"" set ""U_Status""='C' where  ""U_PerformerBPCode""='" & oRec.Fields.Item("U_PerformerBPCode").Value & "' and ""U_WhsCode""='" & oRec.Fields.Item("U_WhsCode").Value & "' and  ""DocEntry""=" & oRec.Fields.Item("DocEntry1").Value & " and ""LineId""=" & oRec.Fields.Item("LineId").Value)
                    oTemp.DoQuery("Select * from OPOR where ""DocEntry""=" & strDocNum)
                    If oTemp.RecordCount > 0 Then
                        oTemp.DoQuery("Update ""@MAX_ORDR"" set ""U_ManuFatRef""='" & oTemp.Fields.Item("DocEntry").Value & "', ""U_ManuFatRefNum""='" & oTemp.Fields.Item("DocNum").Value & "' where ""DocEntry""=" & aDocEntry)
                    End If
                End If
                oRec.MoveNext()
            Next
        End If
        Return True
    End Function

    Public Function getItemCode(aChoice As String)
        Try
            Dim StrSql As String
            Dim ObjOCRD As SAPbobsCOM.Recordset
            ObjOCRD = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Select Case aChoice
                Case "Order"
                    StrSql = "Select ""U_OrderItem"" from ""@MAX_SETUP"""
                Case "Vehicle"
                    StrSql = "Select ""U_VehicleItem"" from ""@MAX_SETUP"""

                Case "Performer"
                    StrSql = "Select ""U_ManufactureItem"" from ""@MAX_SETUP"""
                Case "Day"
                    StrSql = "Select ""U_Day"" from ""@MAX_SETUP"""
                Case "OverNight"
                    StrSql = "Select ""U_OverNight"" from ""@MAX_SETUP"""
                Case "Warehouse"
                    ' StrSql = "Select ""U_WhsCode"" from ""@MAX_WHSE_NAME"" where ""Name""='" & UDFvalue.Replace("'", "''") & "'"
            End Select

            ObjOCRD.DoQuery(StrSql)
            If ObjOCRD.EoF = False Then
                Return ObjOCRD.Fields.Item(0).Value
            Else
                Return ""
            End If
        Catch ex As Exception
            oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Function

    Public Function getDocumentQuantity(ByVal strQuantity As String) As Double
        Dim dblQuant As Double
        Dim strTemp, strTemp1 As String
        Dim oRec As SAPbobsCOM.Recordset
        oRec = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        If strQuantity = "" Then
            Return 0
        End If
        oRec.DoQuery("Select ""CurrCode""  from OCRN")
        For intRow As Integer = 0 To oRec.RecordCount - 1
            strQuantity = strQuantity.Replace(oRec.Fields.Item(0).Value, "")
            oRec.MoveNext()
        Next
        strTemp1 = strQuantity
        strTemp = CompanyDecimalSeprator
        If CompanyDecimalSeprator <> "." Then
            If CompanyThousandSeprator <> strTemp Then
            End If
            strQuantity = strQuantity.Replace(".", ",")
        End If
        If strQuantity = "" Then
            Return 0
        End If
        Try
            dblQuant = Convert.ToDouble(strQuantity)
        Catch ex As Exception
            dblQuant = Convert.ToDouble(strTemp1)
        End Try

        Return dblQuant
    End Function
    Public Function getListedPrice(aCardCode As String, aItemCode As String) As Double
        Dim oRc As SAPbobsCOM.Recordset
        Dim strPln As String
        oRc = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRc.DoQuery("Select * from OCRD where ""CardCode""='" & aCardCode & "'")
        strPln = oRc.Fields.Item("ListNum").Value
        If blnIsHANA = True Then
            oRc.DoQuery("Select ""Price"" from ITM1 where  ""PriceList""=" & strPln & " and ""ItemCode""='" & aItemCode & "'")
        Else
            oRc.DoQuery("Select ""Price"" from ITM1 where  ""PriceList""=" & strPln & " and ""ItemCode""='" & aItemCode & "'")

        End If
        Return oRc.Fields.Item(0).Value
    End Function
    Public Function checkFOC(aItemCode As String) As String
        Dim oRc As SAPbobsCOM.Recordset
        Dim strPln As String
        oRc = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        If blnIsHANA = True Then
            oRc.DoQuery("Select IFNULL(""U_FOC"",'N') from OITM where ""ItemCode""='" & aItemCode & "'")
        Else
            oRc.DoQuery("Select ISNULL(""U_FOC"",'N') from OITM where ""ItemCode""='" & aItemCode & "'")

        End If
        If oRc.Fields.Item(0).Value = "Y" Then
            Return "Y"
        Else
            Return "N"
        End If
    End Function
    Public Function getOCrCode(aWhsCode As String) As String
        Dim oRc As SAPbobsCOM.Recordset
        oRc = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        If blnIsHANA = True Then
            oRc.DoQuery("Select IFNULL(""U_OcrCode"",'') from OWHS where ""WhsCode""='" & aWhsCode & "'")
        Else
            oRc.DoQuery("Select ISNULL(""U_OcrCode"",'') from OWHS where ""WhsCode""='" & aWhsCode & "'")
        End If
        Return oRc.Fields.Item(0).Value

    End Function

    Public Function getCashAccount(aCardCode As String) As String
        Dim oRc As SAPbobsCOM.Recordset
        oRc = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        If blnIsHANA = True Then
            oRc.DoQuery("SELECT T2.""BankCode"", T2.""Account"", T2.""GLAccount"" FROM  ""OCRD""  T0 INNER JOIN  ""ODSC""  T1 ON T0.""HouseBank"" =T1.""BankCode"" INNER JOIN DSC1 T2 ON T0.""HousActKey"" = T2.""AbsEntry"" WHERE T0.""CardCode"" ='" & aCardCode & "'")
        Else
            oRc.DoQuery("SELECT T2.""BankCode"", T2.""Account"", T2.""GLAccount"" FROM  ""OCRD""  T0 INNER JOIN  ""ODSC""  T1 ON T0.""HouseBank"" =T1.""BankCode"" INNER JOIN DSC1 T2 ON T0.""HousActKey"" = T2.""AbsEntry"" WHERE T0.""CardCode"" ='" & aCardCode & "'")
        End If
        Return oRc.Fields.Item(2).Value

    End Function

#Region "Notify Alert"
    Public Sub NotifyAlert()
        'Dim oAlert As clsPromptAlert

        'Try
        '    oAlert = New clsPromptAlert
        '    oAlert.AlertforEndingOrdr()
        'Catch ex As Exception
        '    Throw ex
        'Finally
        '    oAlert = Nothing
        'End Try
    End Sub
#End Region

#End Region

#Region "Function related to Quantities"

#Region "Get Available Quantity"
    Public Function getAvailableQty(ByVal ItemCode As String) As Long
        Dim rsQuantity As SAPbobsCOM.Recordset

        strSQL = "Select SUM(T1.OnHand + T1.OnOrder - T1.IsCommited) From OITW T1 Left Outer Join OWHS T3 On T3.Whscode = T1.WhsCode " & _
                    "Where T1.ItemCode = '" & ItemCode & "'"
        Me.ExecuteSQL(rsQuantity, strSQL)

        If rsQuantity.Fields.Item(0) Is System.DBNull.Value Then
            Return 0
        Else
            Return CLng(rsQuantity.Fields.Item(0).Value)
        End If

    End Function
#End Region

#Region "Get Rented Quantity"
    Public Function getRentedQty(ByVal ItemCode As String, ByVal StartDate As String, ByVal EndDate As String) As Long
        Dim rsQuantity As SAPbobsCOM.Recordset
        Dim RentedQty As Long

        strSQL = " select Sum(U_ReqdQty) from [@REN_RDR1] Where U_ItemCode = '" & ItemCode & "' " & _
                    " And DocEntry IN " & _
                    " (Select DocEntry from [@REN_ORDR] Where U_Status = 'R') " & _
                    " and '" & StartDate & "' between [@REN_RDR1].U_ShipDt1 and [@REN_RDR1].U_ShipDt2 "
        '" and [@REN_RDR1].U_ShipDt1 between '" & StartDate & "' and '" & EndDate & "'"

        ExecuteSQL(rsQuantity, strSQL)
        If Not rsQuantity.Fields.Item(0).Value Is System.DBNull.Value Then
            RentedQty = rsQuantity.Fields.Item(0).Value
        End If

        Return RentedQty

    End Function
#End Region

#Region "Get Reserved Quantity"
    Public Function getReservedQty(ByVal ItemCode As String, ByVal StartDate As String, ByVal EndDate As String) As Long
        Dim rsQuantity As SAPbobsCOM.Recordset
        Dim ReservedQty As Long

        strSQL = " select Sum(U_ReqdQty) from [@REN_QUT1] Where U_ItemCode = '" & ItemCode & "' " & _
                    " And DocEntry IN " & _
                    " (Select DocEntry from [@REN_OQUT] Where U_Status = 'R' And Status = 'O') " & _
                    " and '" & StartDate & "' between [@REN_QUT1].U_ShipDt1 and [@REN_QUT1].U_ShipDt2"

        ExecuteSQL(rsQuantity, strSQL)
        If Not rsQuantity.Fields.Item(0).Value Is System.DBNull.Value Then
            ReservedQty = rsQuantity.Fields.Item(0).Value
        End If

        Return ReservedQty

    End Function
#End Region

#End Region

#Region "Functions related to Tax"

#Region "Get Tax Codes"
    Public Sub getTaxCodes(ByRef oCombo As SAPbouiCOM.ComboBox)
        Dim rsTaxCodes As SAPbobsCOM.Recordset

        strSQL = "Select Code, Name From OVTG Where Category = 'O' Order By Name"
        Me.ExecuteSQL(rsTaxCodes, strSQL)

        oCombo.ValidValues.Add("", "")
        If rsTaxCodes.RecordCount > 0 Then
            While Not rsTaxCodes.EoF
                oCombo.ValidValues.Add(rsTaxCodes.Fields.Item(0).Value, rsTaxCodes.Fields.Item(1).Value)
                rsTaxCodes.MoveNext()
            End While
        End If
        oCombo.ValidValues.Add("Define New", "Define New")
        'oCombo.Select("")
    End Sub
#End Region

#Region "Get Applicable Code"

    Public Function getApplicableTaxCode1(ByVal CardCode As String, ByVal ItemCode As String, ByVal Shipto As String) As String
        Dim oBP As SAPbobsCOM.BusinessPartners
        Dim oItem As SAPbobsCOM.Items
        Dim rsExempt As SAPbobsCOM.Recordset
        Dim TaxGroup As String
        oBP = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)

        If oBP.GetByKey(CardCode.Trim) Then
            If oBP.VatLiable = SAPbobsCOM.BoVatStatus.vLiable Or oBP.VatLiable = SAPbobsCOM.BoVatStatus.vEC Then
                If oBP.VatGroup.Trim <> "" Then
                    TaxGroup = oBP.VatGroup.Trim
                Else
                    strSQL = "select LicTradNum from CRD1 where Address ='" & Shipto & "' and CardCode ='" & CardCode & "'"
                    Me.ExecuteSQL(rsExempt, strSQL)
                    If rsExempt.RecordCount > 0 Then
                        rsExempt.MoveFirst()
                        TaxGroup = rsExempt.Fields.Item(0).Value
                    Else
                        TaxGroup = ""
                    End If
                    'TaxGroup = oBP.FederalTaxID
                End If
            ElseIf oBP.VatLiable = SAPbobsCOM.BoVatStatus.vExempted Then
                strSQL = "Select Code From OVTG Where Rate = 0 And Category = 'O' Order By Code"
                Me.ExecuteSQL(rsExempt, strSQL)
                If rsExempt.RecordCount > 0 Then
                    rsExempt.MoveFirst()
                    TaxGroup = rsExempt.Fields.Item(0).Value
                Else
                    TaxGroup = ""
                End If
            End If
        End If




        Return TaxGroup

    End Function


    Public Function getApplicableTaxCode(ByVal CardCode As String, ByVal ItemCode As String) As String
        Dim oBP As SAPbobsCOM.BusinessPartners
        Dim oItem As SAPbobsCOM.Items
        Dim rsExempt As SAPbobsCOM.Recordset
        Dim TaxGroup As String
        oBP = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)

        If oBP.GetByKey(CardCode.Trim) Then
            If oBP.VatLiable = SAPbobsCOM.BoVatStatus.vLiable Or oBP.VatLiable = SAPbobsCOM.BoVatStatus.vEC Then
                If oBP.VatGroup.Trim <> "" Then
                    TaxGroup = oBP.VatGroup.Trim
                Else
                    TaxGroup = oBP.FederalTaxID
                End If
            ElseIf oBP.VatLiable = SAPbobsCOM.BoVatStatus.vExempted Then
                strSQL = "Select Code From OVTG Where Rate = 0 And Category = 'O' Order By Code"
                Me.ExecuteSQL(rsExempt, strSQL)
                If rsExempt.RecordCount > 0 Then
                    rsExempt.MoveFirst()
                    TaxGroup = rsExempt.Fields.Item(0).Value
                Else
                    TaxGroup = ""
                End If
            End If
        End If

        'If oBP.GetByKey(CardCode.Trim) Then
        '    If oBP.VatLiable = SAPbobsCOM.BoVatStatus.vLiable Or oBP.VatLiable = SAPbobsCOM.BoVatStatus.vEC Then
        '        If oBP.VatGroup.Trim <> "" Then
        '            TaxGroup = oBP.VatGroup.Trim
        '        Else
        '            oItem = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems)
        '            If oItem.GetByKey(ItemCode.Trim) Then
        '                TaxGroup = oItem.SalesVATGroup.Trim
        '            End If
        '        End If
        '    ElseIf oBP.VatLiable = SAPbobsCOM.BoVatStatus.vExempted Then
        '        strSQL = "Select Code From OVTG Where Rate = 0 And Category = 'O' Order By Code"
        '        Me.ExecuteSQL(rsExempt, strSQL)
        '        If rsExempt.RecordCount > 0 Then
        '            rsExempt.MoveFirst()
        '            TaxGroup = rsExempt.Fields.Item(0).Value
        '        Else
        '            TaxGroup = ""
        '        End If
        '    End If
        'End If
        Return TaxGroup

    End Function
#End Region

#End Region

#Region "Log Transaction"
    Public Sub LogTransaction(ByVal DocNum As Integer, ByVal ItemCode As String, _
                                    ByVal FromWhs As String, ByVal TransferedQty As Double, ByVal ProcessDate As Date)
        Dim sCode As String
        Dim sColumns As String
        Dim sValues As String
        Dim rsInsert As SAPbobsCOM.Recordset

        sCode = Me.getMaxCode("@REN_PORDR", "Code")

        sColumns = "Code, Name, U_DocNum, U_WhsCode, U_ItemCode, U_Quantity, U_RetQty, U_Date"
        sValues = "'" & sCode & "','" & sCode & "'," & DocNum & ",'" & FromWhs & "','" & ItemCode & "'," & TransferedQty & ", 0, Convert(DateTime,'" & ProcessDate.ToString("yyyyMMdd") & "')"

        strSQL = "Insert into [@REN_PORDR] (" & sColumns & ") Values (" & sValues & ")"
        oApplication.Utilities.ExecuteSQL(rsInsert, strSQL)

    End Sub

    Public Sub LogCreatedDocument(ByVal DocNum As Integer, ByVal CreatedDocType As SAPbouiCOM.BoLinkedObject, ByVal CreatedDocNum As String, ByVal sCreatedDate As String)
        Dim oUserTable As SAPbobsCOM.UserTable
        Dim sCode As String
        Dim CreatedDate As DateTime
        Try
            oUserTable = oApplication.Company.UserTables.Item("REN_DORDR")

            sCode = Me.getMaxCode("@REN_DORDR", "Code")

            If Not oUserTable.GetByKey(sCode) Then
                oUserTable.Code = sCode
                oUserTable.Name = sCode

                With oUserTable.UserFields.Fields
                    .Item("U_DocNum").Value = DocNum
                    .Item("U_DocType").Value = CInt(CreatedDocType)
                    .Item("U_DocEntry").Value = CInt(CreatedDocNum)

                    If sCreatedDate <> "" Then
                        CreatedDate = CDate(sCreatedDate.Insert(4, "/").Insert(7, "/"))
                        .Item("U_Date").Value = CreatedDate
                    Else
                        .Item("U_Date").Value = CDate(Format(Now, "Long Date"))
                    End If

                End With

                If oUserTable.Add <> 0 Then
                    Throw New Exception(oApplication.Company.GetLastErrorDescription)
                End If
            End If

        Catch ex As Exception
            Throw ex
        Finally
            oUserTable = Nothing
        End Try
    End Sub
#End Region

    Public Function getLocalCurrency(ByVal strCurrency As String) As Double
        Dim oTemp As SAPbobsCOM.Recordset
        oTemp = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTemp.DoQuery("Select Maincurrncy from OADM")
        Return oTemp.Fields.Item(0).Value
    End Function

#Region "Get ExchangeRate"
    Public Function getExchangeRate(ByVal strCurrency As String) As Double
        Dim oTemp As SAPbobsCOM.Recordset
        Dim dblExchange As Double
        If GetCurrency("Local") = strCurrency Then
            dblExchange = 1
        Else
            oTemp = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oTemp.DoQuery("Select IFNULL(Rate,0) from ORTT where convert(nvarchar(10),RateDate,101)=Convert(nvarchar(10),getdate(),101) and currency='" & strCurrency & "'")
            dblExchange = oTemp.Fields.Item(0).Value
        End If
        Return dblExchange
    End Function

    Public Function getExchangeRate(ByVal strCurrency As String, ByVal dtdate As Date) As Double
        Dim oTemp As SAPbobsCOM.Recordset
        Dim strSql As String
        Dim dblExchange As Double
        If GetCurrency("Local") = strCurrency Then
            dblExchange = 1
        Else
            oTemp = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strSql = "Select IFNULL(Rate,0) from ORTT where ratedate='" & dtdate.ToString("yyyy-MM-dd") & "' and currency='" & strCurrency & "'"
            oTemp.DoQuery(strSql)
            dblExchange = oTemp.Fields.Item(0).Value
        End If
        Return dblExchange
    End Function
#End Region

    Public Function GetDateTimeValue(ByVal DateString As String) As DateTime
        Dim objBridge As SAPbobsCOM.SBObob
        objBridge = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge)
        Return objBridge.Format_StringToDate(DateString).Fields.Item(0).Value
    End Function

#Region "Get DocCurrency"
    Public Function GetDocCurrency(ByVal aDocEntry As Integer) As String
        Dim oTemp As SAPbobsCOM.Recordset
        oTemp = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTemp.DoQuery("Select DocCur from OINV where docentry=" & aDocEntry)
        Return oTemp.Fields.Item(0).Value
    End Function
#End Region

#Region "GetEditTextValues"
    Public Function getEditTextvalue(ByVal aForm As SAPbouiCOM.Form, ByVal strUID As String) As String
        Dim oEditText As SAPbouiCOM.EditText
        oEditText = aForm.Items.Item(strUID).Specific
        Return oEditText.Value
    End Function
#End Region

#Region "Get Currency"
    Public Function GetCurrency(ByVal strChoice As String, Optional ByVal aCardCode As String = "") As String
        Dim strCurrQuery, Currency As String
        Dim oTempCurrency As SAPbobsCOM.Recordset
        If strChoice = "Local" Then
            strCurrQuery = "Select MainCurncy from OADM"
        Else
            strCurrQuery = "Select Currency from OCRD where CardCode='" & aCardCode & "'"
        End If
        oTempCurrency = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oTempCurrency.DoQuery(strCurrQuery)
        Currency = oTempCurrency.Fields.Item(0).Value
        Return Currency
    End Function

#End Region

    Public Function FormatDataSourceValue(ByVal Value As String) As Double
        Dim NewValue As Double

        If Value <> "" Then
            If Value.IndexOf(".") > -1 Then
                Value = Value.Replace(".", CompanyDecimalSeprator)
            End If

            If Value.IndexOf(CompanyThousandSeprator) > -1 Then
                Value = Value.Replace(CompanyThousandSeprator, "")
            End If
        Else
            Value = "0"

        End If

        ' NewValue = CDbl(Value)
        NewValue = Val(Value)

        Return NewValue


        'Dim dblValue As Double
        'Value = Value.Replace(CompanyThousandSeprator, "")
        'Value = Value.Replace(CompanyDecimalSeprator, System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
        'dblValue = Val(Value)
        'Return dblValue

    End Function

    Public Function FormatScreenValues(ByVal Value As String) As Double
        Dim NewValue As Double

        If Value <> "" Then
            If Value.IndexOf(".") > -1 Then
                Value = Value.Replace(".", CompanyDecimalSeprator)
            End If
        Else
            Value = "0"
        End If

        'NewValue = CDbl(Value)
        NewValue = Val(Value)

        Return NewValue

        'Dim dblValue As Double
        'Value = Value.Replace(CompanyThousandSeprator, "")
        'Value = Value.Replace(CompanyDecimalSeprator, System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
        'dblValue = Val(Value)
        'Return dblValue

    End Function

    Public Function SetScreenValues(ByVal Value As String) As String

        If Value.IndexOf(CompanyDecimalSeprator) > -1 Then
            Value = Value.Replace(CompanyDecimalSeprator, ".")
        End If

        Return Value

    End Function

    Public Function SetDBValues(ByVal Value As String) As String

        If Value.IndexOf(CompanyDecimalSeprator) > -1 Then
            Value = Value.Replace(CompanyDecimalSeprator, ".")
        End If

        Return Value

    End Function

    Public Function getNames(aCode As String, aType As String) As String
        Dim oGetNames As SAPbobsCOM.Recordset
        oGetNames = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        If aType = "CardCode" Then
            oGetNames.DoQuery("Select ""CardName"" from OCRD where ""CardCode""='" & aCode & "'")
            Return oGetNames.Fields.Item(0).Value
        ElseIf aType = "ItemCode" Then
            oGetNames.DoQuery("Select ""ItemName"" from OITM where ""ItemCode""='" & aCode & "'")
            Return oGetNames.Fields.Item(0).Value
        ElseIf aType = "WhsCode" Then
            oGetNames.DoQuery("Select ""WhsName"" from OWHS where ""WhsCode""='" & aCode & "'")
            Return oGetNames.Fields.Item(0).Value
        ElseIf aType = "AcctCode" Then
            oGetNames.DoQuery("Select ""U_AcctCode"" from OWHS where ""WhsCode""='" & aCode & "'")
            Return oGetNames.Fields.Item(0).Value
        ElseIf aType = "ClawCardCode" Then
            oGetNames.DoQuery("Select ""U_CardCode"" from OWHS where ""WhsCode""='" & aCode & "'")
            Return oGetNames.Fields.Item(0).Value
        ElseIf aType = "GL" Then
            oGetNames.DoQuery("Select ""AcctCode"" from OACT where ""FormatCode""='" & aCode & "'")
            Return oGetNames.Fields.Item(0).Value
        Else

            Return ""
        End If
    End Function

    Public Sub OpenFileDialogBox(ByVal oForm As SAPbouiCOM.Form, ByVal strID As String)
        Dim _retVal As String = String.Empty
        Try
            FolderOpen()
            CType(oForm.Items.Item(strID).Specific, SAPbouiCOM.EditText).Value = strFilepath
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            End If
        Catch ex As Exception
            oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
    Public Sub OpenFileopenBox(ByVal oForm As SAPbouiCOM.Form, ByVal strID As String, ByVal StrID1 As String)
        Dim _retVal As String = String.Empty
        If oApplication.SBO_Application.ClientType = SAPbouiCOM.BoClientType.ct_Browser Then
            Dim strFile As String = oApplication.SBO_Application.GetFileFromBrowser()
            strFilepath = strFile
            strMdbFileName = Path.GetFileName(strFilepath)
            If strFilepath <> "" Then
                CType(oForm.Items.Item(strID).Specific, SAPbouiCOM.EditText).Value = strFilepath
                CType(oForm.Items.Item(StrID1).Specific, SAPbouiCOM.EditText).Value = strMdbFileName
            End If
            '   CType(oForm.Items.Item(strID).Specific, SAPbouiCOM.EditText).Value = strFilepath
            '   CType(oForm.Items.Item(StrID1).Specific, SAPbouiCOM.EditText).Value = strMdbFileName
        Else
            Try
                FileOpen()
                If strFilepath <> "" Then
                    CType(oForm.Items.Item(strID).Specific, SAPbouiCOM.EditText).Value = strFilepath
                    CType(oForm.Items.Item(StrID1).Specific, SAPbouiCOM.EditText).Value = strMdbFileName
                End If
              
            Catch ex As Exception
                oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            End Try
        End If
    End Sub

    Public Sub OpenFileopenBox(ByVal oForm As SAPbouiCOM.Form, ByVal strID As String)
        Dim _retVal As String = String.Empty
        If oApplication.SBO_Application.ClientType = SAPbouiCOM.BoClientType.ct_Browser Then
            Dim strFile As String = oApplication.SBO_Application.GetFileFromBrowser()
            strFilepath = strFile
            strMdbFileName = Path.GetFileName(strFilepath)
            If strFilepath <> "" Then
                CType(oForm.Items.Item(strID).Specific, SAPbouiCOM.EditText).Value = strFilepath
            End If

            ' CType(oForm.Items.Item(StrID1).Specific, SAPbouiCOM.EditText).Value = strMdbFileName
        Else
            Try
                FolderOpen()
                If strFilepath <> "" Then
                    CType(oForm.Items.Item(strID).Specific, SAPbouiCOM.EditText).Value = strFilepath
                End If

                '  CType(oForm.Items.Item(StrID1).Specific, SAPbouiCOM.EditText).Value = strMdbFileName
            Catch ex As Exception
                oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            End Try
        End If
    End Sub
    Private Sub FolderOpen()
        Try
            Dim mythr As New System.Threading.Thread(AddressOf ShowFileDialog)
            mythr.SetApartmentState(Threading.ApartmentState.STA)
            mythr.Start()
            mythr.Join()
        Catch ex As Exception
            oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
    Private Sub FileOpen()
        Try
           
            Dim mythr As New System.Threading.Thread(AddressOf Show_File_Selection_Dialog)
            mythr.SetApartmentState(Threading.ApartmentState.STA)
            mythr.Start()
            mythr.Join()
        Catch ex As Exception
            oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
#Region " Open File Diaologue window"
    Private Sub Show_File_Selection_Dialog()
        Try
            Dim oDialogBox As New OpenFileDialog
            Dim oProcesses() As Process
            Dim oWinForm As New System.Windows.Forms.Form()
            oWinForm.TopMost = True
            oWinForm.Show()
            Try
                oProcesses = Process.GetProcessesByName("SAP Business One")
                If oProcesses.Length <> 0 Then
                    For i As Integer = 0 To oProcesses.Length - 1
                        Dim MyWindow As New WindowWrapper(oProcesses(i).MainWindowHandle)
                        '  SetForegroundWindow(MyWindow.Handle)
                        SetForegroundWindow(FindWindow(Nothing, gWindowText))
                        If oDialogBox.ShowDialog(MyWindow) = DialogResult.OK Then
                            strMdbFileName = oDialogBox.SafeFileName
                            strFilepath = oDialogBox.FileName
                            Exit For
                        Else
                            '  strFilepath = ""
                            '  strMdbFileName = ""
                            Exit For
                        End If
                    Next
                End If
            Catch ex As Exception
                oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Finally
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
    Private Sub ShowFileDialog()
        Try

            Dim oDialogBox As New OpenFileDialog
            Dim oProcesses() As Process
            Dim oWinForm As New System.Windows.Forms.Form()
            oWinForm.TopMost = True
            oWinForm.Show()
            Try
                oProcesses = Process.GetProcessesByName("SAP Business One")
                If oProcesses.Length <> 0 Then
                    For i As Integer = 0 To oProcesses.Length - 1
                        Dim MyWindow As New WindowWrapper(oProcesses(i).MainWindowHandle)
                        'SetForegroundWindow(FindWindow(Nothing, gWindowText))
                        If oDialogBox.ShowDialog(MyWindow) = DialogResult.OK Then
                            strSelectedFolderPath = oDialogBox.SafeFileName
                            strSelectedFilepath = oDialogBox.FileName
                            Exit For
                        Else
                            strSelectedFolderPath = ""
                            strSelectedFilepath = ""
                            Exit For
                        End If
                    Next
                End If
            Catch ex As Exception
                oApplication.Utilities.Message(ex.Message, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Finally
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#Region "AddControls"
    Public Sub AddControls(ByVal objForm As SAPbouiCOM.Form, ByVal ItemUID As String, ByVal SourceUID As String, ByVal ItemType As SAPbouiCOM.BoFormItemTypes, ByVal position As String, Optional ByVal fromPane As Integer = 1, Optional ByVal toPane As Integer = 1, Optional ByVal linkedUID As String = "", Optional ByVal strCaption As String = "", Optional ByVal dblWidth As Double = 0, Optional ByVal dblTop As Double = 0, Optional ByVal Hight As Double = 0, Optional ByVal Enable As Boolean = True)
        Dim objNewItem, objOldItem As SAPbouiCOM.Item
        Dim ostatic As SAPbouiCOM.StaticText
        Dim oButton As SAPbouiCOM.Button
        Dim oCheckbox As SAPbouiCOM.CheckBox
        Dim oEditText As SAPbouiCOM.EditText
        Dim ofolder As SAPbouiCOM.Folder
        objOldItem = objForm.Items.Item(SourceUID)
        objNewItem = objForm.Items.Add(ItemUID, ItemType)
        With objNewItem
            If ItemType = SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON Then
                .Left = objOldItem.Left - 15
                .Top = objOldItem.Top + 1
                .LinkTo = linkedUID
            Else
                If position.ToUpper = "RIGHT" Then
                    .Left = objOldItem.Left + objOldItem.Width + 5
                    .Top = objOldItem.Top
                ElseIf position.ToUpper = "DOWN" Then
                    If ItemUID = "edWork" Then
                        .Left = objOldItem.Left + 40
                    Else
                        .Left = objOldItem.Left
                    End If
                    .Top = objOldItem.Top + objOldItem.Height + 3

                    .Width = objOldItem.Width
                    .Height = objOldItem.Height
                ElseIf position.ToUpper = "COPY" Then
                    .Top = objOldItem.Top
                    .Left = objOldItem.Left
                    .Height = objOldItem.Height
                    .Width = objOldItem.Width
                End If
            End If
            .FromPane = fromPane
            .ToPane = toPane
            If ItemType = SAPbouiCOM.BoFormItemTypes.it_STATIC Then
                .LinkTo = linkedUID
            End If
            .LinkTo = linkedUID
        End With
        If (ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT Or ItemType = SAPbouiCOM.BoFormItemTypes.it_STATIC) Then
            objNewItem.Width = objOldItem.Width
        End If
        If ItemType = SAPbouiCOM.BoFormItemTypes.it_BUTTON Then
            objNewItem.Width = objOldItem.Width '+ 50
            oButton = objNewItem.Specific
            oButton.Caption = strCaption
        ElseIf ItemType = SAPbouiCOM.BoFormItemTypes.it_FOLDER Then
            ofolder = objNewItem.Specific
            ofolder.Caption = strCaption
            ofolder.GroupWith(linkedUID)
        ElseIf ItemType = SAPbouiCOM.BoFormItemTypes.it_STATIC Then
            ostatic = objNewItem.Specific
            ostatic.Caption = strCaption
        ElseIf ItemType = SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX Then
            oCheckbox = objNewItem.Specific
            oCheckbox.Caption = strCaption

        End If
        If dblWidth <> 0 Then
            objNewItem.Width = dblWidth
        End If

        If dblTop <> 0 Then
            objNewItem.Top = objNewItem.Top + dblTop
        End If
        If Hight <> 0 Then
            objNewItem.Height = objNewItem.Height + Hight
        End If
    End Sub
#End Region

#Region "Set / Get Values from Matrix"
    Public Function getMatrixValues(ByVal aMatrix As SAPbouiCOM.Matrix, ByVal coluid As String, ByVal intRow As Integer) As String
        Return aMatrix.Columns.Item(coluid).Cells.Item(intRow).Specific.value
    End Function
    Public Sub SetMatrixValues(ByVal aMatrix As SAPbouiCOM.Matrix, ByVal coluid As String, ByVal intRow As Integer, ByVal strvalue As String)
        aMatrix.Columns.Item(coluid).Cells.Item(intRow).Specific.value = Convert.ToString(strvalue.ToString().Trim())
    End Sub
#End Region

#Region "Add Condition CFL"
    Public Sub AddConditionCFL(ByVal FormUID As String, ByVal strQuery As String, ByVal strQueryField As String, ByVal sCFL As String)
        Dim oRecordSet As SAPbobsCOM.Recordset
        Dim oCFLs As SAPbouiCOM.ChooseFromListCollection
        Dim Conditions As SAPbouiCOM.Conditions
        Dim oCond As SAPbouiCOM.Condition
        Dim oCFL As SAPbouiCOM.ChooseFromList
        Dim oCFLCreationParams As SAPbouiCOM.ChooseFromListCreationParams
        Dim sDocEntry As New ArrayList()
        Dim sDocNum As ArrayList
        Dim MatrixItem As ArrayList
        sDocEntry = New ArrayList()
        sDocNum = New ArrayList()
        MatrixItem = New ArrayList()

        Try
            oCFLs = oApplication.SBO_Application.Forms.Item(FormUID).ChooseFromLists
            oCFLCreationParams = oApplication.SBO_Application.CreateObject( _
                                    SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)

            oCFL = oCFLs.Item(sCFL)

            Dim oRec As SAPbobsCOM.Recordset
            oRec = DirectCast(oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
            oRec.DoQuery(strQuery)
            oRec.MoveFirst()

            Try
                If oRec.EoF Then
                    sDocEntry.Add("")
                Else
                    While Not oRec.EoF
                        Dim DocNum As String = oRec.Fields.Item(strQueryField).Value.ToString()
                        If DocNum <> "" Then
                            sDocEntry.Add(DocNum)
                        End If
                        oRec.MoveNext()
                    End While
                End If
            Catch generatedExceptionName As Exception
                Throw
            End Try

            'If IsMatrixCondition = True Then
            '    Dim oMatrix As SAPbouiCOM.Matrix
            '    oMatrix = DirectCast(oForm.Items.Item(Matrixname).Specific, SAPbouiCOM.Matrix)

            '    For a As Integer = 1 To oMatrix.RowCount
            '        If a <> pVal.Row Then
            '            MatrixItem.Add(DirectCast(oMatrix.Columns.Item(columnname).Cells.Item(a).Specific, SAPbouiCOM.EditText).Value)
            '        End If
            '    Next
            '    If removelist = True Then
            '        For xx As Integer = 0 To MatrixItem.Count - 1
            '            Dim zz As String = MatrixItem(xx).ToString()
            '            If sDocEntry.Contains(zz) Then
            '                sDocEntry.Remove(zz)
            '            End If
            '        Next
            '    End If
            'End If

            'oCFLs = oForm.ChooseFromLists
            'oCFLCreationParams = DirectCast(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams), SAPbouiCOM.ChooseFromListCreationParams)
            'If systemMatrix = True Then
            '    Dim oCFLEvento As SAPbouiCOM.IChooseFromListEvent = Nothing
            '    oCFLEvento = DirectCast(pVal, SAPbouiCOM.IChooseFromListEvent)
            '    Dim sCFL_ID As String = Nothing
            '    sCFL_ID = oCFLEvento.ChooseFromListUID
            '    oCFL = oForm.ChooseFromLists.Item(sCFL_ID)
            'Else
            '    oCFL = oForm.ChooseFromLists.Item(sCHUD)
            'End If

            Conditions = New SAPbouiCOM.Conditions()
            oCFL.SetConditions(Conditions)
            Conditions = oCFL.GetConditions()
            oCond = Conditions.Add()
            oCond.BracketOpenNum = 2
            For i As Integer = 0 To sDocEntry.Count - 1
                If i > 0 Then
                    oCond.Relationship = SAPbouiCOM.BoConditionRelationship.cr_OR
                    oCond = Conditions.Add()
                    oCond.BracketOpenNum = 1
                End If

                oCond.[Alias] = strQueryField
                oCond.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                oCond.CondVal = sDocEntry(i).ToString()
                If i + 1 = sDocEntry.Count Then
                    oCond.BracketCloseNum = 2
                Else
                    oCond.BracketCloseNum = 1
                End If
            Next

            oCFL.SetConditions(Conditions)


        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Public Sub SendMessage(ByVal strMobileNo As String, ByVal strMsg As String)
        Try
            Dim nvCollection As New NameValueCollection
            Dim oRequest As New Net.WebClient
            Dim responseArray() As Byte
            Dim strRequest As String = "http://www.smstoalert.com/Api/apisend/uid/caparsms/pwd/techo123/senderid/CAPAROLPAINTS/to/{0}/msg/{1}"
            Dim strMessage As String = String.Format(strRequest, strMobileNo, strMsg)
            MessageBox.Show(strMessage)
            'responseArray = oRequest.UploadValues(strRequest, "POST", nvCollection)
            'MessageBox.Show(Encoding.ASCII.GetString(responseArray))
        Catch ex As Exception

        End Try
    End Sub

    Public Sub addUDODefaultValues()
        Try
            Dim oGenService As SAPbobsCOM.GeneralService
            Dim oGenData As SAPbobsCOM.GeneralData
            Dim oGenDataCollection As SAPbobsCOM.GeneralDataCollection
            Dim oCompService As SAPbobsCOM.CompanyService
            Dim oChildData1 As SAPbobsCOM.GeneralData
            Dim oGeneralDataParams As SAPbobsCOM.GeneralDataParams
            Dim oRecordSet As SAPbobsCOM.Recordset
            oRecordSet = oApplication.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)


            Dim oHashTable As New Hashtable
            oHashTable.Add("Type", "TypeDeTransaction")
            oHashTable.Add("DocNum", "NumeroDuDocument")
            oHashTable.Add("DocDate", "DateDuDocument")
            oHashTable.Add("ItemCode ", "CodeArticle")
            oHashTable.Add("Dscription ", "DescriptionArticle")
            oHashTable.Add("unitMsr", "unitesDinventaireDeMesure")
            oHashTable.Add("Quantity", "Quantite")
            oHashTable.Add("WhsCode", "CodeDentrepot")
            oHashTable.Add("Lot", "Lot")
            oHashTable.Add("BatchNum", "NumeroDeLot")
            'oHashTable.Add("unitMsr", "unitesDinventaireDeMesure")
            oHashTable.Add("ExpDate ", "DateDexpiration")
            oHashTable.Add("BQuantity ", "QtyParLot")
            oHashTable.Add("Documents ", "Racines")

            oCompService = oApplication.Company.GetCompanyService
            oGenService = oCompService.GetGeneralService("Z_HDF_OBND")
            oGenData = oGenService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
            oGeneralDataParams = oGenService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)


            Dim strQuery As String = "Select * From [@Z_HDF_OBND] Where U_Type = 'GRPO'"
            oRecordSet.DoQuery(strQuery)
            If oRecordSet.EoF Then
                strQuery = "Select * From [@Z_HDF_OBND]"
                oRecordSet.DoQuery(strQuery)
                Dim intRecord As Integer = oRecordSet.RecordCount + 1
                oGenData.SetProperty("Code", intRecord.ToString)
                oGenData.SetProperty("Name", intRecord.ToString)
                oGenData.SetProperty("U_Type", "GRPO")
                oGenDataCollection = oGenData.Child("Z_HDF_OBD1")
                For Each element As DictionaryEntry In oHashTable
                    oChildData1 = oGenDataCollection.Add()
                    oChildData1.SetProperty("U_EName", element.Key)
                    oChildData1.SetProperty("U_FName", element.Value)
                Next
                oGeneralDataParams = oGenService.Add(oGenData)
            End If

            oGenData = oGenService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
            strQuery = "Select * From [@Z_HDF_OBND] Where U_Type = 'GR'"
            oRecordSet.DoQuery(strQuery)
            If oRecordSet.EoF Then
                strQuery = "Select * From [@Z_HDF_OBND]"
                oRecordSet.DoQuery(strQuery)
                Dim intRecord As Integer = oRecordSet.RecordCount + 1
                oGenData.SetProperty("Code", intRecord.ToString)
                oGenData.SetProperty("Name", intRecord.ToString)
                oGenData.SetProperty("U_Type", "GR")
                oGenDataCollection = oGenData.Child("Z_HDF_OBD1")
                For Each element As DictionaryEntry In oHashTable
                    oChildData1 = oGenDataCollection.Add()
                    oChildData1.SetProperty("U_EName", element.Key)
                    oChildData1.SetProperty("U_FName", element.Value)
                Next
                oGeneralDataParams = oGenService.Add(oGenData)
            End If

            oGenData = oGenService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
            strQuery = "Select * From [@Z_HDF_OBND] Where U_Type = 'APCreditMemo'"
            oRecordSet.DoQuery(strQuery)
            If oRecordSet.EoF Then
                strQuery = "Select * From [@Z_HDF_OBND]"
                oRecordSet.DoQuery(strQuery)
                Dim intRecord As Integer = oRecordSet.RecordCount + 1
                oGenData.SetProperty("Code", intRecord.ToString)
                oGenData.SetProperty("Name", intRecord.ToString)
                oGenData.SetProperty("U_Type", "APCreditMemo")
                oGenDataCollection = oGenData.Child("Z_HDF_OBD1")
                For Each element As DictionaryEntry In oHashTable
                    oChildData1 = oGenDataCollection.Add()
                    oChildData1.SetProperty("U_EName", element.Key)
                    oChildData1.SetProperty("U_FName", element.Value)
                Next
                oGeneralDataParams = oGenService.Add(oGenData)
            End If

            oGenData = oGenService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
            strQuery = "Select * From [@Z_HDF_OBND] Where U_Type = 'Inventory'"
            oRecordSet.DoQuery(strQuery)
            If oRecordSet.EoF Then
                strQuery = "Select * From [@Z_HDF_OBND]"
                oRecordSet.DoQuery(strQuery)
                Dim intRecord As Integer = oRecordSet.RecordCount + 1
                oGenData.SetProperty("Code", intRecord.ToString)
                oGenData.SetProperty("Name", intRecord.ToString)
                oGenData.SetProperty("U_Type", "Inventory")
                oGenDataCollection = oGenData.Child("Z_HDF_OBD1")
                For Each element As DictionaryEntry In oHashTable
                    oChildData1 = oGenDataCollection.Add()
                    oChildData1.SetProperty("U_EName", element.Key)
                    oChildData1.SetProperty("U_FName", element.Value)
                Next
                oGeneralDataParams = oGenService.Add(oGenData)
            End If

            '
            oGenService = oCompService.GetGeneralService("Z_HDF_IBND")
            oGenData = oGenService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
            strQuery = "Select * From [@Z_HDF_IBND] Where U_Type = 'Item-Add'"
            oRecordSet.DoQuery(strQuery)
            If oRecordSet.EoF Then
                strQuery = "Select * From [@Z_HDF_IBND]"
                oRecordSet.DoQuery(strQuery)
                Dim intRecord As Integer = oRecordSet.RecordCount + 1
                oGenData.SetProperty("Code", intRecord.ToString)
                oGenData.SetProperty("Name", intRecord.ToString)
                oGenData.SetProperty("U_Type", "Item-Add")
                oGenDataCollection = oGenData.Child("Z_HDF_IBD1")
                For Each element As DictionaryEntry In oHashTable
                    oChildData1 = oGenDataCollection.Add()
                    oChildData1.SetProperty("U_EName", element.Key)
                    oChildData1.SetProperty("U_FName", element.Value)
                Next
                oGeneralDataParams = oGenService.Add(oGenData)
            End If

            '
            oGenService = oCompService.GetGeneralService("Z_HDF_IBND")
            oGenData = oGenService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
            strQuery = "Select * From [@Z_HDF_IBND] Where U_Type = 'Item-Update'"
            oRecordSet.DoQuery(strQuery)
            If oRecordSet.EoF Then
                strQuery = "Select * From [@Z_HDF_IBND]"
                oRecordSet.DoQuery(strQuery)
                Dim intRecord As Integer = oRecordSet.RecordCount + 1
                oGenData.SetProperty("Code", intRecord.ToString)
                oGenData.SetProperty("Name", intRecord.ToString)
                oGenData.SetProperty("U_Type", "Item-Update")
                oGenDataCollection = oGenData.Child("Z_HDF_IBD1")
                For Each element As DictionaryEntry In oHashTable
                    oChildData1 = oGenDataCollection.Add()
                    oChildData1.SetProperty("U_EName", element.Key)
                    oChildData1.SetProperty("U_FName", element.Value)
                Next
                oGeneralDataParams = oGenService.Add(oGenData)
            End If

            '
            oGenService = oCompService.GetGeneralService("Z_HDF_IBND")
            oGenData = oGenService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
            strQuery = "Select * From [@Z_HDF_IBND] Where U_Type = 'Item-Delete'"
            oRecordSet.DoQuery(strQuery)
            If oRecordSet.EoF Then
                strQuery = "Select * From [@Z_HDF_IBND]"
                oRecordSet.DoQuery(strQuery)
                Dim intRecord As Integer = oRecordSet.RecordCount + 1
                oGenData.SetProperty("Code", intRecord.ToString)
                oGenData.SetProperty("Name", intRecord.ToString)
                oGenData.SetProperty("U_Type", "Item-Delete")
                oGenDataCollection = oGenData.Child("Z_HDF_IBD1")
                For Each element As DictionaryEntry In oHashTable
                    oChildData1 = oGenDataCollection.Add()
                    oChildData1.SetProperty("U_EName", element.Key)
                    oChildData1.SetProperty("U_FName", element.Value)
                Next
                oGeneralDataParams = oGenService.Add(oGenData)
            End If

            '
            oGenService = oCompService.GetGeneralService("Z_HDF_IBND")
            oGenData = oGenService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
            strQuery = "Select * From [@Z_HDF_IBND] Where U_Type = 'Consignment'"
            oRecordSet.DoQuery(strQuery)
            If oRecordSet.EoF Then
                strQuery = "Select * From [@Z_HDF_IBND]"
                oRecordSet.DoQuery(strQuery)
                Dim intRecord As Integer = oRecordSet.RecordCount + 1
                oGenData.SetProperty("Code", intRecord.ToString)
                oGenData.SetProperty("Name", intRecord.ToString)
                oGenData.SetProperty("U_Type", "Consignment")
                oGenDataCollection = oGenData.Child("Z_HDF_IBD1")
                For Each element As DictionaryEntry In oHashTable
                    oChildData1 = oGenDataCollection.Add()
                    oChildData1.SetProperty("U_EName", element.Key)
                    oChildData1.SetProperty("U_FName", element.Value)
                Next
                oGeneralDataParams = oGenService.Add(oGenData)
            End If

            '
            oGenService = oCompService.GetGeneralService("Z_HDF_IBND")
            oGenData = oGenService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
            strQuery = "Select * From [@Z_HDF_IBND] Where U_Type = 'JE'"
            oRecordSet.DoQuery(strQuery)
            If oRecordSet.EoF Then
                strQuery = "Select * From [@Z_HDF_IBND]"
                oRecordSet.DoQuery(strQuery)
                Dim intRecord As Integer = oRecordSet.RecordCount + 1
                oGenData.SetProperty("Code", intRecord.ToString)
                oGenData.SetProperty("Name", intRecord.ToString)
                oGenData.SetProperty("U_Type", "Consignment")
                oGenDataCollection = oGenData.Child("Z_HDF_IBD1")
                For Each element As DictionaryEntry In oHashTable
                    oChildData1 = oGenDataCollection.Add()
                    oChildData1.SetProperty("U_EName", element.Key)
                    oChildData1.SetProperty("U_FName", element.Value)
                Next
                oGeneralDataParams = oGenService.Add(oGenData)
            End If


        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub NewWriteErrorLog(ByVal sErrMessage As String, ByVal SoNum As String, ByVal InvNo As String, ByVal FileName As String, ByVal TransType As String, ByVal LogFile As String)
        Try
            'FileWriter.Dispose()
            Dim FileWriter As StreamWriter

            FileWriter = New StreamWriter(LogFile, True)
            Dim builder As New System.Text.StringBuilder
            builder.Append(String.Format("{0,-15}", Date.Now.ToShortTimeString))

            builder.Append(vbTab)

            builder.Append(String.Format("{0,-30}", FileName))
            builder.Append(vbTab)

            builder.Append(String.Format("{0,-5}", TransType))
            builder.Append(vbTab)

            builder.Append(String.Format("{0,-15}", SoNum))
            builder.Append(vbTab)
            builder.Append(vbTab)
            'builder.Append(String.Format("{0,-15}", InvNo))
            'builder.Append(vbTab)
            'builder.Append(vbTab)

            builder.Append(sErrMessage)
            builder.Append(vbTab)

            FileWriter.WriteLine(builder)
            FileWriter.Flush()
            FileWriter.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Shared Sub traceService(ByVal FName As String, ByVal LogPath As String, ByVal content As String)
        Try
            Dim strFile As String = FName + ".txt"
            Dim strPath As String = LogPath & "\" & strFile
            content = System.DateTime.Now.ToString("hh:mm:ss") & "-->" & content
            If Not File.Exists(strPath) Then
                Dim fs As New FileStream(strPath, FileMode.Create, FileAccess.Write)
                Dim sw As New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(content)
                sw.Flush()
                sw.Close()
                ' oApplication.SBO_Application.SendFileToBrowser("LocalFileNameInServer.txt")
            Else
                Dim fs As New FileStream(strPath, FileMode.Append, FileAccess.Write)
                Dim sw As New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(content)
                sw.Flush()
                sw.Close()
            End If
            'throw;
        Catch ex As Exception
        End Try
    End Sub

    Public Function LoadMessageForm(ByVal XMLFile As String, ByVal FormType As String) As SAPbouiCOM.Form
        LoadXMLFiles(XMLFile)
        Return Nothing
    End Function
    Public Function flagupdation() As Integer
        Dim oUDT As SAPbobsCOM.UserTable = Nothing
        Dim iresult As Integer
        oUDT = oApplication.Company.UserTables.Item("EA_OWAR")
        'oUDT.UserFields.Fields.Item("U_Flag").Value = 1
        'oUDT.UserFields.Fields.Item("U_FCount").Value =
        'oUDT.UserFields.Fields.Item("U_PRDoc").Value =
        'oUDT.UserFields.Fields.Item("U_CDate").Value =
        'oUDT.UserFields.Fields.Item("U_CTime").Value =
        iresult = oUDT.Add()
        If iresult <> 0 Then
            oApplication.Utilities.Message(oApplication.Company.GetLastErrorDescription, SAPbouiCOM.BoStatusBarMessageType.smt_Error)

            Return iresult
        End If
        Return iresult

    End Function

End Class
